<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register'=>false]);


/*Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider')->name('social.login')->where('provider', 'facebook|google');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback')->where('provider', 'facebook|google')->name('socialCallback');*/

Route::group(['middleware'=>'guest'], function(){
	Route::get('metrics/{id?}', 'ShopifyController@createLogin');
});

Route::get('shopify/access', 'ShopifyController@shopifyInstall')->name('shopify.integrate');
Route::get('shopify/callback', 'ShopifyController@shopifyCallback')->name('shopify.callback');
Route::get('start', 'ShopifyController@loginFromStore');

Route::get('/', 'HomeController@index')->middleware('guest')->name('landing');

Route::group(['middleware'=>'auth'], function(){
	Route::get('change-password', function(){
		return view('pages.change-password');
	})->name('profile.showChangeForm');
	Route::post('change-password', 'UserController@changePassword')->name('profile.changepassword');

	/* Shopify Complete profile */
	Route::get('complete-profile', 'ShopifyController@completeProfileForm')->name('complete-profile-form');
	Route::post('complete-profile', 'ShopifyController@completeProfile')->name('complete-profile');

	/* Shopify Billing */
	Route::get('complete-payment', 'ShopifyController@shopifySubscribeForm')->name('shopify.subscibe-form');
	Route::post('do-subscribe', 'ShopifyController@shopifyDoSubscribe')->name('shopify.do-subscribe');
	Route::get('shopify/billed/{id}', 'ShopifyController@shopifySubscribed')->name('shopify.subscribed');
});

Route::group(['middleware'=>['auth', 'shopifysubscribed']], function(){
	/* Overview */
	Route::get('dashboard', 'HomeController@dashboard')->middleware('socialaccess')->name('home');
	Route::get('export-data', 'HomeController@exportXls')->name('exportxls');
	
	/* Analytics */
	Route::get('analytics', 'AnalyticsController@index')->name('analytics.index');
	Route::get('analytics-data', 'AnalyticsController@getAnalytics')->name('analytics.fetch');

	/* Products */
	Route::get('product-data', 'ProductController@index')->name('products.index');
	Route::get('products/{id}', 'ProductController@details')->name('products.details');
	Route::post('products/update-cogs', 'ProductController@updateProduct')->name('products.updateprod');
	Route::post('products/update-variants', 'ProductController@updateVariant')->name('products.updatevars');
	
	/* Social Access management */
	Route::get('manage-social-access', 'AdAccountController@index')->middleware('socialaccess')->name('adaccounts.index');
	Route::post('update-social-access', 'AdAccountController@updateAccess')->name('adaccounts.updateaccess');
	Route::get('google/callback', 'AdAccountController@googleCallback')->name('access.googlecallback');
	Route::get('social/remove/{type?}', 'AdAccountController@removeAccount')->name('access.remove');

	/* Settings */
	Route::get('settings', 'CommonController@preferences')->name('settings.index');
	Route::post('update-settings', 'CommonController@savePreferences')->name('settings.update');

	/* Expenses */	
	Route::get('expenses', 'CommonController@expenses')->name('expenses');
	Route::post('save-expenses', 'CommonController@saveExpenses')->name('saveExpenses');
});