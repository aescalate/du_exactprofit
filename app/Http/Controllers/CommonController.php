<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Preference;
use App\Models\Expense;
use App\Models\OrderMetrics;

use Auth, Carbon\Carbon;

class CommonController extends Controller
{
    public function preferences(){
    	$preferences = Auth::user()->preferences->user_preferences;
    	return view('pages.settings.index', compact('preferences'));
    }

    public function savePreferences(Request $request){
        $rules = [
    		'notification'=>'required|in:yes,no',
    		'frequency'=>'required|in:daily,weekly,monthly',
    		'fields'=>'required|array|min:1',
    	];

    	$this->validateForm($request->all(), $rules);

    	$preferences = ['notification'=>$request->notification, 'frequency'=>$request->frequency, 'fields'=>$request->fields];

    	Preference::where('user_id', Auth::id())->update(['user_preferences'=>$preferences]);

    	flash('Settings updated successfully')->success();

    	return redirect()->route('settings.index');
    }

    public function expenses(){
        $expenses = defaultCharges();
        if(Auth::user()->expense()->exists()){
            $expenses = Auth::user()->expense->expenses;
        }
        return view('pages.expenses.index', compact('expenses'));
    }

    public function saveExpenses(Request $request){
        $rules = [
            'subscription_fee'=>'required|min:0',
            'cumulative_charge'=>'required|min:0',
            'transaction_charge'=>'required|min:0',
        ];

        $this->validateForm($request->all(), $rules);
        $updateCharge = false;
        if(!Auth::user()->expense()->exists())
            $updateCharge=true;

        Expense::updateOrCreate(['user_id'=>Auth::id()], ['expenses'=>['subscription_fee' => $request->subscription_fee, 'cumulative_charge' => $request->cumulative_charge, 'transaction_charge' => $request->transaction_charge]]);

        if($updateCharge){
            $this->updateMetricsCharges();
        }

        flash('Expenses updated successfully.')->success();
        return redirect()->route('expenses');
    }

    public function updateMetricsCharges(){
        $user = Auth::user();
        $metrics = OrderMetrics::where('user_id', $user->id)->get();
        $expenses = $user->expense->expenses;
        $rates = $this->getCurrencyRates();

        foreach ($metrics as $metricsRow) {
            $mt = $metricsRow->metrics;
            foreach ($metricsRow->metrics as $month=>$days) {
                $monthDays = Carbon::parse($metricsRow->year.'-'.$month)->daysInMonth;
                foreach ($days as $day=>$row) {
                    $row['transaction_charge'] = $row['total_sales']*$expenses['transaction_charge']/100;
                    $row['subscription_fee'] = ($expenses['subscription_fee']/$monthDays)*$rates[$user->shopify_token['shop_currency']];
                    $row['cumulative_charge'] = ($expenses['cumulative_charge']/$monthDays)*$rates[$user->shopify_token['shop_currency']];
                    $row['dedicated_cost'] = $row['cogs']+$row['discounts']+$row['refunds']+$row['transaction_charge'];

                    $mt[$month][$day] = $row;
                }
            }
            $metricsRow->metrics = $mt;
            $metricsRow->save();
        }
    }
}
