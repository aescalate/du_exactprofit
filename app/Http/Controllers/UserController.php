<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Mail;

use App\Mail\SendOTP;
use App\User;
use App\Models\Transaction;

use Auth, Hash;

class UserController extends Controller
{
    public function changePassword(Request $request){
        $rules = [
            'old' => 'required',
            'password' => 'required|confirmed|min:6',
        ];

        $this->validateForm($request->all(), $rules);

        $user = Auth::user();
        if (Hash::check($request->old, $user->password)) {
            //Change the password
            $user->password = Hash::make($request->password);
            $user->save();

            flash('Password changed successfully.')->success();
            return redirect()->route('settings.index');
        }else{
            flash("Old password doesn't match")->error();
            return redirect()->back();
        }
    }
}
