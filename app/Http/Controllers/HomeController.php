<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\OrderMetrics;
use App\Models\Insight;
use App\Models\Campaign;
use App\Exports\InsightsExport;

use Carbon\Carbon, Arr, Auth, Excel;

class HomeController extends Controller
{
    public function index(){
    	return view('pages.home.index');
    }

    public function dashboard(Request $request, $returnArray=false){
    	$access = $this->accessGranted();
        $googleCnv = [];
    	$end = Carbon::now();
        $start = $end->toImmutable()->subDays(29);

        if($request->has('start') && validDate($request->start)){
            $start = Carbon::parse($request->start);
        }
        if($request->has('start') && validDate($request->start)){
            $end = Carbon::parse($request->end);
        }

        $st = $start->toImmutable();
    	$defMetrics = ['total_orders'=>0, 'cancelled_orders'=>0, 'total_sales'=>0, 'discounts'=>0, 'shipping_cost'=>0, 'cogs'=>0, 'revenue'=>0, 'refunds'=>0, 'tax'=>0, 'transaction_charge'=>0];
        $defInsights = ['visits'=>0, 'adspend'=>0, 'cac'=>0, 'adspend_per_order'=>0, 'net_profit'=>0, 'roas'=>0, 'purchase_frequency'=>0, 'ltv'=>0, 'ltv_cac_ratio'=>0, 'revenue_per_visitor'=>0, 'net_prof_per_visitor'=>0, 'conversion_rate'=>0, 'cpc'=>0, 'cpm'=>0, 'ctr'=>0, 'impressions'=>0, 'clicks'=>0, 'facebook_adspend'=>0, 'google_adspend'=>0];

        $week = $total = $average = $defWeek = ['campaigns'=>0, 'clicks'=>0, 'cpc'=>0, 'ctr'=>0, 'cpm'=>0, 'impressions'=>0, 'visits'=>0, 'adspend'=>0, 'revenue'=>0, 'conversion_rate'=>0, 'gross_profit'=>0, 'net_profit'=>0];

    	$cYear = $st->year;
        $metricsRow = OrderMetrics::where(['user_id'=> Auth::id(), 'year'=> $st->year])->first();
        $fbInsights = Insight::where(['user_id'=> Auth::id(), 'year'=> $st->year, 'type'=>'facebook'])->first();
        $googleInsights = Insight::where(['user_id'=> Auth::id(), 'year'=> $st->year, 'type'=>'google'])->first();

        $dates = $charts = $reportDays = [];

        while($st->diffInDays($end, false) >= 0){
            $dates[] = $st->toImmutable()->format("jS M 'y");

            $day = $defMetrics; 
            $dayInsights = $defInsights; 
            $dm = $st->toImmutable()->format('m.d');

            if($cYear != $st->year){
                $cYear = $st->year;
                $metricsRow = OrderMetrics::where(['user_id'=> Auth::id(), 'year'=> $st->year])->first();
                $fbInsights = Insight::where(['user_id'=> Auth::id(), 'year'=> $st->year, 'type'=>'facebook'])->first();
                $googleInsights = Insight::where(['user_id'=> Auth::id(), 'year'=> $st->year, 'type'=>'google'])->first();
            }

            if($metricsRow){
                $day = Arr::get($metricsRow->metrics, $dm, $defMetrics);
            }
            $dayFbConv = $dayGConv = 0;
            if($fbInsights){                
                $dayInsights['visits'] += Arr::get($fbInsights->insights, $dm.'.visits', 0);
                $dayInsights['facebook_adspend'] = Arr::get($fbInsights->insights, $dm.'.spend', 0);
                $dayInsights['adspend'] += $dayInsights['facebook_adspend'];
                if($dayInsights['visits']){
                    $dayFbConv = $day['total_orders']/$dayInsights['visits'];
                }
            }
            if($googleInsights){
                $dayInsights['google_adspend'] = Arr::get($googleInsights->insights, $dm.'.spend', 0);
                $dayInsights['adspend'] += $dayInsights['google_adspend'];
                $dayGConv = Arr::get($googleInsights->insights, $dm.'.conversion_rate', 0);
            }

            $googleCnv[] = $dayGConv;

            $cRate = $dayFbConv + $dayGConv;

            if($access['facebook'] && $access['google']){
                $cRate /= 2;
            }

            $dayInsights['conversion_rate'] = $cRate;

            $day['revenue'] = $day['total_sales'] - $day['refunds'];
            $day['gross_profit'] = $day['revenue'] - $day['cogs'];
            if($day['revenue'] != 0){
                $day['gross_margin'] = ($day['gross_profit']/$day['revenue'])*100;
                $day['net_margin'] = $day['gross_profit']/$day['revenue'];
            }else{
                $day['gross_margin'] = 0;
                $day['net_margin'] = 0;
            }
            $day['avg_ord_value'] = $day['total_orders'] > 0 ? $day['revenue']/$day['total_orders'] : 0;
            $ords = $day['total_orders']-$day['cancelled_orders'];
            $day['avg_ord_profit'] = $ords != 0 ? ($day['gross_profit']+$day['refunds'])/$ords : 0;

            $dayInsights['net_profit'] = $day['revenue']-$day['cogs']-$dayInsights['adspend'];

            if($day['total_orders']){
                $dayInsights['cac'] = $dayInsights['adspend_per_order'] = $dayInsights['adspend']/$day['total_orders'];
            }

            if($dayInsights['adspend']){
                $dayInsights['roas'] = $day['revenue']/$dayInsights['adspend'];
            }

            if($dayInsights['visits']){
                $dayInsights['purchase_frequency'] = $day['total_orders']/$dayInsights['visits'];
                $dayInsights['revenue_per_visitor'] = $day['revenue']/ $dayInsights['visits'];
                $dayInsights['net_prof_per_visitor'] = $dayInsights['net_profit']/$dayInsights['visits'];
            }
            if($dayInsights['purchase_frequency']){
                $dayInsights['ltv'] = $day['avg_ord_profit']/$dayInsights['purchase_frequency'];
            }
            if($dayInsights['adspend_per_order']){
                $dayInsights['ltv_cac_ratio'] = (($day['avg_ord_profit']*$dayInsights['purchase_frequency'])/$dayInsights['adspend_per_order']);
            }

            $campaigns = Campaign::where(['user_id'=>Auth::id()])->whereBetween('created_time', [$st->format('Y-m-d').' 00:00:00', $st->format('Y-m-d').' 23:59:00'])->count();
            $reportDays[$st->format('Y-m-d')] = array_merge(['campaigns'=>$campaigns], $dayInsights, $day);

            /*$row['total_orders'] += $day['total_orders'];
            $row['cancelled_orders'] += $day['cancelled_orders'];
            $row['total_sales'] += $day['total_sales'];
            $row['discounts'] += $day['discounts'];
            $row['shipping_cost'] += $day['shipping_cost'];
            $row['cogs'] += $day['cogs'];
            $row['refunds'] += $day['refunds'];
            $row['tax'] += $day['tax'];
            $row['revenue'] += $day['revenue'];
            $row['gross_profit'] += $day['gross_profit'];

            $row['adspend'] += $dayInsights['adspend'];
            $row['visits'] += $dayInsights['visits'];*/

            /* Setting the daily chart value for each type of element */
            foreach ($day as $key=>$value) {
                $charts[$key][] = round($value, 2);
            }
            foreach ($dayInsights as $key=>$value) {
                $charts[$key][] = round($value, 2);
            }
            
            $st = $st->addDay();
        }

        if($returnArray){
            return $reportDays;
        }

        $cnt=1;
        foreach ($reportDays as $date=>$row) {
            $temp[] = array_merge(['date'=>$date],$row);
            $week['campaigns'] += $row['campaigns'];
            $week['clicks'] += $row['clicks'];
            $week['ctr'] += $row['ctr'];
            $week['cpc'] += $row['cpc'];
            $week['cpm'] += $row['cpm'];
            $week['impressions'] += $row['impressions'];
            $week['visits'] +=$row['visits'];
            $week['adspend'] += $row['adspend'];
            $week['revenue'] += $row['revenue'];
            $week['conversion_rate'] += $row['conversion_rate'];
            $week['gross_profit'] += $row['gross_profit'];
            $week['net_profit'] += $row['net_profit'];
            
            $total['campaigns'] += $row['campaigns'];
            $total['clicks'] += $row['clicks'];
            $total['ctr'] += $row['ctr'];
            $total['cpc'] += $row['cpc'];
            $total['cpm'] += $row['cpm'];
            $total['impressions'] += $row['impressions'];
            $total['visits'] += $row['visits'];
            $total['adspend'] += $row['adspend'];
            $total['revenue'] += $row['revenue'];
            $total['conversion_rate'] += $row['conversion_rate'];
            $total['gross_profit'] += $row['gross_profit'];
            $total['net_profit'] += $row['net_profit'];

            $x[] = date('M d, y', strtotime($date));
            $dt = Carbon::parse($date);
           

            if((Carbon::parse($date))->dayOfWeek == 0){
                $weeks [] = ['row'=>$week, 'days'=>$temp];
                $temp = [];
                $week = $defWeek;
            }elseif ($cnt == count($reportDays)) {
                $weeks [] = ['row'=>$week, 'days'=>$temp];
                $temp = [];
                $week = $defWeek;
            }
            $cnt++;
        }

        $totalOrders = array_sum($charts['total_orders']);
        $avg_ord_value = $totalOrders > 0 ? array_sum($charts['revenue'])/$totalOrders : 0;

        $totalDays = count($reportDays);
        if($totalDays)
            $average = ['campaigns'=>$total['campaigns'], 'clicks'=>$total['clicks']/$totalDays, 'cpc'=>$total['cpc']/$totalDays, 'ctr'=>$total['ctr']/$totalDays, 'cpm'=>$total['cpm']/$totalDays, 'impressions'=>$total['impressions']/$totalDays, 'visits'=>$total['visits']/$totalDays, 'adspend'=>$total['adspend']/$totalDays, 'revenue'=>$total['revenue']/$totalDays, 'conversion_rate'=>$total['conversion_rate']/$totalDays, 'gross_profit'=>$total['gross_profit']/$totalDays, 'net_profit'=>$total['net_profit']/$totalDays];
        $soFar = $this->getSofarData();
    	return view('pages.dashboard.index', compact('charts', 'dates', 'weeks', 'total', 'average', 'avg_ord_value', 'start', 'end', 'soFar'));
    }

    public function getSofarData(){
        $defs = ['campaigns'=>0, 'total_sales'=>0, 'gross_profit'=>0, 'net_profit'=>0, 'cogs'=>0, 'avg_ord_value'=>0, 'visits'=>0, 'total_orders'=>0, 'revenue'=>0, 'adspend'=>0, 'cpc'=>0, 'transaction_charge'=>0];
        $metrics = OrderMetrics::where(['user_id'=> Auth::id()])->get();
        $fbInsights = Insight::where(['user_id'=> Auth::id(), 'type'=>'facebook'])->get();
        $googleInsights = Insight::where(['user_id'=> Auth::id(), 'type'=>'google'])->get();
        foreach ($metrics as $metricsRow) {
            foreach ($metricsRow->metrics as $days) {
                foreach ($days as $day) {
                    $defs['revenue'] += $day['total_sales'] - $day['refunds'];
                    $defs['total_sales'] += $day['total_sales'];
                    $defs['total_orders'] += $day['total_orders'];
                    $defs['cogs'] += $day['cogs'];
                    $defs['transaction_charge'] += $day['transaction_charge'];
                }
            }
        }

        foreach ($fbInsights as $insightRow) {
            foreach ($insightRow->insights as $fDays) {
                foreach ($fDays as $fDay) {
                    $defs['adspend'] += $fDay['spend'];
                    $defs['visits'] += $fDay['visits'];
                    $defs['cpc'] += $fDay['cpc'];
                }
            }
        }

        foreach ($googleInsights as $gInsightRow) {
            foreach ($gInsightRow->insights as $gDays) {
                foreach ($gDays as $gDay) {
                    $defs['adspend'] += $gDay['spend'];
                    $defs['visits'] += isset($gDay['visits']) ? $gDay['visits'] : 0;
                    $defs['cpc'] += $gDay['cpc'];
                }
            }
        }

        $defs['campaigns'] = Campaign::where(['user_id'=>Auth::id()])->count();
        $defs['avg_ord_value'] = $defs['total_orders'] > 0 ? $defs['revenue']/$defs['total_orders'] : 0;
        $defs['gross_profit'] = $defs['revenue']-$defs['cogs'];
        $defs['avg_ord_value'] = $defs['revenue']-$defs['cogs']-$defs['adspend'];

        return $defs;
    }

    public function exportXls(Request $request){
        $insights = $this->dashboard($request, true);

        foreach ($insights as $date=>$insight) {
            $ins[]= [
                'Date'=>date('d-M-y', strtotime($date)),
                'Total Campaigns'=>$insight['campaigns'],
                'Total Clicks'=>$insight['clicks'],
                'CTR'=>round($insight['ctr'], 2),
                'Average CPC'=> round($insight['cpc'], 2),
                'Visits' => $insight['visits'],
                'CPM' => round($insight['cpm'], 2),
                'Impressions' => $insight['impressions'],
                'Spend' => round($insight['adspend'], 2),
                'Revenue' => round($insight['revenue'], 2),
                'Conversion Rate' => round($insight['conversion_rate'], 2),
                'Gross Profit' => round($insight['gross_profit'], 2),
                'Net Profit' => round($insight['net_profit'], 2),
            ];
        }
        return Excel::download(new InsightsExport($ins), 'report.xlsx');
    }
}
