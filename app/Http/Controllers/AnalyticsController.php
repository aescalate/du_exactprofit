<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\OrderMetrics;
use App\Models\Insight;

use Auth, Carbon\Carbon, Arr;

class AnalyticsController extends Controller
{
    public function index(Request $request){
        return view('pages.reports.metrics');
    }
    public function getAnalytics(Request $request){
        $shop = Auth::user()->shopify_token;
        $access = $this->accessGranted();
        $googleCnv = [];
        $end = Carbon::now();
        $start = $end->toImmutable()->subDays(29);

        if($request->has('start') && validDate($request->start)){
            $start = Carbon::parse($request->start);
        }
        if($request->has('start') && validDate($request->start)){
            $end = Carbon::parse($request->end);
        }

        $days = $start->diffInDays($end, false);
        $days = $days > 0 ? $days : 1;
        $compStart = $start->toImmutable()->subDays($days+1);
        $compEnd = $start->toImmutable()->subDay();

        $row = $compRow = $percent = ['total_orders'=>0, 'cancelled_orders'=>0, 'total_sales'=>0, 'discounts'=>0, 'shipping_cost'=>0, 'cogs'=>0, 'tax'=>0, 'revenue'=>0, 'refunds'=>0, 'gross_profit'=>0, 'gross_margin'=>0, 'avg_ord_value'=>0, 'profit_per_order'=>0, 'net_margin'=>0, 'cac'=>0, 'adspend_per_order'=>0, 'net_profit'=>0, 'purchase_frequency'=>0, 'adspend'=>0, 'ltv'=>0, 'ltv_cac_ratio'=>0, 'visits'=>0, 'conversion_rate'=>0, 'revenue_per_visitor'=>0, 'net_prof_per_visitor'=>0, 'roas'=>0, 'transaction_charge'=>0, 'subscription_fee'=>0, 'cumulative_charge'=>0, 'dedicated_cost'=>0, 'avg_ord_profit'=>0, 'avg_ord_cost'=>0, 'exact_profit'=>0];

        $charts = $pastCharts = ['total_orders'=>[], 'cancelled_orders'=>[], 'total_sales'=>[], 'discounts'=>[], 'shipping_cost'=>[], 'cogs'=>[], 'tax'=>[], 'revenue'=>[], 'refunds'=>[], 'gross_profit'=>[], 'gross_margin'=>[], 'avg_ord_value'=>[], 'profit_per_order'=>[], 'net_margin'=>[], 'cac'=>[], 'adspend_per_order'=>[], 'net_profit'=>[], 'purchase_frequency'=>[], 'adspend'=>[], 'ltv'=>[], 'ltv_cac_ratio'=>[], 'visits'=>[], 'conversion_rate'=>[], 'revenue_per_visitor'=>[], 'net_prof_per_visitor'=>[], 'roas'=>[], 'avg_ord_profit'=>[], 'avg_ord_cost'=>[], 'exact_profit'=>[]];
        $cht = $pastDates = $currentDates = [];

        $names = ['total_orders'=>['unit'=>'', 'title'=>'Orders'], 'cancelled_orders'=>['unit'=>'', 'title'=>'Cancelled Orders'], 'total_sales'=>['unit'=>$shop['shop_currency'], 'title'=>'Sales'], 'discounts'=>['unit'=>$shop['shop_currency'], 'title'=>'Discounts'], 'shipping_cost'=>['unit'=>$shop['shop_currency'], 'title'=>'Shipping'], 'cogs'=>['unit'=>$shop['shop_currency'], 'title'=>'COGS'], 'tax'=>['unit'=>$shop['shop_currency'], 'title'=>'Tax'], 'revenue'=>['unit'=>$shop['shop_currency'], 'title'=>'Total Sales'], 'refunds'=>['unit'=>$shop['shop_currency'], 'title'=>'Refunds'], 'gross_profit'=>['unit'=>$shop['shop_currency'], 'title'=>'Gross Profit'], 'gross_margin'=>['unit'=>'%', 'title'=>'Gross Margin'], 'avg_ord_value'=>['unit'=>$shop['shop_currency'], 'title'=>'Avg. Ord. Value'], 'profit_per_order'=>['unit'=>$shop['shop_currency'], 'title'=>'Profit Per Order'], 'net_margin'=>['unit'=>'', 'title'=>'Net Margin'], 'cac'=>['unit'=>$shop['shop_currency'], 'title'=>'CAC'], 'adspend_per_order'=>['unit'=>$shop['shop_currency'], 'title'=>'Adspend/Order'], 'net_profit'=>['unit'=>$shop['shop_currency'], 'title'=>'Net Profit'], 'purchase_frequency'=>['unit'=>'', 'title'=>'Purch. Frequncy'], 'adspend'=>['unit'=>$shop['shop_currency'], 'title'=>'Adspend'], 'ltv'=>['unit'=>$shop['shop_currency'], 'title'=>'LTV'], 'ltv_cac_ratio'=>['unit'=>'', 'title'=>'LTV/CAC Ratio'], 'visits'=>['unit'=>'', 'title'=>'Visitors'], 'conversion_rate'=>['unit'=>'%', 'title'=>'Conv. Rate'], 'revenue_per_visitor'=>['unit'=>$shop['shop_currency'], 'title'=>'Revenue/Visitor'], 'net_prof_per_visitor'=>['unit'=>$shop['shop_currency'], 'title'=>'Net Profit/Visitor'], 'roas'=>['unit'=>'x', 'title'=>'ROAS'], 'avg_ord_profit'=>['unit'=>$shop['shop_currency'], 'title'=>'Avg. Ord. Profit'], 'avg_ord_cost'=>['unit'=>$shop['shop_currency'], 'title'=>'Avg. Ord. Cost'], 'exact_profit'=>['unit'=>$shop['shop_currency'], 'title'=>'Exact Profit']];


        $st = $start->toImmutable();
        $defMetrics = ['total_orders'=>0, 'cancelled_orders'=>0, 'total_sales'=>0, 'discounts'=>0, 'shipping_cost'=>0, 'cogs'=>0, 'revenue'=>0, 'refunds'=>0, 'tax'=>0, 'transaction_charge'=>0, 'subscription_fee'=>0, 'cumulative_charge'=>0, 'dedicated_cost'=>0];
        $defInsights = ['visits'=>0, 'adspend'=>0, 'cac'=>0, 'adspend_per_order'=>0, 'net_profit'=>0, 'roas'=>0, 'purchase_frequency'=>0, 'ltv'=>0, 'ltv_cac_ratio'=>0, 'revenue_per_visitor'=>0, 'net_prof_per_visitor'=>0, 'conversion_rate'=>0];
        
        /* Fetch Shopify metrics data for the seleted date range */
        $cYear = $st->year;
        $metricsRow = OrderMetrics::where(['user_id'=> Auth::id(), 'year'=> $st->year])->first();
        $fbInsights = Insight::where(['user_id'=> Auth::id(), 'year'=> $st->year, 'type'=>'facebook'])->first();
        $googleInsights = Insight::where(['user_id'=> Auth::id(), 'year'=> $st->year, 'type'=>'google'])->first();

        while($st->diffInDays($end, false) >= 0){
            $date = $st->toImmutable()->format('Y-m-d');
            $day = $defMetrics; 
            $dayInsights = $defInsights; 
            $dm = $st->toImmutable()->format('m.d');

            if($cYear != $st->year){
                $cYear = $st->year;
                $metricsRow = OrderMetrics::where(['user_id'=> Auth::id(), 'year'=> $st->year])->first();
                $fbInsights = Insight::where(['user_id'=> Auth::id(), 'year'=> $st->year, 'type'=>'facebook'])->first();
                $googleInsights = Insight::where(['user_id'=> Auth::id(), 'year'=> $st->year, 'type'=>'google'])->first();
            }

            if($metricsRow){
                $day = Arr::get($metricsRow->metrics, $dm, $defMetrics);

            }
            $dayFbConv = $dayGConv = 0;
            if($fbInsights){                
                $dayInsights['visits'] += Arr::get($fbInsights->insights, $dm.'.visits', 0);
                $dayInsights['adspend'] += Arr::get($fbInsights->insights, $dm.'.spend', 0);
                if($dayInsights['visits']){
                    $dayFbConv = $day['total_orders']/$dayInsights['visits'];
                }
            }
            if($googleInsights){
                $dayInsights['adspend'] += Arr::get($googleInsights->insights, $dm.'.spend', 0);
                $dayGConv = Arr::get($googleInsights->insights, $dm.'.conversion_rate', 0);
            }

            $googleCnv[] = $dayGConv;

            $cRate = $dayFbConv + $dayGConv;

            if($access['facebook'] && $access['google']){
                $cRate /= 2;
            }

            $dayInsights['conversion_rate'] = $cRate;

            $day['revenue'] = $day['total_sales'] - $day['refunds'];
            $day['gross_profit'] = $day['revenue'] - $day['cogs'];
            if($day['revenue'] != 0){
                $day['gross_margin'] = ($day['gross_profit']/$day['revenue'])*100;
                $day['net_margin'] = $day['gross_profit']/$day['revenue'];
            }else{
                $day['gross_margin'] = 0;
                $day['net_margin'] = 0;
            }
            $day['avg_ord_value'] = $day['total_orders'] > 0 ? $day['revenue']/$day['total_orders'] : 0;
            $ords = $day['total_orders']-$day['cancelled_orders'];
            $day['profit_per_order'] = $ords != 0 ? ($day['gross_profit']+$day['refunds'])/$ords : 0;
            
            $cost = $dayInsights['adspend'] + $day['subscription_fee'] + $day['cumulative_charge'] + $day['dedicated_cost'];
            $day['avg_ord_profit'] = $day['total_orders'] > 0 ? ($day['total_sales'] - $cost)/$day['total_orders'] : 0;
            $day['avg_ord_cost'] = $day['total_orders'] > 0 ? $cost/$day['total_orders'] : 0;
            $day['exact_profit'] = $day['total_sales'] - $cost;

            $dayInsights['net_profit'] = $day['revenue']-$day['cogs']-$dayInsights['adspend'];

            if($day['total_orders']){
                $dayInsights['cac'] = $dayInsights['adspend_per_order'] = $dayInsights['adspend']/$day['total_orders'];
            }

            if($dayInsights['adspend']){
                $dayInsights['roas'] = $day['revenue']/$dayInsights['adspend'];
            }

            if($dayInsights['visits']){
                $dayInsights['purchase_frequency'] = $day['total_orders']/$dayInsights['visits'];
                $dayInsights['revenue_per_visitor'] = $day['revenue']/ $dayInsights['visits'];
                $dayInsights['net_prof_per_visitor'] = $dayInsights['net_profit']/$dayInsights['visits'];
            }
            if($dayInsights['purchase_frequency']){
                $dayInsights['ltv'] = $day['profit_per_order']/$dayInsights['purchase_frequency'];
            }
            if($dayInsights['adspend_per_order']){
                $dayInsights['ltv_cac_ratio'] = (($day['profit_per_order']*$dayInsights['purchase_frequency'])/$dayInsights['adspend_per_order']);
            }            

            $row['total_orders'] += $day['total_orders'];
            $row['cancelled_orders'] += $day['cancelled_orders'];
            $row['total_sales'] += $day['total_sales'];
            $row['discounts'] += $day['discounts'];
            $row['shipping_cost'] += $day['shipping_cost'];
            $row['cogs'] += $day['cogs'];
            $row['refunds'] += $day['refunds'];
            $row['tax'] += $day['tax'];
            $row['revenue'] += $day['revenue'];
            $row['gross_profit'] += $day['gross_profit'];

            $row['adspend'] += $dayInsights['adspend'];
            $row['visits'] += $dayInsights['visits'];
            $row['subscription_fee'] += $day['subscription_fee'];
            $row['cumulative_charge'] += $day['cumulative_charge'];
            $row['dedicated_cost'] += $day['dedicated_cost'];

            /* Setting the daily chart value for each type of element */
            foreach ($day as $key=>$value) {
                $charts[$key][] = round($value, 2);
            }
            foreach ($dayInsights as $key=>$value) {
                $charts[$key][] = round($value, 2);
            }

            $currentDates[] = $date;
            
            $st = $st->addDay();
        }
            
        $fbCnv = 0;
        $cost = $row['adspend'] + $row['subscription_fee'] + $row['cumulative_charge'] + $row['dedicated_cost'];

        if($row['revenue'] != 0){
            $row['gross_margin'] = ($row['gross_profit']/$row['revenue'])*100;
            $row['net_margin'] = $row['gross_profit']/$row['revenue'];
        }
        if($row['total_orders'] > 0){
            $row['avg_ord_value'] = $row['revenue']/$row['total_orders'];
            $row['cac'] = $row['adspend_per_order'] = $row['adspend']/$row['total_orders'];
            $row['avg_ord_profit'] = ($row['total_sales'] - $cost)/$row['total_orders'];
            $row['avg_ord_cost'] = $cost/$row['total_orders'];
        }

        $row['exact_profit'] = $row['total_sales'] - $cost;

        $ords = $row['total_orders']-$row['cancelled_orders'];
        if($ords != 0)
            $row['profit_per_order'] = ($row['gross_profit']+$row['refunds'])/$ords;

        $row['net_profit'] = $row['revenue'] - $row['cogs'] - $row['adspend'];

        if($row['visits']>0){
            $fbCnv = $row['purchase_frequency'] = $row['total_orders']/$row['visits'];
            $row['revenue_per_visitor'] = $row['revenue']/$row['visits'];
            $row['net_prof_per_visitor'] = $row['net_profit']/$row['visits'];
        }
        if($row['purchase_frequency'] >0 )
            $row['ltv'] = $row['profit_per_order']/ $row['purchase_frequency'];
        if($row['adspend_per_order']>0)
            $row['ltv_cac_ratio'] = ($row['profit_per_order']*$row['purchase_frequency'])/$row['adspend_per_order'];
        if($row['adspend']>0)
            $row['roas'] = $row['revenue']/$row['adspend'];


        $cnv = (count($googleCnv) ? (array_sum($googleCnv)/count($googleCnv)) : 0)+($fbCnv);
        if($access['facebook'] && $access['google'])
            $cnv /=2;
        $row['conversion_rate'] = $cnv;


        /* Fetching past period data from shopify metrics for the comparision with the past data  */
        $cst = $compStart->toImmutable();
        $cYear = $cst->year;
        $metricsRow = OrderMetrics::where(['user_id'=> Auth::id(), 'year'=> $cst->year])->first();
        $fbInsights = Insight::where(['user_id'=> Auth::id(), 'year'=> $cst->year, 'type'=>'facebook'])->first();
        $googleInsights = Insight::where(['user_id'=> Auth::id(), 'year'=> $cst->year, 'type'=>'google'])->first();

        $googleCnv = [];
        while($cst->diffInDays($compEnd, false) >= 0){
            $date = $cst->toImmutable()->format('Y-m-d');
            $day = $defMetrics; 
            $dayInsights = $defInsights; 
            $dm = $cst->toImmutable()->format('m.d');

            if($cYear != $cst->year){
                $cYear = $cst->year;
                $metricsRow = OrderMetrics::where(['user_id'=> Auth::id(), 'year'=> $cst->year])->first();
                $fbInsights = Insight::where(['user_id'=> Auth::id(), 'year'=> $cst->year, 'type'=>'facebook'])->first();
                $googleInsights = Insight::where(['user_id'=> Auth::id(), 'year'=> $cst->year, 'type'=>'google'])->first();
            }

            if($metricsRow){
                $day = Arr::get($metricsRow->metrics, $dm, $defMetrics);

            }
            $dayFbConv = $dayGConv = 0;
            if($fbInsights){
                $dayInsights['visits'] += Arr::get($fbInsights->insights, $dm.'.visits', 0);
                $dayInsights['adspend'] += Arr::get($fbInsights->insights, $dm.'.visits', 0);
                if($dayInsights['visits']){
                    $dayFbConv = $day['total_orders']/$dayInsights['visits'];
                }
            }
            if($googleInsights){
                $dayInsights['adspend'] += Arr::get($googleInsights->insights, $dm.'.spend', 0);
                $dayGConv = Arr::get($googleInsights->insights, $dm.'.conversion_rate', 0);
            }

            $googleCnv[] = $dayGConv;

            $cRate = $dayFbConv + $dayGConv;

            if($access['facebook'] && $access['google']){
                $cRate /= 2;
            }

            $dayInsights['conversion_rate'] = $cRate;

            $day['revenue'] = $day['total_sales'] - $day['refunds'];
            $day['gross_profit'] = $day['revenue'] - $day['cogs'];
            if($day['revenue'] != 0){
                $day['gross_margin'] = ($day['gross_profit']/$day['revenue'])*100;
                $day['net_margin'] = $day['gross_profit']/$day['revenue'];
            }else{
                $day['gross_margin'] = 0;
                $day['net_margin'] = 0;
            }
            $day['avg_ord_value'] = $day['total_orders'] > 0 ? $day['revenue']/$day['total_orders'] : 0;
            $ords = $day['total_orders']-$day['cancelled_orders'];
            $day['profit_per_order'] = $ords != 0 ? ($day['gross_profit']+$day['refunds'])/$ords : 0;
            
            $cost = $dayInsights['adspend'] + $day['subscription_fee'] + $day['cumulative_charge'] + $day['dedicated_cost'];
            $day['avg_ord_profit'] = $day['total_orders'] > 0 ? ($day['total_sales'] - $cost)/$day['total_orders'] : 0;
            $day['avg_ord_cost'] = $day['total_orders'] > 0 ? $cost/$day['total_orders'] : 0;
            $day['exact_profit'] = $day['total_sales'] - $cost;

            $dayInsights['net_profit'] = $day['revenue']-$day['cogs']-$dayInsights['adspend'];

            if($day['total_orders']){
                $dayInsights['cac'] = $dayInsights['adspend_per_order'] = $dayInsights['adspend']/$day['total_orders'];
            }

            if($dayInsights['adspend']){
                $dayInsights['roas'] = $day['revenue']/$dayInsights['adspend'];
            }

            if($dayInsights['visits']){
                $dayInsights['purchase_frequency'] = $day['total_orders']/$dayInsights['visits'];
                $dayInsights['revenue_per_visitor'] = $day['revenue']/ $dayInsights['visits'];
                $dayInsights['net_prof_per_visitor'] = $dayInsights['net_profit']/$dayInsights['visits'];
            }
            if($dayInsights['purchase_frequency']){
                $dayInsights['ltv'] = $day['profit_per_order']/$dayInsights['purchase_frequency'];
            }
            if($dayInsights['adspend_per_order']){
                $dayInsights['ltv_cac_ratio'] = (($day['profit_per_order']*$dayInsights['purchase_frequency'])/$dayInsights['adspend_per_order']);
            }
            
            $compRow['total_orders'] += $day['total_orders'];
            $compRow['cancelled_orders'] += $day['cancelled_orders'];
            $compRow['total_sales'] += $day['total_sales'];
            $compRow['discounts'] += $day['discounts'];
            $compRow['shipping_cost'] += $day['shipping_cost'];
            $compRow['cogs'] += $day['cogs'];
            $compRow['refunds'] += $day['refunds'];
            $compRow['tax'] += $day['tax'];
            $compRow['revenue'] += $day['revenue'];
            $compRow['gross_profit'] += $day['gross_profit'];

            $compRow['adspend'] += $dayInsights['adspend'];
            $compRow['visits'] += $dayInsights['visits'];
            $compRow['subscription_fee'] += $day['subscription_fee'];
            $compRow['cumulative_charge'] += $day['cumulative_charge'];
            $compRow['dedicated_cost'] += $day['dedicated_cost'];

            /* Setting the daily chart value for each type of element */
            foreach ($day as $key=>$value) {
                $pastCharts[$key][] = round($value, 2);
            }
            foreach ($dayInsights as $key=>$value) {
                $pastCharts[$key][] = round($value, 2);
            }

            $pastDates[] = $date;

            $cst = $cst->addDay();
        }

        /* setting the remaingin metrics based on the past advertisement insights and past shopify metrics*/
        $fbCnv = 0;
        $cost = $compRow['adspend'] + $compRow['subscription_fee'] + $compRow['cumulative_charge'] + $compRow['dedicated_cost'];

        if($compRow['revenue'] != 0){
            $compRow['gross_margin'] = ($compRow['gross_profit']/$compRow['revenue'])*100;
            $compRow['net_margin'] = $compRow['gross_profit']/$compRow['revenue'];
        }
        if($compRow['total_orders'] > 0){
            $compRow['avg_ord_value'] = $compRow['revenue']/$compRow['total_orders'];
            $compRow['cac'] = $compRow['adspend_per_order'] = $compRow['adspend']/$compRow['total_orders'];
            $compRow['avg_ord_profit'] = ($compRow['total_sales'] - $cost)/$compRow['total_orders'];
            $compRow['avg_ord_cost'] = $cost/$compRow['total_orders'];
        }

        $compRow['exact_profit'] = $compRow['total_sales'] - $cost;

        $ords = $compRow['total_orders']-$compRow['cancelled_orders'];
        if($ords != 0)
            $compRow['profit_per_order'] = ($compRow['gross_profit']+$compRow['refunds'])/$ords;

        $compRow['net_profit'] = $compRow['revenue'] - $compRow['cogs'] - $compRow['adspend'];

        if($compRow['visits']>0){
            $fbCnv = $compRow['purchase_frequency'] = $compRow['total_orders']/$compRow['visits'];
            $compRow['revenue_per_visitor'] = $compRow['revenue']/$compRow['visits'];
            $compRow['net_prof_per_visitor'] = $compRow['net_profit']/$compRow['visits'];
        }
        if($compRow['purchase_frequency'] >0 )
            $compRow['ltv'] = $compRow['profit_per_order']/ $compRow['purchase_frequency'];
        if($compRow['adspend_per_order']>0)
            $compRow['ltv_cac_ratio'] = ($compRow['profit_per_order']*$compRow['purchase_frequency'])/$compRow['adspend_per_order'];
        if($compRow['adspend']>0)
            $compRow['roas'] = $compRow['revenue']/$compRow['adspend'];


        $cnv = (count($googleCnv) ? (array_sum($googleCnv)/count($googleCnv)) : 0)+($fbCnv);
        if($access['facebook'] && $access['google'])
            $cnv /=2;
        $compRow['conversion_rate'] = $cnv;

        foreach ($row as $key=>$val) {
            if($compRow[$key] == 0){
                if($row[$key] < 0){
                    $percent[$key] = -100;
                }elseif($row[$key] > 0){
                    $percent[$key] = 100;
                }else{
                    $percent[$key] = 0;
                }
            }else{
                $per = 100;
                if(($row[$key] < 0 && $compRow[$key] < 0) && ($row[$key] < $compRow[$key]))
                    $per = -100;
                $percent[$key] = (($row[$key]-$compRow[$key])/$compRow[$key])*$per;   
            }
        }

        $currency = !is_null(Auth::user()->shopify_token) ? Auth::user()->shopify_token['shop_currency'] : '';
        $title = 'Metrics';

        $data = [
            'html' => view('pages.reports.analytics', compact('percent', 'row', 'currency'))->render(),
            'charts'=>$charts,
            'pastCharts'=>$pastCharts,
            'names'=>$names,
            'currentDates'=>$currentDates,
            'pastDates'=>$pastDates,
        ];

        return response()->json($data);
        
    }
}
