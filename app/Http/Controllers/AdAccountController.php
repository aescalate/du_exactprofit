<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Google\AdsApi\AdWords\AdWordsServices;
use Google\AdsApi\AdWords\AdWordsSessionBuilder;
use Google\AdsApi\AdWords\v201809\mcm\CustomerService;
use Google\Auth\OAuth2;

use App\Models\GoogleClientCustomer;
use App\Models\GoogleAnalyticsAccount;
use App\Models\AdAccount;
use App\Models\Pixel;

use Auth;

class AdAccountController extends Controller
{
    public function index(){
    	$access = $this->accessGranted();
    	$accounts = [];
    	if($access['facebook']){
    		$fbToken = Auth::user()->facebook_token;
	    	$fields = ['fields'=>'adaccounts{created_time,owner,age,currency,name}', 'access_token'=>$fbToken['token']];
	    	$res = json_decode($this->curl_request('https://graph.facebook.com/'.$this->fbApiVersion.'/me?'.http_build_query($fields)));
	                
	        if(isset($res->adaccounts)){
	            foreach ($res->adaccounts->data as $account) {
	                $pxls =[];
	                $pixelRes = json_decode($this->curl_request('https://graph.facebook.com/'.$this->fbApiVersion.'/'.$account->id.'/adspixels?access_token='.$fbToken['token'].'&fields=name,id'));
	                if(isset($pixelRes->data)){
	                    foreach ($pixelRes->data as $pixel) {
	                        $pxls[] = ['pixel_id' => $pixel->id, 'pixel_name'=>$pixel->name];
	                    }
	                }
	                $accounts['facebook']['adaccounts'] [] = [
	                    'account_name'=>$account->name,
	                    'account_id'=>$account->id,
	                    'created_time'=>date('Y-m-d H:i:s', strtotime($account->created_time)), 
	                    'owner'=>$account->owner, 
	                    'currency'=>$account->currency,
	                    'pixels'=>$pxls
	                ];
	            }
	        }	

            $accounts['facebook']['fbAdAccounts'] = Auth::user()->adAccounts->pluck('account_id')->toArray();
            $accounts['facebook']['pixel'] = Auth::user()->pixel()->exists() ? Auth::user()->pixel->pixel_id : null;
            session(['facebook'=>$accounts['facebook']]);
    	}
    	
    	if($access['google']){
            $googleToken = Auth::user()->google_token;
            $accounts['google'] = [
                'clientCustomers' => $this->getClientCustomers($googleToken['refreshToken']),
                'analyticsAccounts' => [], // $this->getAnalyticsAccounts($googleToken['refreshToken']),
                'googleClients' => Auth::user()->clientCustomers->pluck('customer_id')->toArray(),
                'googleAnalytics' => Auth::user()->analyticAccounts->pluck('account_id')->toArray()
            ];
            session(['google'=>$accounts['google']]);

    	}
        return view('pages.adaccount.index', compact('accounts'));
    }

    public function getClientCustomers($refreshToken=''){
        $accounts = [];
        $oauth2 = $this->getOAuth2($refreshToken);
        $session = (new AdWordsSessionBuilder())->withDeveloperToken(config('vars.google_dev_token'))->withOAuth2Credential($oauth2)->build();
        $adWordsServices = new AdWordsServices();
        $customerService = $adWordsServices->get($session, CustomerService::class);
        $customers = $customerService->getCustomers();
        foreach ($customers as $customer) {
            $trackingId = ($customer->getConversionTrackingSettings())->getEffectiveConversionTrackingId();
            if($trackingId && !$customer->getTestAccount()){
                $accounts [] = [
                    'customer_id'=>$customer->getCustomerId(), 
                    'currencyCode'=>$customer->getCurrencyCode(),
                    'descriptiveName'=>$customer->getDescriptiveName() != '' ? $customer->getDescriptiveName() : $customer->getCustomerId(),
                    'conversionTrackingId'=>$trackingId
                ];
            }
        }
        return $accounts;
    }

    public function getAnalyticsAccounts($refreshToken){
        $client = new \Google_Client();
        $client->setApplicationName("Analytics");
        $client->setAuthConfig(base_path('client_secrets.json'));
        $client->setScopes(explode(',', config('vars.analytics_scope')));
        $client->fetchAccessTokenWithRefreshToken($refreshToken); 
        $analytics = new \Google_Service_Analytics($client);
        $accounts = $analytics->management_accountSummaries->listManagementAccountSummaries();
        $analyticsAccounts= [];
        foreach ($accounts->getItems() as $account) {
            $webPros = [];
            foreach ($account->getWebProperties() as $property) {
                $temp = [];
                foreach ($property->getProfiles() as $profile) {
                    $temp[] = ['profile_id'=>$profile->getId(), 'name'=>$profile->getName()];
                }
                $webPros[] = ['property_id'=>$property->getId(), 'name'=>$property->getName(), 'web_url'=>$property->getWebsiteUrl(), 'level'=>$property->getLevel(), 'view_profiles'=>$temp];
            }
            $analyticsAccounts[] = ['account_id'=>$account->getId(), 'name'=>$account->getName(), 'web_properties'=>$webPros];
        }

        return $analyticsAccounts;
    }

    public function getOAuth2($refreshToken=''){
        $config = [
            'authorizationUri' => 'https://accounts.google.com/o/oauth2/v2/auth',
            'tokenCredentialUri' => 'https://www.googleapis.com/oauth2/v4/token',
            'redirectUri' => route('access.googlecallback'),
            'clientId' => config('vars.google_client_id'),
            'clientSecret' => config('vars.google_client_secret'),
            /*'scope' => config('vars.adwords_scope').' '.str_replace(',', ' ', config('vars.analytics_scope'))*/
            'scope' => config('vars.adwords_scope')
        ];

        if($refreshToken){
            $config['refresh_token'] = $refreshToken;
        }

        return new OAuth2($config);
    }

    public function updateAccess(Request $request){
        $rules = [];
        
        if(session()->has('facebook')){
            $rules['fbaccount'] = 'required|array|min:1';
            $rules['fbaccount.*'] = 'required';
            /*$rules['pixels'] = 'required';*/
        }
        if(session()->has('google')){
            $rules['googleclient'] = 'required|array|min:1';
            $rules['googleclient.*'] = 'required';   
            /*$rules['analytics'] = 'required';   */
        }

        $this->validateForm($request->all(), $rules);

        $user = Auth::user();

        if(session()->has('google')){
            $gClients = $gAnalytics = [];
            $google = session('google');
            foreach ($google['clientCustomers'] as $client) {
                if(in_array($client['customer_id'], $request->googleclient)){
                    $gClients[] = $client['customer_id'];
                    $this->updateCurrency($client['currencyCode']);
                    GoogleClientCustomer::updateOrCreate(['user_id'=>Auth::id(), 'customer_id'=>$client['customer_id']], ['metadata'=>['currencyCode'=>$client['currencyCode'],'descriptiveName'=>$client['descriptiveName'], 'conversionTrackingId'=>$client['conversionTrackingId']]]);
                }
            }
            
            foreach ($google['analyticsAccounts'] as $analytic) {
                if($analytic['account_id'] ==  $request->analytics){
                    $gAnalytics[] = $analytic['account_id'];
                    GoogleAnalyticsAccount::updateOrCreate([
                        'user_id'=>Auth::id(),
                        'account_id'=>$analytic['account_id']],[
                        'account_name'=>$analytic['name'],
                        'web_properties'=>$analytic['web_properties']
                    ]);
                    break;
                }
            }
            GoogleClientCustomer::where('user_id', Auth::id())->whereNotIn('customer_id', $gClients)->delete();
            GoogleAnalyticsAccount::where('user_id', Auth::id())->whereNotIn('account_id', $gAnalytics)->delete();
            session()->forget(['google']);
        }

        if(session()->has('facebook')){
            $fAds = $pxls = [];
            $facebook = session('facebook');
            foreach ($facebook['adaccounts'] as $adaccount) {
                if(in_array($adaccount['account_id'], $request->fbaccount)){
                    $fAds[] = $adaccount['account_id'];
                    $this->updateCurrency($adaccount['currency']);
                    $adAcc = AdAccount::updateOrCreate(['account_id'=>$adaccount['account_id'], 'user_id'=>Auth::id()], ['created_time'=>$adaccount['created_time'], 'owner'=>$adaccount['owner'], 'currency'=>$adaccount['currency']]);
                    foreach ($adaccount['pixels'] as $pixel) {
                        if($pixel['pixel_id'] == $request->pixels){
                            $pxls[] = $pixel['pixel_id'];
                            Pixel::updateOrCreate(['ad_account_id'=>$adAcc->id, 'user_id'=>Auth::id()], ['pixel_id'=>$pixel['pixel_id'], 'pixel_name'=>$pixel['pixel_name']]);
                        }
                    }
                }
            }

            AdAccount::where('user_id', Auth::id())->whereNotIn('account_id', $fAds)->delete();
            Pixel::where('user_id', Auth::id())->whereNotIn('pixel_id', $pxls)->delete();
            session()->forget(['facebook']);
        }

        flash('Accounts updated successfully')->success();
        return redirect()->route('adaccounts.index');
    }

    public function removeAccount($type){
        if($type != ''){
            $user= Auth::user();
            switch ($type) {
                case 'google':
                    $user->google_token = null;
                    $user->save();
                    $user->clientCustomers()->delete();
                    $user->analyticAccounts()->delete();
                    flash('Your Google account removed successfully.')->success();
                    break;

                case 'facebook':                    
                    $user->facebook_token = null;
                    $user->save();
                    $user->adAccounts()->delete();
                    flash('Your Facebook account removed successfully.')->success();
                    break;
                
                default:
                    // code...
                    break;
            }
        }
        return redirect()->route('adaccounts.index');
    }

    public function googleCallback(){}
}
