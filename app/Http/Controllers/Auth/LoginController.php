<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\User;

use Socialite, Exception, Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            ['email'=>$request->email, 'password'=>$request->password, 'active'=>1, 'user_type'=>'client'], $request->filled('remember')
        );
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        if(is_null($user->name) || $user->name == ''){
            return redirect()->route('complete-profile-form');
        }elseif(!is_null($user->shopify_token) && !isset($user->shopify_token['updated_date'])){
            return redirect()->route('shopify.integrate', ['shop_url'=>$user->shopify_token['shop_url']]);
        }
    }

    public function redirectToProvider($provider){
        return Socialite::driver($provider)->redirect();        
    }

    public function handleProviderCallback(Request $request, $provider){
        try{
            $social = Socialite::driver($provider)->user();
            if($provider == 'facebook'){
                $user = User::where('facebook_id', $social->id)->orWhere('email', $social->email)->first();
            }else{
                $user = User::where('google_id', $social->id)->orWhere('email', $social->email)->first();
            }
            if(!$user){
                $user = [
                    'name'=>$social->name,
                    'email'=>$social->email,
                    $provider.'_id'=>$social->id,
                    'email_verified_at'=>time(),
                    'active'=>1,
                ];

                if(isset($social->birthday) && $social->birthday != '' && count(explode('/', $social->birthday))>2){
                    $user['birth_date'] = date('Y-m-d', strtotime($social->birthday));
                }

                $user = User::create($user);   
                $user->createAsStripeCustomer();             
            }else{
                if($provider == 'facebook'){
                    $user->facebook_id = $social->id;
                }
                else{
                    $user->google_id = $social->id;
                }
                $user->active=1;
                $user->save();
            }
            Auth::login($user);

            $access = $this->accessGranted(Auth::user());
            if(!$access['facebook'] && !$access['google']){
                return redirect()->away(config('vars.grant_url').'/'.encryptString($user->id));
            }
            
            return redirect()->route('home');

        }catch(Exception $ex){
            return redirect()->route('login');
        }
    }
}
