<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Task;
use App\Models\GoogleClientCustomer;
use App\User;

use Auth, Socialite, Exception, Carbon\Carbon;

class ShopifyController extends Controller
{
    public function createLogin($id=''){
        try{
            if($id != ''){
                $id = decryptString($id);
                $user = User::where(['id'=>$id, 'active'=>1, 'user_type'=>'client', 'otp'=>null, 'otp_token'=>null])->firstOrFail();
                Auth::login($user);
                return redirect()->route('home');
            }else{
                throw new ModelNotFoundException();
            }
        }catch(ModelNotFoundException $ex){
            return redirect()->route('login');
        }        
    }

    public function shopifyInstall(Request $request){
        $rules = [
            'shop_url' =>'required|max:255'
        ];
        $this->validateForm($request->all(), $rules);
        
        if(preg_match('/[a-zA-Z0-9][a-zA-Z0-9\-]*/', $request->shop_url)){
            $scopes = "read_orders,read_products,read_inventory";
            $redirect_uri = route('shopify.callback');
            $shopUrl = explode('.', $request->shop_url);
            $shopUrl = $shopUrl[0].'.myshopify.com';

            $install_url = "https://" . $shopUrl . "/admin/oauth/authorize?client_id=" . config('vars.shopify_key') . "&scope=" . $scopes . "&redirect_uri=" . urlencode($redirect_uri);
            
            return redirect()->away($install_url);
        }else{
            flash("Please provide valid shop name")->error();
            return redirect()->back();
        }
    }

    public function shopifyCallback(Request $request){
        try {
            if(preg_match('/[(https|http)\:\/\/]?[a-zA-Z0-9][a-zA-Z0-9\-]*\.myshopify\.com[\/]?/', $request->shop) && $request->code != ''){
                
                $params = array_diff_key($request->all(), array('hmac' => ''));
                ksort($params);

                $computed_hmac = hash_hmac('sha256', http_build_query($params), config('vars.shopify_secret'));

                if(hash_equals($request->hmac, $computed_hmac)) {
                    // Set variables for our request
                    $query = array(
                        "client_id" => config('vars.shopify_key'),
                        "client_secret" => config('vars.shopify_secret'),
                        "code" => $request->code
                    );

                    // Generate access token URL
                    $access_token_url = "https://" . $params['shop'] . "/admin/oauth/access_token";

                    // Configure curl client and execute request
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_URL, $access_token_url);
                    curl_setopt($ch, CURLOPT_POST, count($query));
                    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($query));
                    $result = json_decode(curl_exec($ch), true);
                    curl_close($ch);

                    $res = $this->shopifyCall($result['access_token'], $params['shop'], 'shop.json', [], false);
                    $user = User::where('shopify_token->shop_url', $params['shop'])->first();
                    if($user){
                        $fetchPro = false;
                        if(!isset($user->shopify_token['updated_date']))
                            $fetchPro = true;

                        $user->shopify_token = ['shop_url'=>$params['shop'], 'token'=>$result['access_token'], 'shop_name'=>$res['shop']['name'], 'shop_currency'=>$res['shop']['currency'], 'created_at'=>$res['shop']['created_at'], 'timezone'=>$res['shop']['iana_timezone'], 'shop_domain'=>$res['shop']['domain'], 'updated_date'=>Carbon::now()->format('Y-m-d H:i:s')];
                        $user->save();
                        Auth::login($user);
                        if(!is_null($user->shopify_charge_id)){
                            $charge = $this->shopifyCall($user->shopify_token['token'], $user->shopify_token['shop_url'], 'recurring_application_charges/'.$user->shopify_charge_id.'.json', [], false);    
                            if(!array_key_exists('errors', $charge) && count($charge['recurring_application_charge']) && $charge['recurring_application_charge']['status'] == 'active'){
                                if($fetchPro)
                                    $this->curl_request(config('vars.admin_url').'/firec?type=fetchShopifyPro&token='.encryptString($user->id));
                                
                                return redirect()->route('home');
                            }else{
                                $user->shopify_charge_id = null;
                                $user->shopify_subscribe_status = null;
                                $user->save();
                            }
                        }
                    }else{
                        $user = User::create([
                            'email'=>$res['shop']['email'], 
                            'shopify_token'=>[
                                'shop_url'=>$params['shop'], 
                                'token'=>$result['access_token'], 
                                'shop_name'=>$res['shop']['name'],
                                'shop_currency'=>$res['shop']['currency'], 
                                'created_at'=>$res['shop']['created_at'],
                                'timezone'=>$res['shop']['iana_timezone'],
                                'shop_domain'=>$res['shop']['domain'],
                                'updated_date'=>Carbon::now()->format('Y-m-d H:i:s')
                            ],
                            'email_verified_at'=>time(),
                            'active'=>1,
                        ]);
                        Auth::login($user);
                    }
                    return redirect()->route('complete-profile-form');

                }else{
                    throw new Exception();
                }
            }else{
                throw new Exception();
            }
        } catch (Exception $e) {
            flash('Something went wrong, Please try again')->error();
            return redirect()->route('home');
        }
    }

    public function completeProfileForm(){
        if(!is_null(Auth::user()->shopify_charge_id)){
            return redirect()->route('home');
        }
        $title = 'Complete your profile';
        return view('pages.complete-profile-form', compact('title'));
    }

    public function completeProfile(Request $request){
        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email,'.Auth::id().',id',
            'password' => 'required|min:6',
        ];

        $this->validateForm($request->all(), $rules);
        try{
            $user = Auth::user();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            
            $user->save();
            
            return redirect()->route('shopify.subscibe-form');

        }catch(ModelNotFoundException $ex){
            flash('Something went wrong, please try again.')->error();
            return redirect()->back();
        }                    
    }

    public function loginFromStore(Request $request){
        try {
            if(preg_match('/[(https|http)\:\/\/]?[a-zA-Z0-9][a-zA-Z0-9\-]*\.myshopify\.com[\/]?/', $request->shop)){

                $params = array_diff_key($request->all(), array('hmac' => ''));
                ksort($params);

                $computed_hmac = hash_hmac('sha256', http_build_query($params), config('vars.shopify_secret'));
                if(hash_equals($request->hmac, $computed_hmac)) {
                    // Set variables for our request
                    
                    $scopes = "read_orders,read_products,read_inventory";
                    $redirect_uri = route('shopify.callback');
                    $shopUrl = $params['shop'];

                    $install_url = "https://" . $shopUrl . "/admin/oauth/authorize?client_id=" . config('vars.shopify_key') . "&scope=" . $scopes . "&redirect_uri=" . urlencode($redirect_uri);
                    
                    return redirect()->away($install_url);

                }else{
                    throw new Exception();
                }
            }else{
                throw new Exception();
            }
        } catch (Exception $e) {
            flash('Something went wrong, Please try again')->error();
            return redirect()->route('home');
        }
    }

    public function getshopifyTransactionUrl(){
        $user = Auth::user();

        $fields = [
            "recurring_application_charge"=> [
                'name'=>'Basic Plan',
                'price'=>$this->shopify_plan_price,
                "test"=>config('vars.shopify_mode') != 'live',
                "return_url"=> route("shopify.subscribed", encryptString($user->id)),
            ]
        ];
        if(!Auth::user()->shopify_subscribe_done){
            $fields['recurring_application_charge']['trial_days'] = $this->shopify_trial_days;
        }
        return $this->shopifyCall($user->shopify_token['token'], $user->shopify_token['shop_url'], 'recurring_application_charges.json', $fields);
    }

    public function shopifySubscribeForm(){
        $user = Auth::user();
        $title = 'Complete Subscription';
        $shouldGiveTrial = !$user->shopify_subscribe_done;
        $trialDays = $this->shopify_trial_days;
        return view('pages.billing', compact('title', 'shouldGiveTrial', 'trialDays'));
    }

    public function shopifyDoSubscribe(){
        $res = $this->getshopifyTransactionUrl();
        if(!array_key_exists('errors', $res) && isset($res['recurring_application_charge'])){
            $url = $res['recurring_application_charge']['confirmation_url'];
            return redirect()->away($url);
        }else{
            flash('Something went wrong with subscription process, Please try again.')->error();
            return redirect()->back();
        }
    }

    public function shopifySubscribed(Request $request, $id=''){
        try {            
            if($request->filled('charge_id') && Auth::id() == decryptString($id)){
                $user = Auth::user();
                $charge = $this->shopifyCall($user->shopify_token['token'], $user->shopify_token['shop_url'], 'recurring_application_charges/'.$request->charge_id.'.json', [], false);
                if(!array_key_exists('errors', $charge) && count($charge['recurring_application_charge']) && $charge['recurring_application_charge']['status'] == 'accepted'){
                    
                    $fields = [
                        "recurring_application_charge"=> [
                            'id'=>$request->charge_id,
                            'name'=>'Basic Plan',
                            'api_client_id'=>rand(10000000, 99999999),
                            'price'=>$this->shopify_plan_price,
                            "test"=>config('vars.shopify_mode') != 'live',
                            "return_url"=> route("shopify.subscribed", encryptString($user->id)),
                            'billing_on'=>null,
                            'activated_on'=>null,
                            'trial_ends_on'=>null,
                            'cancelled_on'=>null,
                            'decorated_return_url'=>route("shopify.subscribed", ['id'=>encryptString($user->id), 'charge_id'=>$request->charge_id]),
                        ]
                    ];
                    if(!Auth::user()->shopify_subscribe_done){
                        $fields['recurring_application_charge']['trial_days'] = $this->shopify_trial_days;
                    }
                    
                    $res = $this->shopifyCall($user->shopify_token['token'], $user->shopify_token['shop_url'], 'recurring_application_charges/'.$request->charge_id.'/activate.json', $fields);
                    if(!array_key_exists('errors', $res) && count($res['recurring_application_charge']) && $res['recurring_application_charge']['status'] == 'active'){
                        $user->shopify_subscribe_done = 1;
                        $user->shopify_charge_id = $request->charge_id;
                        $user->shopify_subscribe_status = $res['recurring_application_charge']['status'];
                        $user->shopify_subscription_date = Carbon::now();
                        $user->save();

                        \App\Models\Preference::firstOrCreate(['user_id'=>$user->id], ['user_preferences'=>getPreferences()]);
                        
                        $this->curl_request(config('vars.admin_url').'/firec?type=fetchShopify&token='.encryptString($user->id));

                    }else{
                        throw new Exception();
                    }
                }
                $access = $this->accessGranted();
                if(!$access['facebook'] || !$access['google'])
                    return redirect()->away(config('vars.grant_url').'/'.encryptString($user->id));
                else
                    return redirect()->route('home');
            }else{
                throw new Exception();
            }
        } catch (Exception $e) {
            flash('Something went wrong with subscription process, Please try again.')->error();
            return redirect()->route('shopify.subscibe-form');
        }
        
    }
}
