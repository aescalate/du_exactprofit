<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Models\Product;
use App\Models\ProductVariant;

use Auth;

class ProductController extends Controller
{
    public function index(){
        $title = 'Products';
        $products = Product::where('user_id', Auth::id())->withCount('variants')->orderBy('product_name')->get();

        $currency = !is_null(Auth::user()->shopify_token) ? Auth::user()->shopify_token['shop_currency'] : '';

        return view('pages.products.list', compact('products', 'currency', 'title'));
    }

    public function details($id){
    	try {
    		$product = Product::where(['product_id'=>$id, 'user_id'=>Auth::id()])->with('variants')->firstOrFail();
            $currency = !is_null(Auth::user()->shopify_token) ? Auth::user()->shopify_token['shop_currency'] : '';

            return view('pages.products.details', compact('product', 'currency'));

    	} catch (ModelNotFoundException $e) {
    		abort(404);
    	}
    }

    public function updateProduct(Request $request){
        $product = Product::where(['product_id'=>$request->id, 'user_id'=>Auth::id()])->first();
        if($product){
            $product->handling_fee = $request->handling ?? 0; 
            $product->base_price = $request->base ?? 0; 
            $product->save();
            
            return response()->json(['status'=>200, 'message'=>'Product updated successfully']);
        }else{
            return response()->json(['status'=>404, 'message'=>'Product not found']);
        }
    }

    public function updateVariant(Request $request){
    	$variant = ProductVariant::where('variant_id', $request->id)->whereHas('product', function($query){
    		$query->where('user_id', Auth::id());
    	})->first();

    	if($variant){
    		$variant->handling_fee = $request->handling ?? 0; 
            $variant->base_price = $request->base ?? 0; 
            $variant->save();	
    	}

    	return true;
    }
}
