<?php
namespace App\Http\ViewComposers;

use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;

use App\Traits\CustomHelpers;

use Route;

class VariableComposer 
{
    use CustomHelpers;

    private $routeBase, $routeName;

	/**
     * Create a menu composer.
     *
     * @return void
     */
    public function __construct()
    {
        $this->routeName = Route::currentRouteName();
        $this->routeBase = substr($this->routeName, 0, strrpos($this->routeName, '.'));
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $data = [
            'date_format' => $this->date_format,
            'date_time_format' => $this->date_time_format,
            'routeBase' => $this->routeBase,
            'routeName' => $this->routeName,
            'accessGranted' => Auth::check() ? $this->accessGranted() : [],
            'shop' => Auth::check() ? Auth::user()->shopify_token : []
        ];

        $view->with($data);
    }
}
