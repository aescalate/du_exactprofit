<?php

namespace App\Http\Middleware;

use Closure, Auth;

class ShopifySubscription
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if(!is_null($user->shopify_uninstalled) || is_null($user->shopify_token))
            return redirect()->route('shopify.integrate', ['shop_url'=>$user->shopify_token['shop_url']]);
        elseif(!$user->shopify_subscribe_done)
            return redirect()->route('complete-profile-form');
        elseif($user->shopify_subscribe_status != 'active')
            return redirect()->route('shopify.subscibe-form');
        
        return $next($request);
    }
}
