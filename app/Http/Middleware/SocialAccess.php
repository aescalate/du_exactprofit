<?php

namespace App\Http\Middleware;

use Closure;

use Auth;

class SocialAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if(!is_null($user->facebook_token) && !isset($user->facebook_token['updated_date'])){
            return redirect()->away(config('vars.grant_url').'/'.encryptString($user->id).'?reqType=facebook');

        }/*elseif(!is_null($user->google_token) && !isset($user->google_token['updated_date'])){
            return redirect()->away(config('vars.grant_url').'/'.encryptString($user->id).'?reqType=google');
        }*/

        return $next($request);
    }
}
