<?php 

namespace App\Traits;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\User;
use App\Models\Price;
use App\Models\Currency;
use App\Models\CurrencyRate;

use Validator, Carbon\Carbon;  

trait CustomHelpers
{
    public $date_format = "m-d-Y";
    public $date_time_format = "M jS, Y H:i A";
    public $db_date_format = "%b %D, %Y";
    public $db_date_time_format = "%b %D, %Y %h:%i %p";
    public $mailsPerPage = 10;
    public $fbApiVersion = 'v8.0';
    public $shopifyApiV = '2020-07';
    public $shopify_plan_price = 9.99;
    public $shopify_trial_days = 14;
    
    public function ValidateForm($fields, $rules, $messages = []){
    	$validator = Validator::make($fields, $rules, $messages)->validate();
    }

    public function curl_request($url="", $data =[], $method="get", $rowData = "", $headers=[]){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if(count($data)){
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        }elseif($rowData != ''){
            curl_setopt($ch, CURLOPT_POSTFIELDS, $rowData);
        }

        switch (strtolower($method)) {
            case 'post':
                curl_setopt($ch, CURLOPT_POST, true);
                break;
            case 'put':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                break;
            case 'delete':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
                break;           
            default:
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
                break;
        }
        
        if(count($headers)){
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        $result = curl_exec($ch);
        if (curl_errno($ch) && config('app.debug')) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        return $result;
    }
    
    public function shopifyCall($token, $shopUrl, $api='', $data=[], $postMethod=true){
        $url = "https://" . $shopUrl . "/admin/api/". $this->shopifyApiV . "/" . $api;

        if($token != ''){
            if(!$postMethod){
                $data = array_merge(['access_token'=>$token], $data);
                $url .= '?'.http_build_query($data);
            }else{
                $url .= "?access_token=". $token;
            }
        }else{
            if(!$postMethod && count($data)){
                $url .= '?'.http_build_query($data);
            }
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, $postMethod);
        if($postMethod){
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        }
        $result = json_decode(curl_exec($ch), true);
        curl_close($ch);        

        return $result;
    }

    public function accessGranted($user = 0){
        if(!$user)
            $user= Auth::user();
        
        $access = [
            'facebook'=>(!is_null($user->facebook_token) && is_array($user->facebook_token) && $user->facebook_token['token'] != ''), 
            'google'=>(!is_null($user->google_token) && is_array($user->google_token) && $user->google_token['token'] != ''), 
            'shopify'=>(!is_null($user->shopify_token) && is_array($user->shopify_token) && $user->shopify_token['token'] != ''),
            'analytics'=>$user->analyticAccounts->count(),
            'pixels'=>$user->pixel()->exists()
        ];
        return $access;
    }

    public function updateCurrency($currencyCode =''){
        if($currencyCode){
            $curr = Currency::where('currency_code', $currencyCode)->first();
            if(!$curr){
                Currency::create(['currency_code'=>$currencyCode, 'active'=>1]);
                $this->updateCurrencyRates(true);
            }
        }
    }

    public function updateCurrencyRates($historical=false){
        $symbols = Currency::pluck('currency_code')->toArray();
        if(count($symbols)){
            $base = 'USD';
            $query = ['access_key'=>config('vars.fixer_key'), 'base'=>$base, 'symbols'=>implode(',', $symbols)];
            if($historical){
                $start = Carbon::now()->subMonths(2);
                $end = Carbon::now();
                while($start <= $end){
                    $url = 'https://data.fixer.io/api/'.$start->format('Y-m-d').'?'.http_build_query($query);
                    $result = json_decode($this->curl_request($url), true);
                    
                    if($result['success'] == true && !array_key_exists('error', $result)){
                        $date = Carbon::parse($result['date']);
                        CurrencyRate::updateOrCreate(['date'=>$date->format('Y-m-d'), 'base'=>$base], ['rates'=>$result['rates']]);
                    }
                    $start->addDay();
                }
            }else{
                $url = 'https://data.fixer.io/api/latest?'.http_build_query($query);
                $result = json_decode($this->curl_request($url), true);
                
                if($result['success'] == true && !array_key_exists('error', $result)){
                    $date = Carbon::parse($result['date']);
                    CurrencyRate::updateOrCreate(['date'=>$date->format('Y-m-d'), 'base'=>$base], ['rates'=>$result['rates']]);
                }
            }
        }
    }

    public function getCurrencyRates($date = ''){
        if(!$date)
            $date = Carbon::now();
        else
            $date = Carbon::parse($date);
        
        $currency = CurrencyRate::where('date', $date->format('Y-m-d'))->first();
        if($currency)
            return $currency->rates;

        return [];
    }
}