<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoogleAnalyticsAccount extends Model
{
    protected $guarded = ['id'];

    protected $casts = ['web_properties'=>'array'];
}
