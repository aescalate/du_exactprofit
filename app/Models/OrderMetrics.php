<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderMetrics extends Model
{
    protected $casts = ['metrics'=>'array'];
}
