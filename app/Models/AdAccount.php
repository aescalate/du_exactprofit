<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdAccount extends Model
{
    protected $guarded = ['id'];

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function pixel(){
    	return $this->hasOne('App\Models\Pixel');
    }
}
