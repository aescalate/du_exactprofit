<?php

function isTokenExist($token, $table, $field) {
	if ($token != '') {
		$q = DB::table($table)->where($field,'=',$token);
		if ($q->count() > 0) {
			return true;
		} else {
			return false;
		}
	} else {
		return true;
	}
}

function formattedArray($array = array(), $shouldExit = true){
	echo "<pre>";
	print_r($array);
	echo '</pre>';
	if($shouldExit)die;
}

function genUniqueStr($prefix='', $table, $field, $length = 10, $isAlphaNum =false, $upperCaseOnly=false) {
	$arr = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];
	if($isAlphaNum){
		$arr = array_merge($arr, array(
		'A', 'B', 'C', 'D', 'E', 'F',
		'G', 'H', 'I', 'J', 'K', 'L',
		'M', 'N', 'O', 'P', 'R', 'S',
		'T', 'U', 'V', 'X', 'Y', 'Z'));

		if(!$upperCaseOnly){
			$arr = $array_merge($arr, array(
				'a', 'b', 'c', 'd', 'e', 'f',
				'g', 'h', 'i', 'j', 'k', 'l',
				'm', 'n', 'o', 'p', 'r', 's',
				't', 'u', 'v', 'x', 'y', 'z',
			));
		}
	}
	

	$token = $prefix;
	$maxLen = max(($length-strlen($prefix)), 0);
	for ($i = 0; $i < $maxLen; $i++) {
		$index = rand(0, count($arr) - 1);
		$token .= $arr[$index];
	}

	if (isTokenExist($token, $table, $field)) {
		genUniqueStr($prefix, $table, $field, $length, $isAlphaNum, $upperCaseOnly);
	} else {
		return $token;
	}
}

function mb_truncate($str, $limit=80) {
	return mb_substr(strip_tags($str), 0, $limit, 'UTF-8') . (mb_strlen($str) > $limit ? '...' : '');
}

function empty_replace($value='', $replacement='N/A'){
	return ($value != '' && $value != NULL ? $value : $replacement); 
}

function removeSpace($string = ''){
	return preg_replace("/\s+/", "", $string);
}

function encryptString($string) {
	
    $output = false;
    if (!$string) {
        return $output;
    }
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'ThiSisTHesEcreTkEyF0RDi9iTalUpri$Ers';
    $secret_iv = 'ThiSisTHesEcreTkEyF0RDi9iTalUpri$Ers';

    // hash
    $key = hash('sha256', $secret_key);
    
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);

    $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
    $output = base64_encode($output);

    return $output;
}

function decryptString($string) {
    $output = false;
    if (! $string) {
        return $output;
    }
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'ThiSisTHesEcreTkEyF0RDi9iTalUpri$Ers';
    $secret_iv = 'ThiSisTHesEcreTkEyF0RDi9iTalUpri$Ers';

    // hash
    $key = hash('sha256', $secret_key);
    
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);

    $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);

    return $output;
}

function validDate($date='', $delimeter='-'){
	$parts = explode( $delimeter, $date);
	if(count($parts) == 3){
	    if(checkdate( $parts[1], $parts[2], $parts[0] )){
	        return true;
	    }
	}

	return false;
}

function getPreferences(){
    return ['notification'=>'yes', 'frequency'=>'daily', 'fields'=>['total_sales', 'visits', 'total_orders', 'cogs', 'net_profit', 'adspend']];
}

function defaultCharges(){
    return [
        'subscription_fee' => 0,
        'cumulative_charge' => 0,
        'transaction_charge' => 0,
    ];
}