<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;

class User extends Authenticatable
{
    use Notifiable, Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'facebook_token'=>'array',
        'google_token'=>'array',
        'onboarding'=>'array',
        'notifications'=>'array',
        'shopify_token' => 'array',
        'mail_settings' => 'array'
    ];

    public function adAccounts(){
        return $this->hasMany('App\Models\AdAccount');
    }

    public function clientCustomers(){
        return $this->hasMany('App\Models\GoogleClientCustomer');
    }

    public function analyticAccounts(){
        return $this->hasMany('App\Models\GoogleAnalyticsAccount');
    }

    public function pixel(){
        return $this->hasOne('App\Models\Pixel');
    }

    public function preferences(){
        return $this->hasOne('App\Models\Preference');
    }

    public function expense(){
        return $this->hasOne('App\Models\Expense');
    }
}
