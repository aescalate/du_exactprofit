<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class InsightsExport implements FromArray, WithHeadings, WithEvents, ShouldAutoSize, WithStrictNullComparison
{
    protected $insights;

	function __construct(array $insights){
		$this->insights = $insights;
	}

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:N1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
            },
        ];
    }


    public function array(): array
    {
        return $this->insights;
    }

    public function headings() : array
    {
        return ['Date', 'Total Campaings', 'Total Clicks', 'CTR', 'Average CPC', 'Visits', 'CPM', 'Impressions', 'Spend', 'Revenue', 'Conversion Rate', 'Gross Profit', 'Net Profit'];
 	}
}
