@extends('layouts.app')

@section('content')
	<div class="container whitebox nopad ">
		<div class="formbox pad30">
			<div class="leftbox">
				<div class="icondiv"><i class="fa fa-gear" aria-hidden="true"></i></div>
			</div>
			<div class="rightbox">
				<div class="headingdiv">
					<h4>Shopify Expenses</h4>
					<p>Enter your expense details.</p>
				</div>
				<form id="frmPref" name="frmPref" action="{{ route('saveExpenses') }}" method="post">
					@csrf
					<div class="questiondiv">
						<div class="textboxdiv m-b-20">
							<label for="inputEmail4">Shopify subscription price (USD):</label>
							<input type="text" class="form-control" name="subscription_fee" placeholder="Shopify plan subscription charge" value="{{ $expenses['subscription_fee'] }}">
						</div>
						<div class="textboxdiv m-b-20">
							<label for="inputEmail4">Cumulative charge of store (USD):</label>
							<input type="text" class="form-control" name="cumulative_charge" placeholder="Cumulative charges" value="{{ $expenses['cumulative_charge'] }}">
						</div>
						<div class="textboxdiv m-b-20">
							<label for="inputEmail4">Transaction charges (%):</label>
							<input type="text" class="form-control" name="transaction_charge" placeholder="Transaction fee per order" value="{{ $expenses['transaction_charge'] }}">
						</div>						
					</div>							
					<button type="submit" class="submit-btn left-align">Save</button>
				</form>
			</div>
		</div>
	</div>	
@endsection

@push('scripts')
	<script type="text/javascript">
		$(function(){
			$('#frmPref').validate({
	            rules: {
	                subscription_fee: {
	                    required: true,
	                    number:true,
	                    min:0
	                },
	                cumulative_charge: {
	                    required: true,
	                    min:0,
	                    number:true
	                },
	                transaction_charge:{
	                    required:true, 
	                    min:0,
	                    number:true
	                }
	            },
	            messages: {
	                subscription_fee: {
	                    required: "@lang('validation.required',['attribute'=>'subscription fee'])",
	                },
	                cumulative_charge: {
	                    required: "@lang('validation.required',['attribute'=>'cumulative charge'])"
	                },
	                transaction_charge:{
	                    required: "@lang('validation.required',['attribute'=>'transaction charge'])"
	                }
	            },
	        });
		});
	</script>
@endpush