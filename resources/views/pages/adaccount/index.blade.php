@extends('layouts.app')

@section('content')
@if (!$accessGranted['facebook'] && !$accessGranted['google'])
    <div class="allowblock">
        <h2>Please allow access to Ad accounts to get more profit analytics.</h2>
        <p>By allowing Facebook/Google ad account access you can unlock below profit analysis and can track your exact profit considering store’s Visitors, Ad spends and Other analytics.
            You can also update/delete this settings later from ad account menu.</p>
        <div class="access-dtl">
            <a href="{{ config('vars.grant_url').'/'.encryptString(Auth::id()) }}" class="btn btn-access m-r-20 {{ $accessGranted['facebook'] ? 'selected' : '' }}"><span class="icn facebook"></span>{{ $accessGranted['facebook'] ? 'Granted' : 'Grant Access' }}<span class="grant"></span></a>
            <a href="{{ config('vars.grant_url').'/'.encryptString(Auth::id()) }}" class="btn btn-access  {{ $accessGranted['google'] ? 'selected' : '' }}"><span class="icn google"></span>{{ $accessGranted['google'] ? 'Granted' : 'Grant Access' }}<span class="grant"></span></a>
        </div>
    </div>
@endif
<form name="frmAcc" id="frmAcc" action="{{ route('adaccounts.updateaccess') }}" method="post">
	@csrf
	<div class="container whitebox nopad ">
		<div class="accordion" id="accordionExample">
			@isset ($accounts['google'])
				<div class="card">
					<div class="card-header" id="headingOne">
					  	<h2 class="mb-0">
							<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							  	Google Accounts
							</button>
					 	</h2>
					 	<span id="g-err"></span>
					</div>
					<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
					  	<div class="card-body">
					  		<a href="#" class="link-btn rem-social" data-tp="google"><i class="fa fa-trash-o" aria-hidden="true"></i> Remove Google Account</a>
					  		@foreach ($accounts['google']['clientCustomers'] as $i=>$client)
					  			<div class="togglerow activerow">
									<label><img src="{{ asset('images/icn_google.png') }}" alt="">{{ $client['descriptiveName'] }}</label>
									<div class="accountbtn">
										<p>07/30/1977</p>
										<div class="togglebutton">
											<label class="toggle-container">
									  			<input name="googleclient[]" id="client_{{ $i }}" type="checkbox" class="real-checkbox" data-error-container="#g-err" value="{{ $client['customer_id'] }}" {{ in_array($client['customer_id'], $accounts['google']['googleClients']) ? 'checked' : '' }}>
									  			<div class="toggle-button"></div>
											</label>
										</div>
									</div>	
								</div>	
					  		@endforeach
					  	</div>
					</div>
				</div>
			@endisset
			@isset ($accounts['facebook'])
				<div class="card">
					<div class="card-header" id="headingTwo">
					  	<h2 class="mb-0">
							<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Faccebook Accounts</button>
					  	</h2>
					  	<span id="f-err"></span>
					</div>
					<div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionExample">
					  	<div class="card-body">
					  		<a href="#" class="link-btn rem-social" data-tp="facebook"><i class="fa fa-trash-o" aria-hidden="true"></i> Remove Facebook Account</a>
					  		@foreach ($accounts['facebook']['adaccounts'] as $i=>$adaccount)
					  			<div class="togglerow">
									<label><img src="{{ asset('images/icn_fb.png') }}" alt="">{{ $adaccount['account_name'] }}</label>
									<div class="accountbtn">
										<p>07/30/1977</p>
										<div class="togglebutton">
											<label class="toggle-container">
											  	<input name="fbaccount[]" type="checkbox" id="ad-{{ $i }}" class="real-checkbox fbacc" data-error-container="#f-err" value="{{ $adaccount['account_id'] }}" {{ in_array($adaccount['account_id'], $accounts['facebook']['fbAdAccounts']) ? 'checked' : '' }}>
											  	<div class="toggle-button"></div>
											</label>
										</div>
									</div>			
							 	</div>
					  		@endforeach
					  	</div>
					</div>
				</div>	    
			@endisset	
		</div>
	</div>
	<div class="container whitebox nopad ">
		@isset ($accounts['facebook'])
			<div class="analyticsrow">
				<span><img src="{{ asset('images/icn_fb.png') }}" alt=""></span>
				<div class="">
					<h2>Select Account for Facebook Pixel</h2>
					<p>The Facebook pixel is code that you place on your website. It collects data that helps you track conversions from Facebook ads, optimize ads, build targeted audiences for future ads, and remarket to people who have already taken some kind of action on your website.</p>
					<div class="dropdown custom-selectbox">
						<select class="selectpicker" data-live-search="true" title="Select Pixel" id="pixels" name="pixels">
							<option value="">Please Select</option>
						</select>
					</div>
				</div>
			</div>
		@endisset
		@isset ($accounts['google'])
			<div class="analyticsrow">
				<span><img src="{{ asset('images/icn_google.png') }}" alt=""></span>
				<div class="">
					<h2>Select Account for Google Analytics</h2>
					<p>Google Analytics lets you measure your advertising ROI as well as track your Flash, video, and social networking sites and applications. Access Google's unique insights and machine learning capabilities to make the most of your data.</p>
					<div class="dropdown custom-selectbox">
					  	<select class="selectpicker" data-live-search="true" title="Select Analytic Account" id="analytics" name="analytics">
							<option value="">Please Select</option>
							@foreach ($accounts['google']['analyticsAccounts'] as $analytic)
								<option {{ in_array($analytic['account_id'], $accounts['google']['googleAnalytics']) ? 'selected="selected"' : '' }} value="{{ $analytic['account_id'] }}">{{ $analytic['name'] }}</option>
							@endforeach
					  	</select>
					</div>
				</div>
			</div>
		@endisset		
	</div>
	@if (isset($accounts['google']) || isset($accounts['facebook']))
		<button type="submit" class="submit-btn"> Submit</button>	
	@endif	
</form>
@endsection

@push('page_css')
	<link rel="stylesheet" href="{{ asset('css/bootstrap-select.css') }}">
@endpush

@push('page_js')
	<script src="{{ asset('js/bootstrap-select.js') }}"></script>
@endpush

@push('scripts')
	<script type="text/javascript">
		var fb = @json(session('facebook'));
		var fbacc = fb ? fb.fbAdAccounts : null;

		function updatePixels(){
			var optpix = ['<option value="">Please Select</option>'];
			fb.adaccounts.forEach(function(account) {
                if(fbacc.includes(account.account_id)){
                    account.pixels.forEach(function(pixel) {
                        optpix.push('<option '+(pixel.pixel_id == fb.pixel ? 'selected="selected"' : '')+' value="'+pixel.pixel_id+'">'+pixel.pixel_name+'</option>');
                    });
                }
            });
            $("#pixels").html(optpix.join(''));
            $("#pixels").selectpicker('refresh');
            $("#pixels").val(fb.pixel);
            $("#pixels").selectpicker('render');
		}

		$(function(){
			$('.custom-selectbox select').selectpicker();
			if(fb)
				updatePixels();
			
			$(document).on("change", ".fbacc", function(){
                var acid = $(this).val();
                if(this.checked){
                    fbacc.push(acid);
                }else{
                    fbacc = $.grep(fbacc, function(value){
                        return value != acid;
                    });
                }
                updatePixels();                
            });

            $("#frmAcc").validate({
                rules:{
                    "googleclient[]":{
                        required:true,
                        minlength:1
                    },
                    "fbaccount[]":{
                        required:true,
                        minlength:1
                    }/*,
                    "pixels":{
                        required:true
                    },
                    "analytics":{
                        required:true
                    }*/
                },
                messages:{
                    "googleclient[]":{
                        required:"Please select atleast one google account"
                    },
                    "fbaccount[]":{
                        required:"Please select atleast one facebook account"
                    }/*,
                    "pixels":{
                        required:"Please select facebook pixel"
                    },
                    "analytics":{
                        required:"Please select google analytics profile"
                    }*/
                }
            });

            $(document).on("click", ".rem-social", function(){
            	var type = $(this).data('tp');
            	bootbox.confirm({
				    message: "Are you sure? you want to completely remove "+type+" account?",
				    buttons: {
				        confirm: {
				            label: 'Yes',
				            className: 'btn-danger'
				        },
				        cancel: {
				            label: 'No',
				            className: 'btn-success'
				        }
				    },
				    callback: function (result) {
				    	if(result){
				    		window.location.href="{{ route('access.remove') }}/"+type
				    	}
				    }
				});
            });
		})
	</script>
@endpush