@extends('layouts.app')

@section('content')
<div class="whitebox profilebox">
    <form name="frmProfileComp" id="frmProfileComp" action="{{ route('complete-profile') }}" method="post">
        @csrf
        <div class="form-group">
            <label for="exampleInputEmail1">Full Name</label>
            <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}" 
            placeholder="Enter Your Full Name">
            @error('name')
                <span class="help-block" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" placeholder="Ie. steve@dummysite.com ">
            <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
            @error('email')
                <span class="help-block" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
            @error('password')
                <span class="help-block" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <!-- <button type="submit" class="btn btn-primary" disabled>Continue</button> -->
        <button type="submit" class="btn btn-primary">Continue</button>
    </form>
</div>

@endsection

@push('scripts')
	<script type="text/javascript">
		$(function(){
			$("#frmProfileComp").validate({
		        ignore:[],
		        rules: {
		            name:{
		                required:true,
		                maxlength:255
		            },
		            email:{
                        required:true,
                        email:true,
                        maxlength:255
                    },
                    password:{
                        required:true,
                        minlength:6,
                    }
		        },
		        messages: {
		            name:{
		                required:"@lang('validation.required',['attribute'=>'full name'])",
		                maxlength:"@lang('validation.max.string',['attribute'=>'full name','max'=>255])"
		            },
		            email:{
		                required:"@lang('validation.required',['attribute'=>'email address'])",
		                maxlength:"@lang('validation.max.string',['attribute'=>'email address','max'=>255])"
		            },
		            password:{
		                required:"@lang('validation.required',['attribute'=>'password'])",
		                minlength:"@lang('validation.max.string',['attribute'=>'password','min'=>6])"
		            }
		        }
		    });
		});
	</script>
@endpush