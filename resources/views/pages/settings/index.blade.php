@extends('layouts.app')
@section('content')
	<div class="container whitebox nopad ">
		<div class="formbox pad30">
			<div class="leftbox">
				<div class="icondiv"><i class="fa fa-envelope" aria-hidden="true"></i></div>
			</div>
			<div class="rightbox">
				<div class="headingdiv">
					<h4>Email Notification Settings</h4>
					<p>Choose when and what things do you want to be notified about.</p>
				</div>
				<form name="frmSettings" id="frmSettings" action="{{ route('settings.update') }}" method="post">
					@csrf
					<div class="questiondiv">
						<p>Do you wish to receive email notification?</p>
						<div class="radiobuttongroup">
							<div class="custom-control custom-radio custom-control-inline">
							  	<input type="radio" id="notification-y" name="notification" class="custom-control-input" value="yes" {{ $preferences['notification'] == 'yes' ? 'checked' : '' }} data-error-container="#err-not">
							  	<label class="custom-control-label" for="notification-y">Yes</label>
							</div>
							<div class="custom-control custom-radio custom-control-inline">
								<input type="radio" id="notification-n" name="notification" class="custom-control-input" value="no" {{ $preferences['notification'] == 'no' ? 'checked' : '' }} data-error-container="#err-not">
								<label class="custom-control-label" for="notification-n">No</label>
							</div>
						</div>
						<span id="err-not"></span>
					</div>
					<div class="questiondiv">
						<p>Do you wish to receive email notification?</p>
						<div class="radiobuttongroup">
							<div class="custom-control custom-radio custom-control-inline">
							  	<input type="radio" id="freq-d" name="frequency" class="custom-control-input" value="daily" {{ $preferences['frequency'] == 'daily' ? 'checked' : '' }}  data-error-container="#freq-err">
							  	<label class="custom-control-label" for="freq-d">Daily</label>
							</div>
							<div class="custom-control custom-radio custom-control-inline">
							  	<input type="radio" id="freq-w" name="frequency" class="custom-control-input" value="weekly" {{ $preferences['frequency'] == 'weekly' ? 'checked' : '' }} data-error-container="#freq-err">
							  	<label class="custom-control-label" for="freq-w">Weekly</label>
							</div>
							<div class="custom-control custom-radio custom-control-inline">
							  	<input type="radio" id="freq-m" name="frequency" class="custom-control-input" value="monthly" {{ $preferences['frequency'] == 'monthly' ? 'checked' : '' }} data-error-container="#freq-err">
							  	<label class="custom-control-label" for="freq-m">Monthly</label>
							</div>
						</div>
						<span id="freq-err"></span>
					</div>
					<div class="questiondiv">
						<p>Select Parameters you wish to receive notification of.</p>
						<div class="checkboxgroup row">
							<div class="custom-control col-sm-6 col-md-4 custom-checkbox">  
								<input type="checkbox" name="fields[]" class="custom-control-input" id="totalorder" value="total_orders" {{ in_array("total_orders", $preferences['fields']) ? 'checked' : '' }}>  
								<label class="custom-control-label " for="totalorder">Total Orders</label>
							</div>
							<div class="custom-control col-sm-6 col-md-4 custom-checkbox">  
								<input type="checkbox" name="fields[]" class="custom-control-input" id="cancelorders" value="cancelled_orders" {{ in_array("cancelled_orders", $preferences['fields']) ? 'checked' : '' }}>  
								<label class="custom-control-label " for="cancelorders">Cancelled Orders</label>  
							</div>
							<div class="custom-control col-sm-6 col-md-4 custom-checkbox">  
								<input type="checkbox" name="fields[]" class="custom-control-input" id="totalsales" value="total_sales" {{ in_array("total_sales", $preferences['fields']) ? 'checked' : '' }} >  
								<label class="custom-control-label " for="totalsales">Total Sales</label>  
							</div>
							<div class="custom-control col-sm-6 col-md-4 custom-checkbox">  
								<input type="checkbox" name="fields[]" class="custom-control-input" id="totaldiscounts" value="discounts" {{ in_array("discounts", $preferences['fields']) ? 'checked' : '' }} >
								<label class="custom-control-label " for="totaldiscounts">Total Discounts</label>  
							</div>
							<div class="custom-control col-sm-6 col-md-4 custom-checkbox">  
								<input type="checkbox" name="fields[]" class="custom-control-input" id="shippingcosts" value="shipping_cost" {{ in_array("shipping_cost", $preferences['fields']) ? 'checked' : '' }} > 
								<label class="custom-control-label " for="shippingcosts">Shipping Costs</label>  
							</div>
							<div class="custom-control col-sm-6 col-md-4 custom-checkbox">  
								<input type="checkbox" name="fields[]" class="custom-control-input" id="cogs" value="cogs" {{ in_array("cogs", $preferences['fields']) ? 'checked' : '' }} >  
								<label class="custom-control-label " for="cogs">COGS</label>  
							</div>
							<div class="custom-control col-sm-6 col-md-4 custom-checkbox">  
								<input type="checkbox" name="fields[]" class="custom-control-input" id="grossmargin" value="gross_margin" {{ in_array("gross_margin", $preferences['fields']) ? 'checked' : '' }} >  
								<label class="custom-control-label " for="grossmargin">Gross Margin</label>  
							</div>
							<div class="custom-control col-sm-6 col-md-4 custom-checkbox">  
								<input type="checkbox" name="fields[]" class="custom-control-input" id="grossprofit" value="gross_profit" {{ in_array("gross_profit", $preferences['fields']) ? 'checked' : '' }} >  
								<label class="custom-control-label " for="grossprofit">Gross Profit</label>  
							</div>
							<div class="custom-control col-sm-6 col-md-4 custom-checkbox">  
								<input type="checkbox" name="fields[]" class="custom-control-input" id="taxes" value="tax" {{ in_array("tax", $preferences['fields']) ? 'checked' : '' }}>
								<label class="custom-control-label " for="taxes">taxes</label>  
							</div>
							<div class="custom-control col-sm-6 col-md-4 custom-checkbox">  
								<input type="checkbox" name="fields[]" class="custom-control-input" id="totalrefund" value="refunds" {{ in_array("refunds", $preferences['fields']) ? 'checked' : '' }} >  
								<label class="custom-control-label " for="totalrefund">Total Refund</label>  
							</div>
							<div class="custom-control col-sm-6 col-md-4 custom-checkbox">  
								<input type="checkbox" name="fields[]" class="custom-control-input" id="avgprdervalue" value="avg_ord_value" {{ in_array("avg_ord_value", $preferences['fields']) ? 'checked' : '' }} >  
								<label class="custom-control-label " for="avgprdervalue">Avg. Order Value</label>  
							</div>
							<div class="custom-control col-sm-6 col-md-4 custom-checkbox">  
								<input type="checkbox" name="fields[]" class="custom-control-input" id="profitperorder" value="profit_per_order" {{ in_array("profit_per_order", $preferences['fields']) ? 'checked' : '' }} >
								<label class="custom-control-label " for="profitperorder">Profit Per Order</label>  
							</div>
							<div class="custom-control col-sm-6 col-md-4 custom-checkbox">  
								<input type="checkbox" name="fields[]" class="custom-control-input" id="netmargin" value="net_margin" {{ in_array("net_margin", $preferences['fields']) ? 'checked' : '' }}>  
								<label class="custom-control-label " for="netmargin">Net Margin</label>  
							</div>
							<div class="custom-control col-sm-6 col-md-4 custom-checkbox">  
								<input type="checkbox" name="fields[]" class="custom-control-input" id="netprofit" value="net_profit" {{ in_array("net_profit", $preferences['fields']) ? 'checked' : '' }} >
								<label class="custom-control-label " for="netprofit">Net Profit</label>  
							</div>
							<div class="custom-control col-sm-6 col-md-4 custom-checkbox">  
								<input type="checkbox" name="fields[]" class="custom-control-input" id="adspend" value="adspend" {{ in_array("adspend", $preferences['fields']) ? 'checked' : '' }} >  
								<label class="custom-control-label " for="adspend">Adspend</label>  
							</div>
							<div class="custom-control col-sm-6 col-md-4 custom-checkbox">  
								<input type="checkbox" name="fields[]" class="custom-control-input" id="adspendperorder" value="adspend_per_order" {{ in_array("adspend_per_order", $preferences['fields']) ? 'checked' : '' }}>  
								<label class="custom-control-label " for="adspendperorder">Adspend per order</label>  
							</div>
							<div class="custom-control col-sm-6 col-md-4 custom-checkbox">  
								<input type="checkbox" name="fields[]" class="custom-control-input" id="cac" value="cac" {{ in_array("cac", $preferences['fields']) ? 'checked' : '' }} >  
								<label class="custom-control-label " for="cac">CAC</label>  
							</div>
							<div class="custom-control col-sm-6 col-md-4 custom-checkbox">  
								<input type="checkbox" name="fields[]" class="custom-control-input" id="purchasefrequency" value="purchase_frequency" {{ in_array("purchase_frequency", $preferences['fields']) ? 'checked' : '' }}>  
								<label class="custom-control-label " for="purchasefrequency">Purchase Frequency</label>  
							</div>
							<div class="custom-control col-sm-6 col-md-4 custom-checkbox">  
								<input type="checkbox" name="fields[]" class="custom-control-input" id="lifetimecustonervalue" value="ltv" {{ in_array("ltv", $preferences['fields']) ? 'checked' : '' }}>  
								<label class="custom-control-label " for="lifetimecustonervalue">Life-time Customer Value</label>  
							</div>
							<div class="custom-control col-sm-6 col-md-4 custom-checkbox">  
								<input type="checkbox" name="fields[]" class="custom-control-input" id="ltvcacration" value="ltv_cac_ratio" {{ in_array("ltv_cac_ratio", $preferences['fields']) ? 'checked' : '' }} >
								<label class="custom-control-label " for="ltvcacration">LTV/CAC Ratio</label>  
							</div>
							<div class="custom-control col-sm-6 col-md-4 custom-checkbox">  
								<input type="checkbox" name="fields[]" class="custom-control-input" id="conversionrate" value="conversion_rate" {{ in_array("conversion_rate", $preferences['fields']) ? 'checked' : '' }} >
								<label class="custom-control-label " for="conversionrate">Conversion Rate</label>  
							</div>
							<div class="custom-control col-sm-6 col-md-4 custom-checkbox">  
								<input type="checkbox" name="fields[]" class="custom-control-input" id="visitors" value="visits" {{ in_array("visits", $preferences['fields']) ? 'checked' : '' }} >  
								<label class="custom-control-label " for="visitors">Visitors</label>  
							</div>
							<div class="custom-control col-sm-6 col-md-4 custom-checkbox">  
								<input type="checkbox" name="fields[]" class="custom-control-input" id="revenuepervisitors" value="revenue_per_visitor" {{ in_array("revenue_per_visitor", $preferences['fields']) ? 'checked' : '' }}>  
								<label class="custom-control-label " for="revenuepervisitors">Revenue Per Visitors</label>  
							</div>
							<div class="custom-control col-sm-6 col-md-4 custom-checkbox">  
								<input type="checkbox" name="fields[]" class="custom-control-input" id="netprofitpervisitors" value="net_prof_per_visitor" {{ in_array("net_prof_per_visitor", $preferences['fields']) ? 'checked' : '' }} >  
								<label class="custom-control-label " for="netprofitpervisitors">Net Profit Per Visitors</label>  
							</div>
							<div class="custom-control col-sm-6 col-md-4 custom-checkbox">  
								<input type="checkbox" name="fields[]" class="custom-control-input" id="ros" value="roas" {{ in_array("roas", $preferences['fields']) ? 'checked' : '' }} >  
								<label class="custom-control-label " for="ros">ROAS</label>  
							</div>
							<div class="custom-control col-sm-6 col-md-4 custom-checkbox">  
								<input type="checkbox" name="fields[]" class="custom-control-input" id="avgordprof" value="avg_ord_profit" {{ in_array("avg_ord_profit", $preferences['fields']) ? 'checked' : '' }} >  
								<label class="custom-control-label " for="avgordprof">Average Order Profit</label>  
							</div>
							<div class="custom-control col-sm-6 col-md-4 custom-checkbox">  
								<input type="checkbox" name="fields[]" class="custom-control-input" id="avgordcost" value="avg_ord_cost" {{ in_array("avg_ord_cost", $preferences['fields']) ? 'checked' : '' }} >  
								<label class="custom-control-label " for="avgordcost">Average Order Cost</label>  
							</div>
						</div>
					</div>
					<button type="submit" role="menuitem" class="submit-btn">Save</button>
				</form>
			</div>
		</div>				
		<div class="resetpassword">				
			<div class="formbox pad30">
				<div class="leftbox">
					<div class="icondiv">
						<i class="fa fa-lock" aria-hidden="true"></i>
					</div>
				</div>
				<div class="rightbox">
					<div class="headingdiv">
						<h4>Reset Password</h4>
						<p>Password should be minimum of 8 characters including at least 1 numeric and 1 alphabet.</p>
					</div>
					<form name="frmChange" id="frmChange" action="{{ route('profile.changepassword') }}" method="post" >
						@csrf
						<div class="form-row">
							<div class="textboxdiv">
								<label for="password">Old Password</label>
						      	<input type="password" name="old" id="old" class="form-control" placeholder="Enter Old Password">
						      	@error('old')
				                    <span class="help-block" role="alert">
				                        <strong>{{ $message }}</strong>
				                    </span>
				                @enderror
						    </div>
						    <div class="textboxdiv">
								<label for="password">Enter Password</label>
						      	<input type="password" name="password" id="password" class="form-control" placeholder="New Password">
						      	@error('password')
				                    <span class="help-block" role="alert">
				                        <strong>{{ $message }}</strong>
				                    </span>
				                @enderror
						    </div>
						    <div class="textboxdiv">
								<label for="password_confirmation">Confirm Password</label>
						      	<input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Re-enter Password">
	                            @error('password_confirmation')
				                    <span class="help-block" role="alert">
				                        <strong>{{ $message }}</strong>
				                    </span>
				                @enderror
						    </div>
						    <div class="textboxdiv">
								<label for="inputEmail4">&nbsp;</label>
								<button type="submit" class="form-control btn btn-primary">Set Password</button>
						    </div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="logout_section">
			<button onclick="event.preventDefault(); document.getElementById('logout-form').submit();" type="button" class="form-control btn btn-secondary">Logout</button>
		</div>
		<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	        @csrf
	    </form>
	</div>
@endsection

@push('scripts')
	<script type="text/javascript">
		$(function(){
			$('#frmChange').validate({
	            rules: {
	                old: {
	                    required: true,
	                },
	                password: {
	                    required: true,minlength:6,
	                },
	                password_confirmation:{
	                    equalTo:"#password"
	                }
	            },
	            messages: {
	                old: {
	                    required: "@lang('validation.required',['attribute'=>'old password'])",
	                    minlength:"@lang('validation.min.string',['attribute'=>'old password','min'=>6])"
	                },
	                password: {
	                    required: "@lang('validation.required',['attribute'=>'password'])",
	                    minlength:"@lang('validation.min.string',['attribute'=>'password','min'=>6])"
	                },
	                password_confirmation:{
	                    equalTo:"@lang('validation.same',['attribute'=>'password','other'=>'confirm password'])"
	                }
	            },
	        });

	        $('#frmSettings').validate({
	            rules: {
	                notification: {
	                    required: true,
	                },
	                frequency: {
	                    required: true,
	                },
	                'fields[]': {
	                    required: true,
	                    minlength:1
	                },
	            },
	            messages: {
	                notification: {
	                    required: "Please give your preference",
	                },
	                frequency: {
	                    required: "Please select the notification frequency.",
	                },
	                'fields[]':{
	                    required:"Please select the fields you want in report"
	                }
	            },
	        });
		});
	</script>
@endpush