@extends('layouts.app')

@section('title_bar')
    <div class="subtitlebar">
        <h3 class="col-xs-8 col-sm-8">Product Details</h3>
    </div>
@endsection

@section('content')
    <div class="table-responsive">
        <table class="display table table-striped table-bordered variant-table" id="variants">
            <thead>
              <tr>
                <th>Variant ID</th>
                <th>Name</th>
                <th>Handling Fee ({{ $currency }})</th>
                <th>COGS ({{ $currency }})</th>
                <th class="tabledit-toolbar-column">Action</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($product->variants as $variant)
                    <tr>
                        <td>{{ $variant->variant_id }}</td>
                        <td>{{ $variant->title }}</td>
                        <td>{{ $variant->handling_fee }}</td>
                        <td>{{ $variant->base_price }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection

@push('page_js')
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/responsive.bootstrap.min.js') }}"></script>  
    <script src="{{ asset('plugins/tabledit/jquery.tabledit.js') }}"></script>    
@endpush

@push('page_css')
    <link href="{{ asset('css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" />
@endpush

@push('scripts')
	<script type="text/javascript">
		$(function(){            
			$('#variants').Tabledit({
                url:"{{ route('products.updatevars') }}",
                columns: {
                    identifier: [0, 'id'],                    
                    editable: [[2, 'handling'], [3, 'base']],                    
                },
                buttons: {
                    edit: {
                        html: ''
                    },
                    save: {
                        html: ''
                    }
                },
                deleteButton:false
            });

            $('#variants').DataTable({
                columnDefs: [
                    { orderable: false, targets: 4 }
                ],
                paging: true,
                searching: true,
                ordering:  true,
                filter:true,
                pageLength :10,
                lengthMenu: [[10, 20, 50, -1], [10, 20, 50, 'All']],
                "bInfo" : false,
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-6"l><"col-sm-6"p>>'
            });
            $("#variants_wrapper .float-left").append("<h5 class='table-heading'>Product Variants</h5>");
            $(".dataTables_filter input").attr("placeholder", "Search Product");
		});
	</script>
@endpush