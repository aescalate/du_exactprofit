@extends('layouts.app')

@section('content')
    <div class="whitebox nopad">
        <table id="product_table" class="table table-striped table-bordered nowrap" style="width:100%">
            <thead>
                <tr>
                    <th>Product Name</th>
                    <th>Product Id</th>
                    <th>Variants</th>
                    <th>Handling Fee ({{ $currency }})</th>
                    <th>COGS ({{ $currency }})</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($products as $product)
                    <tr>
                        <td><a href="{{ route('products.details', $product->product_id) }}">
                            <div class="product-dtl">
                                <img src="{{ $product->image_url }}" alt="{{ $product->product_name }}"/>
                                <span>{{ $product->product_name }}</span>
                            </div>
                        </a></td>
                        <td>{{ $product->product_id }}</td>
                        <td>{{ $product->variants_count }}</td>
                        @if ($product->variants_count == 1)
                            <td>{{ $product->variants[0]->handling_fee }}</td>
                            <td>{{ $product->variants[0]->base_price }}</td>    
                        @else
                            <td> - </td>
                            <td> - </td>    
                        @endif
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div> 
@endsection

@push('page_js')
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/responsive.bootstrap.min.js') }}"></script>  
@endpush

@push('page_css')
    <link href="{{ asset('css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" />
@endpush

@push('scripts')
	<script type="text/javascript">
		$(function(){
			$('#product_table').DataTable( {
                //paging: false,
                //searching: false,
                // ordering:  false,
                pageLength :10,
                lengthMenu: [[10, 20, 50, -1], [10, 20, 50, 'All']],
                "bInfo" : false,
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-6"l><"col-sm-6"p>>',
                responsive: true
            });
            
            $("#product_table_wrapper .float-left").append("<h2 class='table-heading'>Product Data</h2>");
            $(".dataTables_filter input").attr("placeholder", "Search Product");
		});
	</script>
@endpush