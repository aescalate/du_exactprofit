<div class="info-bar bdr-b-1 prdct-dtl">
    <div class="container">
       	<div class="float-left">
			<a href="{{ route('products.index') }}">
				<i class="fa fa-arrow-left"></i>
			</a>
			<div class="product-info">
				<div class="img-thumb">
					<img src="{{ $product->image_url }}" alt="{{ $product->product_name }}">
				</div>
				<div class="img-dtl">
					<h5>{{ $product->product_name }}</h5>
					<span>Product ID: </span><span>#{{ $product->product_id }}</span>
				</div>
			</div>
		</div>
		<div class="float-right">
			<span class="date">{{ $product->updated_at->format($date_time_format) }}</span>
			<span class="footnote">Last Updated</span>
		</div>
    </div>
</div>