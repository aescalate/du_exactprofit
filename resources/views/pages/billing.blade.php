@extends('layouts.auth')

@section('content')
<div class="auth-box login-box">
    <!-- Start row -->
    <div class="row no-gutters align-items-center justify-content-center">
        <!-- Start col -->
        <div class="col-md-6 col-lg-5">
            <!-- Start Auth Box -->
            <div class="auth-box-right">
                <div class="card">
                     <div class="card-header" style=" background: #3d59d9;">
                        <a href="index.html" class="logo"><img src="images/logo_white.png" class="img-fluid" alt="logo"></a>
                    </div>
                    <div class="card-body">
                        <form name="frmBill" id="frmBill" action="{{ route('shopify.do-subscribe') }}" method="post">
                            @csrf                                       
                            <h4 class="text-primary my-4 mt-0">Complete Payment</h4>
                            <button type="submit" class="btn btn-success btn-lg btn-block font-18">
                                @if ($shouldGiveTrial)
                                    Continue {{ $trialDays }} day trial
                                @else
                                    Continue to subscription
                                @endif                                
                            </button>
                        </form>
                    </div>
                </div>
            </div>
            <!-- End Auth Box -->
        </div>
        <!-- End col -->
    </div>
    <!-- End row -->
</div>
@endsection