    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 matrix_listing">
	    <div class="card m-b-30">
	        <div class="card-header">                                
                <h5 class="card-title">Total Orders 
                    @if ($percent['total_orders']>0)
                        <span class="badge badge-success"><i class="feather-trending-up"></i>{{ number_format($percent['total_orders'], 2) }}%</span>
                    @else
                        <span class="badge badge-warning"><i class="feather-trending-down"></i>{{ number_format($percent['total_orders'], 2) }}%</span>
                    @endif
                </h5>
	            <h4>{{ $row['total_orders'] }}</h4>
	        </div>
	        <div class="card-body">
	           <div id="total_orders"></div>
	        </div>
	    </div>
	</div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 matrix_listing">
        <div class="card m-b-30">
            <div class="card-header">                                
                <h5 class="card-title">Cancelled Orders
                    @if ($percent['cancelled_orders']>0)
                        <span class="badge badge-warning"><i class="feather-trending-up"></i>{{  number_format($percent['cancelled_orders'], 2)}}%</span>
                    @else
                        <span class="badge badge-success"><i class="feather-trending-down"></i>{{  number_format($percent['cancelled_orders'], 2) }}%</span>
                    @endif
                </h5>
                <h4>{{ $row['cancelled_orders'] }}</h4>
            </div>
            <div class="card-body">
                <div id="cancelled_orders"></div>
            </div>
        </div>
    </div>
	{{-- <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 matrix_listing">
        <div class="card m-b-30">
            <div class="card-header">                                
                <h5 class="card-title">Total Sales
                    @if ($percent['total_sales']>0)
                        <span class="badge badge-success"><i class="feather-trending-up"></i>{{  number_format($percent['total_sales'], 2) }}%</span>
                    @else
                        <span class="badge badge-warning"><i class="feather-trending-down"></i>{{  number_format($percent['total_sales'], 2) }}%</span>
                    @endif
                </h5>
                <h4>{{ number_format($row['total_sales'], 2) }} {{ $currency }}</h4>
            </div>
            <div class="card-body">
                <div class="graph_view">
                    <div id="total_sales"></div>
                </div>                                
            </div>
        </div>
    </div> --}}
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 matrix_listing">
        <div class="card m-b-30">
            <div class="card-header">                                
                <h5 class="card-title">Total Sales
                    @if ($percent['revenue']>0)
                        <span class="badge badge-success"><i class="feather-trending-up"></i>{{  number_format($percent['revenue'], 2) }}%</span>
                    @else
                        <span class="badge badge-warning"><i class="feather-trending-down"></i>{{  number_format($percent['revenue'], 2) }}%</span>
                    @endif
                </h5>
                <h4>{{ number_format($row['revenue'], 2) }} {{ $currency }}</h4>
            </div>
            <div class="card-body">
                <div class="graph_view">
                    <div id="revenue"></div>
                </div>                                
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 matrix_listing">
        <div class="card m-b-30">
            <div class="card-header">                                
                <h5 class="card-title">Total Discounts
                    @if ($percent['discounts']>0)
                        <span class="badge badge-success"><i class="feather-trending-up"></i>{{  number_format($percent['discounts'], 2) }}%</span>
                    @else
                        <span class="badge badge-warning"><i class="feather-trending-down"></i>{{  number_format($percent['discounts'], 2) }}%</span>
                    @endif
                </h5>
                <h4>{{ number_format($row['discounts'], 2) }} {{ $currency }}</h4>
            </div>
            <div class="card-body">
                <div class="graph_view">
                	<div id="discounts"></div>
                </div>                                
            </div>
        </div>
    </div>
    {{-- <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 matrix_listing">
        <div class="card m-b-30">
            <div class="card-header">                                
                <h5 class="card-title">Revenue
                    @if ($percent['revenue']>0)
                        <span class="badge badge-success"><i class="feather-trending-up"></i>{{  number_format($percent['revenue'], 2) }}%</span>
                    @else
                        <span class="badge badge-warning"><i class="feather-trending-down"></i>{{  number_format($percent['revenue'], 2) }}%</span>
                    @endif
                </h5>
                <h4>{{ number_format($row['revenue'], 2) }} {{ $currency }}</h4>
            </div>
            <div class="card-body">
                <div class="graph_view">
                	<div id="revenue"></div>
                </div>                                
            </div>
        </div>
    </div> --}}
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 matrix_listing">
         <div class="card m-b-30">
            <div class="card-header">                                
                <h5 class="card-title">Shipping Cost
                    @if ($percent['shipping_cost']>0)
                        <span class="badge badge-success"><i class="feather-trending-up"></i>{{  number_format($percent['shipping_cost'], 2) }}%</span>
                    @else
                        <span class="badge badge-warning"><i class="feather-trending-down"></i>{{  number_format($percent['shipping_cost'], 2) }}%</span>
                    @endif
                </h5>
                <h4>{{ number_format($row['shipping_cost'], 2) }} {{ $currency }}</h4>
            </div>
            <div class="card-body">
                <div class="graph_view">
                	<div id="shipping_cost"></div>
                </div>                                
            </div>
    	</div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 matrix_listing">
         <div class="card m-b-30">
            <div class="card-header">                                
                <h5 class="card-title">COGS
                    @if ($percent['cogs']>0)
                        <span class="badge badge-success"><i class="feather-trending-up"></i>{{  number_format($percent['cogs'], 2) }}%</span>
                    @else
                        <span class="badge badge-warning"><i class="feather-trending-down"></i>{{  number_format($percent['cogs'], 2) }}%</span>
                    @endif
                </h5>
                <h4>{{ number_format($row['cogs'], 2) }} {{ $currency }}</h4>
            </div>
            <div class="card-body">
                <div class="graph_view">
                    <div id="cogs"></div>
                </div>                                
            </div>
    	</div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 matrix_listing">
         <div class="card m-b-30">
            <div class="card-header">                                
                <h5 class="card-title">Gross Margin
                    @if ($percent['gross_margin']>0)
                        <span class="badge badge-success"><i class="feather-trending-up"></i>{{  number_format($percent['gross_margin'], 2) }}%</span>
                    @else
                        <span class="badge badge-warning"><i class="feather-trending-down"></i>{{  number_format($percent['gross_margin'], 2) }}%</span>
                    @endif
                </h5>
                <h4>{{ number_format($row['gross_margin'], 2) }}%</h4>
            </div>
            <div class="card-body">
                <div class="graph_view">
                	<div id="gross_margin"></div>
                </div>                                
            </div>
    	</div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 matrix_listing">
         <div class="card m-b-30">
            <div class="card-header">                                
                <h5 class="card-title">Gross Profit
                    @if ($percent['gross_profit']>0)
                        <span class="badge badge-success"><i class="feather-trending-up"></i>{{  number_format($percent['gross_profit'], 2) }}%</span>
                    @else
                        <span class="badge badge-warning"><i class="feather-trending-down"></i>{{  number_format($percent['gross_profit'], 2) }}%</span>
                    @endif
                </h5>
                <h4>{{ number_format($row['gross_profit'], 2) }} {{ $currency }}</h4>
            </div>
            <div class="card-body">
                <div class="graph_view">
                	<div id="gross_profit"></div>
                </div>                                
            </div>
    	</div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 matrix_listing">
         <div class="card m-b-30">
            <div class="card-header">                                
                <h5 class="card-title">Taxes
                    @if ($percent['tax']>0)
                        <span class="badge badge-success"><i class="feather-trending-up"></i>{{  number_format($percent['tax'], 2) }}%</span>
                    @else
                        <span class="badge badge-warning"><i class="feather-trending-down"></i>{{  number_format($percent['tax'], 2) }}%</span>
                    @endif
                </h5>
                <h4>{{ number_format($row['tax'], 2) }} {{ $currency }}</h4>
            </div>
            <div class="card-body">
                <div class="graph_view">
                	<div id="tax"></div>
                </div>                                
            </div>
    	</div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 matrix_listing">
         <div class="card m-b-30">
            <div class="card-header">                                
                <h5 class="card-title">Total Refund
                    @if ($percent['refunds']>0)
                        <span class="badge badge-warning"><i class="feather-trending-up"></i>{{  number_format($percent['refunds'], 2) }}%</span>
                    @else
                        <span class="badge badge-success"><i class="feather-trending-down"></i>{{  number_format($percent['refunds'], 2) }}%</span>
                    @endif
                </h5>
                <h4>{{ number_format($row['refunds'], 2) }} {{ $currency }}</h4>
            </div>
            <div class="card-body">
                <div class="graph_view">
                	<div id="refunds"></div>
                </div>                                
            </div>
    	</div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 matrix_listing">
         <div class="card m-b-30">
            <div class="card-header">                                
                <h5 class="card-title">Avg. Order Value
                    @if ($percent['avg_ord_value']>0)
                        <span class="badge badge-success"><i class="feather-trending-up"></i>{{  number_format($percent['avg_ord_value'], 2) }}%</span>
                    @else
                        <span class="badge badge-warning"><i class="feather-trending-down"></i>{{  number_format($percent['avg_ord_value'], 2) }}%</span>
                    @endif
                </h5>
                <h4>{{ number_format($row['avg_ord_value'], 2) }} {{ $currency }}</h4>
            </div>
            <div class="card-body">
                <div class="graph_view">
                	<div id="avg_ord_value"></div>
                </div>                                
            </div>
    	</div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 matrix_listing">
         <div class="card m-b-30">
            <div class="card-header">                                
                <h5 class="card-title">Profit Per Order
                    @if ($percent['profit_per_order']>0)
                        <span class="badge badge-success"><i class="feather-trending-up"></i>{{  number_format($percent['profit_per_order'], 2) }}%</span>
                    @else
                        <span class="badge badge-warning"><i class="feather-trending-down"></i>{{  number_format($percent['profit_per_order'], 2) }}%</span>
                    @endif
                </h5>
                <h4>{{ number_format($row['profit_per_order'], 2) }} {{ $currency }}</h4>
            </div>
            <div class="card-body">
                <div class="graph_view">
                	<div id="profit_per_order"></div>
                </div>                                
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 matrix_listing">
         <div class="card m-b-30">
            <div class="card-header">                                
                <h5 class="card-title">Net Margin
                    @if ($percent['net_margin']>0)
                        <span class="badge badge-success"><i class="feather-trending-up"></i>{{  number_format($percent['net_margin'], 2) }}%</span>
                    @else
                        <span class="badge badge-warning"><i class="feather-trending-down"></i>{{  number_format($percent['net_margin'], 2) }}%</span>
                    @endif
                </h5>
                <h4>{{ number_format($row['net_margin'], 2) }}</h4>
            </div>
            <div class="card-body">
                <div class="graph_view">
                	<div id="net_margin"></div>
                </div>                                
            </div>
    	</div>
    </div>
    @if (!$accessGranted['facebook'] && !$accessGranted['google'])
        <div class="allowblock">
            <h2>Please allow access to Ad accounts to get more profit analytics.</h2>
            <p>By allowing Facebook/Google ad account access you can unlock below profit analysis and can track your exact profit considering store’s Visitors, Ad spends and Other analytics.
                You can also update/delete this settings later from ad account menu.</p>
                <div class="access-dtl">
                    <a href="{{ config('vars.grant_url').'/'.encryptString(Auth::id()) }}" class="btn btn-access m-r-20 {{ $accessGranted['facebook'] ? 'selected' : '' }}"><span class="icn facebook"></span>{{ $accessGranted['facebook'] ? 'Granted' : 'Grant Access' }}<span class="grant"></span></a>
                    <a href="{{ config('vars.grant_url').'/'.encryptString(Auth::id()) }}" class="btn btn-access  {{ $accessGranted['google'] ? 'selected' : '' }}"><span class="icn google"></span>{{ $accessGranted['google'] ? 'Granted' : 'Grant Access' }}<span class="grant"></span></a>
                </div>
        </div>
    @endif
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 matrix_listing">
         <div class="card m-b-30">
            <div class="card-header">                                
                <h5 class="card-title">Adspend
                    @if ($percent['adspend']>0)
                        <span class="badge badge-success"><i class="feather-trending-up"></i>{{  number_format($percent['adspend'], 2) }}%</span>
                    @else
                        <span class="badge badge-warning"><i class="feather-trending-down"></i>{{  number_format($percent['adspend'], 2) }}%</span>
                    @endif
                </h5>
                <h4>{{ number_format($row['adspend'], 2) }} {{ $currency }}</h4>
            </div>
            <div class="card-body">
                <div class="graph_view">
                    <div id="adspend"></div>
                </div>                                
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 matrix_listing">
         <div class="card m-b-30">
            <div class="card-header">                                
                <h5 class="card-title">Adspend per Order
                    @if ($percent['adspend_per_order']>0)
                        <span class="badge badge-success"><i class="feather-trending-up"></i>{{  number_format($percent['adspend_per_order'], 2) }}%</span>
                    @else
                        <span class="badge badge-warning"><i class="feather-trending-down"></i>{{  number_format($percent['adspend_per_order'], 2) }}%</span>
                    @endif
                </h5>
                <h4>{{ number_format($row['adspend_per_order'], 2) }} {{ $currency }}</h4>
            </div>
            <div class="card-body">
                <div class="graph_view">
                	<div id="adspend_per_order"></div>
                </div>                                
            </div>
    	</div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 matrix_listing">
         <div class="card m-b-30">
            <div class="card-header">                                
                <h5 class="card-title">CAC
                    @if ($percent['cac']>0)
                        <span class="badge badge-warning"><i class="feather-trending-up"></i>{{  number_format($percent['cac'], 2) }}%</span>
                    @else
                        <span class="badge badge-success"><i class="feather-trending-down"></i>{{  number_format($percent['cac'], 2) }}%</span>
                    @endif
                </h5>
                <h4>{{ number_format($row['cac'], 2) }} {{ $currency }}</h4>
            </div>
            <div class="card-body">
                <div class="graph_view">
                	<div id="cac"></div>
                </div>                                
            </div>
    	</div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 matrix_listing">
         <div class="card m-b-30">
            <div class="card-header">                                
                <h5 class="card-title">ROAS
                    @if ($percent['roas']>0)
                        <span class="badge badge-success"><i class="feather-trending-up"></i>{{  number_format($percent['roas'], 2) }}%</span>
                    @else
                        <span class="badge badge-warning"><i class="feather-trending-down"></i>{{  number_format($percent['roas'], 2) }}%</span>
                    @endif
                </h5>
                <h4>{{ number_format($row['roas'], 2) }}x</h4>
            </div>
            <div class="card-body">
                <div class="graph_view">
                    <div id="roas"></div>
                </div>                                
            </div>
        </div>
    </div>
    @if (($accessGranted['facebook'] || $accessGranted['google']) && (!$accessGranted['pixels'] && !$accessGranted['analytics']))
        <div class="allowblock">
            <h2>Please allow access to Pixel/Analytics to get more profit analytics.</h2>
            <p>By allowing Facebook/Google ad account access you can unlock below profit analysis and can track your exact profit considering store’s Visitors, Ad spends and Other analytics.
                You can also update/delete this settings later from ad account menu.</p>
                <div class="access-dtl">
                    <a href="{{ $accessGranted['facebook'] ? route('adaccounts.index') : config('vars.grant_url').'/'.encryptString(Auth::id()) }}" class="btn btn-access m-r-20 {{ $accessGranted['facebook'] ? 'selected' : '' }}"><span class="icn facebook"></span>{{ $accessGranted['facebook'] ? 'Granted' : 'Grant Access' }}<span class="grant"></span></a>
                    <a href="{{ $accessGranted['google'] ? route('adaccounts.index') : config('vars.grant_url').'/'.encryptString(Auth::id()) }}" class="btn btn-access  {{ $accessGranted['google'] ? 'selected' : '' }}"><span class="icn google"></span>{{ $accessGranted['google'] ? 'Granted' : 'Grant Access' }}<span class="grant"></span></a>
                </div>
        </div>
    @endif
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 matrix_listing">
         <div class="card m-b-30">
            <div class="card-header">                                
                <h5 class="card-title">Visitors
                    @if ($percent['visits']>0)
                        <span class="badge badge-success"><i class="feather-trending-up"></i>{{  number_format($percent['visits'], 2) }}%</span>
                    @else
                        <span class="badge badge-warning"><i class="feather-trending-down"></i>{{  number_format($percent['visits'], 2) }}%</span>
                    @endif
                </h5>
                <h4>{{ number_format($row['visits'], 0) }}</h4>
            </div>
            <div class="card-body">
                <div class="graph_view">
                    <div id="visits"></div>
                </div>                                
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 matrix_listing">
         <div class="card m-b-30">
            <div class="card-header">                                
                <h5 class="card-title">Revenue per Visitor
                    @if ($percent['revenue_per_visitor']>0)
                        <span class="badge badge-success"><i class="feather-trending-up"></i>{{  number_format($percent['revenue_per_visitor'], 2) }}%</span>
                    @else
                        <span class="badge badge-warning"><i class="feather-trending-down"></i>{{  number_format($percent['revenue_per_visitor'], 2) }}%</span>
                    @endif
                </h5>
                <h4>{{ number_format($row['revenue_per_visitor'], 2) }} {{ $currency }}</h4>
            </div>
            <div class="card-body">
                <div class="graph_view">
                    <div id="revenue_per_visitor"></div>
                </div>                                
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 matrix_listing">
         <div class="card m-b-30">
            <div class="card-header">                                
                <h5 class="card-title">Net Profit
                    @if ($percent['net_profit']>0)
                        <span class="badge badge-success"><i class="feather-trending-up"></i>{{  number_format($percent['net_profit'], 2) }}%</span>
                    @else
                        <span class="badge badge-warning"><i class="feather-trending-down"></i>{{  number_format($percent['net_profit'], 2) }}%</span>
                    @endif
                </h5>
                <h4>{{ number_format($row['net_profit'], 2) }} {{ $currency }}</h4>
            </div>
            <div class="card-body">
                <div class="graph_view">
                    <div id="net_profit"></div>
                </div>                                
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 matrix_listing">
         <div class="card m-b-30">
            <div class="card-header">                                
                <h5 class="card-title">Net Profit per Visitor
                    @if ($percent['net_prof_per_visitor']>0)
                        <span class="badge badge-success"><i class="feather-trending-up"></i>{{  number_format($percent['net_prof_per_visitor'], 2) }}%</span>
                    @else
                        <span class="badge badge-warning"><i class="feather-trending-down"></i>{{  number_format($percent['net_prof_per_visitor'], 2) }}%</span>
                    @endif
                </h5>
                <h4>{{ number_format($row['net_prof_per_visitor'], 2) }} {{ $currency }}</h4>
            </div>
            <div class="card-body">
                <div class="graph_view">
                    <div id="net_prof_per_visitor"></div>
                </div>                                
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 matrix_listing">
         <div class="card m-b-30">
            <div class="card-header">                                
                <h5 class="card-title">Purchase Frequncy
                    @if ($percent['purchase_frequency']>0)
                        <span class="badge badge-success"><i class="feather-trending-up"></i>{{  number_format($percent['purchase_frequency'], 2) }}%</span>
                    @else
                        <span class="badge badge-warning"><i class="feather-trending-down"></i>{{  number_format($percent['purchase_frequency'], 2) }}%</span>
                    @endif
                </h5>
                <h4>{{ number_format($row['purchase_frequency'], 2) }}</h4>
            </div>
            <div class="card-body">
                <div class="graph_view">
                    <div id="purchase_frequency"></div>
                </div>                                
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 matrix_listing">
         <div class="card m-b-30">
            <div class="card-header">                                
                <h5 class="card-title">Lifetime Customer Value
                    @if ($percent['ltv']>0)
                        <span class="badge badge-success"><i class="feather-trending-up"></i>{{  number_format($percent['ltv'], 2) }}%</span>
                    @else
                        <span class="badge badge-warning"><i class="feather-trending-down"></i>{{  number_format($percent['ltv'], 2) }}%</span>
                    @endif
                </h5>
                <h4>{{ number_format($row['ltv'], 2) }} {{ $currency }}</h4>
            </div>
            <div class="card-body">
                <div class="graph_view">
                    <div id="ltv"></div>
                </div>                                
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 matrix_listing">
         <div class="card m-b-30">
            <div class="card-header">                                
                <h5 class="card-title">Conversion Rate
                    @if ($percent['conversion_rate']>0)
                        <span class="badge badge-success"><i class="feather-trending-up"></i>{{  number_format($percent['conversion_rate'], 2) }}%</span>
                    @else
                        <span class="badge badge-warning"><i class="feather-trending-down"></i>{{  number_format($percent['conversion_rate'], 2) }}%</span>
                    @endif
                </h5>
                <h4>{{ number_format($row['conversion_rate'], 2) }}%</h4>
            </div>
            <div class="card-body">
                <div class="graph_view">
                    <div id="conversion_rate"></div>
                </div>                                
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 matrix_listing">
         <div class="card m-b-30">
            <div class="card-header">                                
                <h5 class="card-title">LTV/CAC Ratio
                    @if ($percent['ltv_cac_ratio']>0)
                        <span class="badge badge-success"><i class="feather-trending-up"></i>{{  number_format($percent['ltv_cac_ratio'], 2) }}%</span>
                    @else
                        <span class="badge badge-warning"><i class="feather-trending-down"></i>{{  number_format($percent['ltv_cac_ratio'], 2) }}%</span>
                    @endif
                </h5>
                <h4>{{ number_format($row['ltv_cac_ratio'], 2) }}</h4>
            </div>
            <div class="card-body">
                <div class="graph_view">
                    <div id="ltv_cac_ratio"></div>
                </div>                                
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 matrix_listing">
         <div class="card m-b-30">
            <div class="card-header">                                
                <h5 class="card-title">Avg. Ord. Profit
                    @if ($percent['avg_ord_profit']>0)
                        <span class="badge badge-success"><i class="feather-trending-up"></i>{{  number_format($percent['avg_ord_profit'], 2) }}%</span>
                    @else
                        <span class="badge badge-warning"><i class="feather-trending-down"></i>{{  number_format($percent['avg_ord_profit'], 2) }}%</span>
                    @endif
                </h5>
                <h4>{{ number_format($row['avg_ord_profit'], 2) }} {{ $currency }}</h4>
            </div>
            <div class="card-body">
                <div class="graph_view">
                    <div id="avg_ord_profit"></div>
                </div>                                
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 matrix_listing">
         <div class="card m-b-30">
            <div class="card-header">                                
                <h5 class="card-title">Avg. Ord. Cost
                    @if ($percent['avg_ord_cost']>0)
                        <span class="badge badge-success"><i class="feather-trending-up"></i>{{  number_format($percent['avg_ord_cost'], 2) }}%</span>
                    @else
                        <span class="badge badge-warning"><i class="feather-trending-down"></i>{{  number_format($percent['avg_ord_cost'], 2) }}%</span>
                    @endif
                </h5>
                <h4>{{ number_format($row['avg_ord_cost'], 2) }} {{ $currency }}</h4>
            </div>
            <div class="card-body">
                <div class="graph_view">
                    <div id="avg_ord_cost"></div>
                </div>                                
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 matrix_listing">
         <div class="card m-b-30">
            <div class="card-header">                                
                <h5 class="card-title">Exact Profit
                    @if ($percent['exact_profit']>0)
                        <span class="badge badge-success"><i class="feather-trending-up"></i>{{  number_format($percent['exact_profit'], 2) }}%</span>
                    @else
                        <span class="badge badge-warning"><i class="feather-trending-down"></i>{{  number_format($percent['exact_profit'], 2) }}%</span>
                    @endif
                </h5>
                <h4>{{ number_format($row['exact_profit'], 2) }} {{ $currency }}</h4>
            </div>
            <div class="card-body">
                <div class="graph_view">
                    <div id="exact_profit"></div>
                </div>                                
            </div>
        </div>
    </div>