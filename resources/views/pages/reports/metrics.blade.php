@extends('layouts.app')

@section('content')
<div class="subtitlebar">
    <h4>Profit Analytics</h4>
    <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
        <i class="fa fa-calendar"></i>&nbsp;<span></span><i class="fa fa-caret-down"></i>
    </div>
</div>
<div class="row nopad" id="analyt">
	<h3>Please Wait</h3>
</div>
@endsection
@push('page_js')
	<script src="{{ asset('plugins/apexcharts/apexcharts.min.js') }}"></script>
    <script src="{{ asset('plugins/apexcharts/irregular-data-series.js') }}"></script>
@endpush

@push('page_css')
	<link href="{{ asset('plugins/apexcharts/apexcharts.css') }}" rel="stylesheet">
@endpush

@push('scripts')
<script type="text/javascript">
    var cookis = readcookies(); 
    var start = cookis.start ? moment(cookis.start) : moment().subtract(29, 'days');
    var end = cookis.start ? moment(cookis.end) : moment();
    var currency = "{{ $shop['shop_currency'] }}";
    var names = {};
    var currentDates = [];
    var pastDates = [];
    var charts = {};
    var pastCharts = {};
    var options= {
        chart: {height: 150, type: 'line', zoom: {enabled: false}, toolbar:{show:false}},
        dataLabels: {enabled: false},
        colors:['#3d59d9', '#c4c4c4'],
        stroke: { width: [2, 2], curve: 'smooth', dashArray: [0, 5]},
        legend: {show:false},
        markers: {size: 0, hover: {sizeOffset: 6}},
        xaxis: {
            type: 'datetime', 
            labels:{show:false }, 
            axisBorder: {show: false, color:'rgba(0,0,0,0.05)'}, 
            axisTicks: {show: true, color: 'rgba(0,0,0,0.05)'}, 
            categories: currentDates,
        },
        yaxis: {},
        tooltip: {
            custom: function({series, seriesIndex, dataPointIndex, w}) {
                return '<table><thead><tr><th>'+w.globals.seriesNames[seriesIndex]+'</th></tr></thead><tbody><tr><td><div class="currentdate"><span>'+currentDates[dataPointIndex]+'</span><span>'+(series[0][dataPointIndex] || 0 )+' '+names[w.config.chart.id].unit+'</span></div></td></tr><br/><tr><td><div class="pastdate"><span>'+pastDates[dataPointIndex]+'</span><span>'+(series[1][dataPointIndex] || 0 )+' '+names[w.config.chart.id].unit+'</span></div></td></tr></tbody></table>';
            }
        },
        grid: {borderColor: '#f3f3f3'}
    };

    function readcookies() {
        var allcookies = document.cookie;
        cookiearray = allcookies.split(';');
        var cooks = {};
        for(var i=0; i<cookiearray.length; i++) {
            cooks[(cookiearray[i].split('=')[0]).trim()]=cookiearray[i].split('=')[1];
        }
        return cooks;
    }

    var fetchData = function(){
        $.ajax({
            url: '{{ route('analytics.fetch') }}',
            data: {start:start.format('YYYY-MM-DD'), end:end.format('YYYY-MM-DD')},
            success:function(res){
                $("#analyt").html(res.html);
                charts = res.charts;
                pastCharts = res.pastCharts;
                names= res.names;
                currentDates = res.currentDates;
                options.xaxis.categories = res.currentDates;
                pastDates = res.pastDates;
                applyChart();
            }
        });
    }

    var applyChart = function(){
        $.each(names, function(key, value){
            options.chart.id = key;
            options.series = [
                {name:value.title, data:charts[key]},
                {name:value.title, data:pastCharts[key]}
            ];
            options.yaxis = {
                labels:{ 
                    formatter : function(val){
                        return (!val.isInteger ? val.toFixed(2) : val)+' '+names[key].unit;
                    }
                }
            };
            var chart = new ApexCharts(
                document.querySelector("#"+key),
                options
            );
            chart.render(); 
        })
    }
	$(function(){
	    function cb(start, end) {
            $('#reportrange span').html(start.format('MMM DD') + ' - ' + end.format('MMM DD'));
        }

        fetchData();

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
            start = picker.startDate;
            end = picker.endDate;
            document.cookie="start="+start.format('YYYY-MM-DD');
            document.cookie="end="+end.format('YYYY-MM-DD');
            fetchData();
        });

        cb(start, end); 

        setInterval(fetchData, 60000);
	});
	</script>
@endpush