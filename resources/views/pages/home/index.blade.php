<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <title>Exact Profit</title>

	    <!-- Framework -->
	    <link rel="icon" type="image/png" href="{{ asset('images/favicon-32x32.png') }}">
	    <link rel="stylesheet" href="{{ asset('home/css/bootstrap.css') }}">
	    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700&family=Noto+Sans:wght@400;700&display=swap" rel="stylesheet">
	    <link rel="stylesheet" type="text/css" href="{{ asset('home/css/slick.css') }}"/>
	    <link rel="stylesheet" type="text/css" href="{{ asset('home/css/slick-theme.css') }}"/>
	    <link rel="stylesheet" type="text/css" href="{{ asset('home/css/font-awesome.min.css') }}"/>
	    <link rel="stylesheet" href="{{ asset('home/css/animate.css') }}">
	    <link rel="stylesheet" href="{{ asset('home/css/main.css') }}">

	    <!-- Facebook Pixel Code -->
        <script>
          !function(f,b,e,v,n,t,s)
          {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
          n.callMethod.apply(n,arguments):n.queue.push(arguments)};
          if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
          n.queue=[];t=b.createElement(e);t.async=!0;
          t.src=v;s=b.getElementsByTagName(e)[0];
          s.parentNode.insertBefore(t,s)}(window, document,'script',
          'https://connect.facebook.net/en_US/fbevents.js');
          fbq('init', '434080454254186');
          fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
          src="https://www.facebook.com/tr?id=434080454254186&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-LE2W1RTQWN"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'G-LE2W1RTQWN');
        </script>
        <script type='text/javascript'>
            window.__lo_site_id = 257889;

            (function() {
                var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
                wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
              })();
        </script>

        <!-- Smartsupp Live Chat script -->
        <script type="text/javascript">
            var _smartsupp = _smartsupp || {};
            _smartsupp.key = '4590ae3ba6de6d61fabce4bb2bdaabd6156abe88';
            window.smartsupp||(function(d) {
              var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
              s=d.getElementsByTagName('script')[0];c=d.createElement('script');
              c.type='text/javascript';c.charset='utf-8';c.async=true;
              c.src='https://www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
            })(document);
        </script>

	</head>
    <body>
	<!-- Header data starts-->
	<header class="market_header" id="myHeader">
	    <nav class="navbar navbar-default">
	        <div class="container">
	          <!-- Brand and toggle get grouped for better mobile display -->
	          <div class="navbar-header">
	            <button type="button" class="mobile-toggle">
	              <span class="sr-only">Toggle navigation</span>
	              <span class="icon-bar"></span>
	              <span class="icon-bar"></span>
	              <span class="icon-bar"></span>
	            </button>
	            <a class="navbar-brand" href="{{ route('landing') }}"><img class="header_logo" src="{{ asset('home/images/logo_white.png') }}"></a>

	          </div>
	      
	          <!-- Collect the nav links, forms, and other content for toggling -->
	          <div class="navbar-collapse" id="headermenu">
	          	<a class="closebtn" id="closebtn"><img src="{{ asset('home/images/icn_menu_close.png') }}" alt=""></a>
	            <ul class="nav navbar-nav navbar-right">
	                <li><a href="{{ route('landing') }}" class="activeparent">Home</a></li>
	              	<li><a href="{{ config('app.marketing_url') }}">Developer site</a></li>
	            	<li><a href="{{ config('app.marketing_url') }}/about-us">About us</a></li>
	               	<li><a href="{{ config('app.marketing_url') }}/apps/exactprofit/privacy-policy">Privacy policy</a></li>
	              	<li><a href="mailto:info@digitaluprisers.com">Contact us</a></li>
	            </ul>
	          </div><!-- /.navbar-collapse -->
	        </div><!-- /.container-fluid -->
	      </nav>
	</header>
	<!-- Header data ends-->
	<section>
	  	<!-- Banner data starts-->
		<div id="section1" class="banner_block wow fadeInDown" data-wow-delay="0.1s">
		      <div class="container">
		          <div class="row">
		          	<div class="col-xs-12 col-sm-5 col-md-6 banner_right visible-xs">
		                <img src="{{ asset('home/images/banner_background.png') }}">
		            </div>
		            <div class="col-xs-12 col-sm-6 col-md-6 banner_left">
			                <h1>Profit Tracking Made Easy</h1>
			                <p class="mtop_15">Say goodbye to spreadsheets! Monitor all costs and know your true profit. All in a clean dashboard. </p>
			                <div class="loginbox">
			                <form method="POST" action="{{ route('login') }}" id="frmLogin">     
			                	@csrf                                   
								<div class="form-group">
	                            	<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email" autofocus placeholder="Email Address">
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
	                            </div>
	                            <div class="form-group">
	                            	<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="current-password" placeholder="Password">
	                            </div>
	                            <div class="form-row mb-3">
	                            	<div class="row">
	                                    <div class="col-xs-6">
	                                        <div class="custom-control custom-checkbox text-left">
	                                            <input type="checkbox" class="custom-control-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                            	<label class="custom-control-label font-14" for="remember">Remember Me</label>
	                                        </div>                                
	                                    </div>
	                                    <div class="col-xs-6 text-right">
	                                      <div class="forgot-psw"> 
	                                        <a id="forgot-psw" href="{{ route('password.request') }}" class="font-14">Forgot Password?</a>
	                                      </div>
	                                    </div>
	                                </div>
	                                </div>
								<button type="submit" class="btn btn-success btn-lg btn-block font-18">Log in</button>
	                        </form>
	                        <div class="login-or text-center">
	                                <h6 class="text-muted">OR</h6>
	                        </div>
	                        <div class="social-login text-center">
	                                <!-- Shophify popup code start  -->
	                                <!-- Button coce -->
	                                <button type="button" class="btn btn-success btn-lg btn-block font-18 shopifybtn" data-toggle="modal" data-target=".bd-example-modal-lg">Continue with <img srcset="{{ asset('home/images/shopify-logo.png') }}"></button>
	                                <!-- Button coce -->
	                                <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	                                    <div class="modal-dialog modal-lg">
	                                        <div class="modal-content">
	                                            <form name="frmShopify" id="frmShopify" action="{{ route('shopify.integrate') }}" method="get" novalidate="novalidate">
	                                                <div class="modal-header">
	                                                    <h5 class="modal-title" id="exampleLargeModalLabel">Login</h5>
	                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                                                    <span aria-hidden="true">×</span>
	                                                    </button>
	                                                </div>
	                                                <div class="modal-body">
	                                                    <p class="text-left">Enter your Shopify store name:</p>
	                                                    <div class="input-group mb-3">
	                                                        <input type="text" class="form-control" name="shop_url" id="shop_url" placeholder="my-shop-name" aria-label="Shop Name" aria-describedby="basic-addon2" data-error-container="#shop-err">
	                                                        <div class="input-group-append">
	                                                            <span class="input-group-text" id="basic-addon2">.myshopify.com</span>
	                                                        </div>
	                                                    </div>
	                                                    <span id="shop-err"></span>
	                                                </div>
	                                                <div class="modal-footer">
	                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
	                                                    <button type="submit" class="btn btn-primary">Connect</button>
	                                                </div>
	                                            </form>
	                                        </div>
	                                    </div>
	                                    <!-- Shophify popup code end -->
	                                </div>
	                        </div>
	                    </div>
			            <a href="https://apps.shopify.com/exact-profit" class="btn btn-shopify mtop_30"><img src="{{ asset('home/images/Shopify-btn.png') }}" alt=""></a>
			            <div class="ellipse_bg"></div>
		            </div>
		            <div class="col-xs-12 col-sm-6 col-md-6 banner_right hidden-xs">
		                <img src="{{ asset('home/images/banner_background.png') }}">
		            </div>
		          </div>
		      </div>
		</div>
		<div id="section2" class="testimonial_block wow fadeInUp"  data-wow-delay="0.1s">
		      <div class="client_logo_block">
		          <div class="container">
		             	<h2>Automate profit analysis!</h2>
		             	<p>No more oversimplified calculation. Know exactly how much money you’ve earned and start making instant data-driven decisions.</p>
		             	<a href="https://apps.shopify.com/exact-profit" class="btn btn-primary mtop_30">Start 14-day free trial</a>
		          </div>
		      </div>
		</div>
		<div id="section3" class="video_block wow fadeInUp"  data-wow-delay="0.1s">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-xs-12">
						<div id="videobtn" class="video_thumb wow fadeInUp" data-toggle="modal" data-target="#homeVideo" data-wow-delay="0.1s">
							<iframe src="https://www.youtube.com/embed/1B-nt40F1jA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="section4" class="project_block">
			<div class="container wow fadeInUp" data-wow-delay="0.1s">
				<div class="row">
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div class="productdiv no_bg">
						<h2>Let us handle the hard work</h3>
						<button class="btn btn-primary mtop_30 visible-sm visible-md visible-lg">Start 14-day free trial</button>
					</div>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div class="productdiv card">
							<i class="fa fa-shopping-cart" aria-hidden="true"></i>
							<div class="product_dtl">
								<h3>Manage Cost of Good Sold</h3>
								<p>Sync Shopify’s Cost per item, input directly, upload file, or make a super quick estimation.</p>
							</div>
							</a>
						</div>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div class="productdiv card">
							<i class="fa fa-facebook" aria-hidden="true"></i>
							<div class="product_dtl">
								<h3>Sync Facebook & Google Ad spend</h3>
								<p>Integrate with Facebook & Google to automatically pull in Ad spend.</p>
							</div>
							</a>
						</div>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div class="productdiv card">
							<i class="fa fa-usd" aria-hidden="true"></i>
							<div class="product_dtl">
								<h3>Manage all possible costs</h3>
								<p>Shipping costs, transaction fees, handling fees & custom spend. Everything gets recorded!</p>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div class="productdiv card">
							<i class="fa fa-file-text-o" aria-hidden="true"></i>
							<div class="product_dtl">
								<h3>Order history</h3>
								<p>Manage and adjust order’s COGS & shipping costs when necessary.</p>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div class="productdiv card">
							<i class="fa fa-bar-chart" aria-hidden="true"></i>
							<div class="product_dtl">
								<h3>Product Analytics</h3>
								<p>Know the insights of each product: views, ATC, ATC rate, conversion rate, revenue…</p>
							</div>
							</a>
						</div>
					</div>
					<a href="https://apps.shopify.com/exact-profit" class="btn btn-primary mtop_15 hidden-sm hidden-md hidden-lg">Start 14-day free trial</a>
				</div>	
			</div>
		</div>
		<div id="section5" class="screenshotblock wow fadeInUp"  data-wow-delay="0.1s">
			<div class="kpibox container">
				<div class="screen_slider ">
					<div class="client_logo_block">
			        	<img src="{{ asset('home/images/screenshot_1.png') }}">
			        </div>
			        <div class="client_logo_block">
			        	<img src="{{ asset('home/images/screenshot_2.png') }}">
			        </div>
			        <div class="client_logo_block">
			        	<img src="{{ asset('home/images/screenshot_3.png') }}">
			        </div>
				</div>	
			</div>
		</div>
		<div class="blue_block" id="bestplan">
      <div class="container wow fadeInUp" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
        <h2> Choose Your Plan</h2>
        <p>We enjoy adapting our strategies to offer every client the best solutions that are at the forefront of the industry.</p>
        <a href="https://apps.shopify.com/exact-profit" class="btn btn-primary mtop_15 trialbtn">Start 14-day free trial</a>
        <div class="row" id="plancategory">
          <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="iconblock">
              <img src="{{ asset('home/images\icn_basicplan.svg') }}" alt="">
            </div>
            <div class="plandtl">
              <div class="plantype">Basic <span>$19/month</span></div>
              <ul>
                <li>Ad Spend,<i class="fa fa-check-circle available"></i></li>
                <li>Marketing Attribution,<i class="fa fa-check-circle available"></i></li>
                <li>Advanced Insights,<i class="fa fa-check-circle available"></i></li>
                <li>Shopify insight<i class="fa fa-check-circle available"></i></li>
              </ul>
             <a href="https://apps.shopify.com/exact-profit" class="btn btn-primary btn_small mtop_15">Buy Now</a>
            </div>
          </div>
        </div>
      </div>
    </div>
	
		<!-- Banner data ends-->
	</section>
	<footer>
		<div class="container wow fadeInUp" data-wow-delay="0.1s">
			<div class="row">
				<div class="col-xs-5 col-sm-4" id="ourproduct">
					<h3 class="linkheading">Our Products</h3>
					<a href="{{ route('landing') }}"><img src="{{ asset('home/images/logo_white.png') }}"></a>
				</div>
				<div class="col-sm-8 col-xs-7" id="quickinfo">
					<h3 class="linkheading">Quick Links</h3>
					<ul class="linklist">
						<li><a href="{{ config('app.marketing_url') }}">Developer site</a></li>
	               		<li><a href="{{ config('app.marketing_url') }}/apps/exactprofit/privacy-policy">Privacy policy</a></li>
	               		<li><a href="{{ config('app.marketing_url') }}/about-us">About Us</a></li>
	              		<li><a href="mailto:info@digitaluprisers.com">Contact us</a></li>
					</ul>
					<p>Copyright © 2019 Digital Uprisers Pvt Ltd. All Rights Reserved</p>
				</div>
			</div>
		</div>
	</footer>
    <!-- Scripts for all pages -->
    <script src="{{ asset('home/js/jquery-1.12.4.min.js') }}" ></script>
    <script src="{{ asset('home/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('home/js/slick.min.js') }}"></script>
    <script src="{{ asset('home/js/wow.min.js') }}"></script>
    <script>new WOW().init();</script>
	<script src="{{ asset('home/js/design.js') }}"></script>
  </body>
</html>