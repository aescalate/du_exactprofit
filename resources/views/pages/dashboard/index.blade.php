@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12 col-lg-4">
        <div class="whitebox sameheight">
            <div class="milestonebox">
                <img src="images/milestone-img.svg"  alt=""/>
                <div class="milestone_text">
                    @if ($soFar['total_sales']< 1000)
                        <h6>Good start!</h6>
                        <p>Let's aim to achieve first milestone.</p>
                    @else
                        <h6>Congratulations!</h6>
                        <p>You’ve achieved {{ ($soFar['total_sales'] >= 1000000 ? 'fourth' : ($soFar['total_sales'] >= 100000 ? 'third' : ($soFar['total_sales'] >= 10000 ? 'second' : 'first'))) }} milestone.</p>
                    @endif
                </div>
            </div>
            <div class="activities-history">
                <div class="activities-history-list {{ $soFar['total_sales'] >= 1000 ? "cmplt" : '' }}">
                    <div class="activities-history-item">                                            
                        <h6>Four Figure Club</h6>
                        <p class="mb-0">1,000<small> {{ $shop['shop_currency'] }}</small></p>
                    </div>
                </div>
                <div class="activities-history-list {{ $soFar['total_sales'] >= 10000 ? "cmplt" : '' }}">
                    <div class="activities-history-item">
                        <h6>Five Figure Club</h6>
                        <p class="mb-0">10,000<small> {{ $shop['shop_currency'] }}</small></p>
                    </div>
                </div>
                <div class="activities-history-list {{ $soFar['total_sales'] >= 100000 ? "cmplt" : '' }}">
                    <div class="activities-history-item">
                        <h6>Six Figure Club</h6>
                        <p class="mb-0">1,00,000<small> {{ $shop['shop_currency'] }}</small></p>
                    </div>
                </div>
                <div class="activities-history-list {{ $soFar['total_sales'] >= 1000000 ? "cmplt" : '' }}">
                    <div class="activities-history-item">
                        <h6>Millionaire Club</h6>
                        <p class="mb-0">10,00,000<small> {{ $shop['shop_currency'] }}</small></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-lg-4">
        <div class="whitebox sameheight">
            <div class="smallbox">
                <div class="whitebox-header ">                                
                    <div class="bdr-btm2">
                        <h4>{{ $soFar['total_sales'] }}<small> {{ $shop['shop_currency'] }}</small></h4>
                        <span>Total Sales so far</span>
                    </div>
                </div>
                    <div class="whitebox-body">
                        <div class="activities-history">
                            <div class="simple-row">                                            
                                <span>Gross profit</span>
                                <span class="value">{{ number_format($soFar['gross_profit'], 2) }}<small> {{ $shop['shop_currency'] }}</small></span>
                            </div>
                            <div class="simple-row">
                                <span>Net Profit</span>
                                <span class="value">{{ number_format($soFar['net_profit'], 2) }}<small> {{ $shop['shop_currency'] }}</small></span>
                            </div>
                        <div class="simple-row">
                            <span>COGS - Cost of goods sold</span>
                            <span class="value">{{ number_format($soFar['cogs'], 2) }}<small> {{ $shop['shop_currency'] }}</small></span>
                        </div> 
                        <div class="simple-row">
                            <span>Average order value</span>
                            <span class="value">{{ number_format($soFar['avg_ord_value'], 2) }}<small> {{ $shop['shop_currency'] }}</small></span>
                        </div>                               
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-lg-4">
        <div class="whitebox sameheight">
            <div class="smallbox">
                <div class="whitebox-header ">                                
                    <div class="bdr-btm2">
                        <h4>{{ $soFar['campaigns'] }}</h4>
                        <span>Total Campaign so far</span>
                    </div>
                </div>
                    <div class="whitebox-body">
                        <div class="activities-history">
                            <div class="simple-row">                                            
                                <span>Total views</span>
                                <span class="value">{{ $soFar['visits'] }}</span>
                            </div>
                            <div class="simple-row">
                                <span>Transaction Charges</span>
                                <span class="value">{{ number_format($soFar['transaction_charge'], 2) }}<small> {{ $shop['shop_currency'] }}</small></span>
                            </div>
                        <div class="simple-row">
                            <span>Cost Per Conversion</span>
                            <span class="value">{{ number_format($soFar['cpc'], 2) }}<small> {{ $shop['shop_currency'] }}</small></span>
                        </div> 
                        <div class="simple-row">
                            <span>Total purchase</span>
                            <span class="value">{{ $soFar['total_orders'] }}</span>
                        </div>                               
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="subtitlebar">
    <h4>Custom Chart</h4>
    <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
        <i class="fa fa-calendar"></i>&nbsp;<span></span> <i class="fa fa-caret-down"></i>
    </div>
</div>
<!-- Sales and Profit Graph start here -->
<div class="container whitebox nopad">
    <div class="row">                   
        <div class="col-xs-12 col-sm-12 col-md-4 bdr-r-1">
            <div class="pad30">
                <h5 class="section-heading">Sales and Profit</h5>
                <!-- <button id="removeDataset">Remove Dataset</button> -->
                <div class="custom-control form-control-lg custom-checkbox bgblue">  
                    <input type="checkbox" class="custom-control-input sales_profit" id="totalSales" data-title="Total Sales" checked>  
                    <label class="custom-control-label " for="totalSales">Total Sales</label>  
                </div>
                <div class="custom-control form-control-lg custom-checkbox bgsky">  
                    <input type="checkbox" class="custom-control-input sales_profit" id="cogsdata" data-title="COGS" checked>  
                    <label class="custom-control-label" for="cogsdata">COGS</label>  
                </div>
                <div class="custom-control form-control-lg custom-checkbox bgred">  
                    <input type="checkbox" class="custom-control-input sales_profit" id="grossProfit" data-title="Gross Profit" checked>  
                    <label class="custom-control-label" for="grossProfit">Gross Profit</label>  
                </div>
                <div class="custom-control form-control-lg custom-checkbox bggreen">  
                    <input type="checkbox" class="custom-control-input sales_profit" id="netProfit" data-title="Net Profit" checked>
                    <label class="custom-control-label" for="netProfit">Net Profit</label>  
                </div>
                <div class="custom-control form-control-lg custom-checkbox bgpink">  
                    <input type="checkbox" class="custom-control-input sales_profit" id="adSpent" data-title="Ad Spend" checked> 
                    <label class="custom-control-label" for="adSpent">Ad Spend</label>  
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-8 p-15">
            <div class="pad30">
                <canvas id="canvas"></canvas>
            </div>
        </div>
    </div>
</div>
<!-- Sales and Profit Graph end here -->
<!-- Expenditure Graph start here -->
<div class="container whitebox nopad">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-4 bdr-r-1">
            <div class="pad30">
                <h5 class="section-heading">Expenditure</h5>
                <div class="custom-control form-control-lg custom-checkbox bgblue">  
                    <input type="checkbox" class="custom-control-input expends" data-title="Total Discounts" id="totalDiscount" checked>  
                    <label class="custom-control-label " for="totalDiscount">Total Discount</label>  
                </div>
                <div class="custom-control form-control-lg custom-checkbox bgsky">  
                    <input type="checkbox" class="custom-control-input expends" data-title="Shipping Cost" id="shippingCost" checked>  
                    <label class="custom-control-label" for="shippingCost">Shipping Cost</label>  
                </div>
                <div class="custom-control form-control-lg custom-checkbox bgpink">  
                    <input type="checkbox" class="custom-control-input expends" data-title="COGS" id="cogsone" checked>  
                    <label class="custom-control-label" for="cogsone">COGS</label>  
                </div>
                <div class="custom-control form-control-lg custom-checkbox bggreen">  
                    <input type="checkbox" class="custom-control-input expends" data-title="Taxes" id="taxes" checked>  
                    <label class="custom-control-label" for="taxes">Taxes</label>  
                </div>
                <div class="custom-control form-control-lg custom-checkbox bgdarkblue">  
                    <input type="checkbox" class="custom-control-input expends" data-title="Facebook Adspend" id="fbAdspend" checked>  
                    <label class="custom-control-label" for="fbAdspend">Facebook Adspend</label>  
                </div>
                <div class="custom-control form-control-lg custom-checkbox bgdarkred">  
                    <input type="checkbox" class="custom-control-input expends" data-title="Google Adspend" id="gAdspend" checked>  
                    <label class="custom-control-label" for="gAdspend">Google Adspend</label>  
                </div>
                <div class="custom-control form-control-lg custom-checkbox bgbrown">  
                    <input type="checkbox" class="custom-control-input expends" data-title="Refunds" id="refund" checked>  
                    <label class="custom-control-label" for="refund">Refunds</label>  
                </div>
                <div class="custom-control form-control-lg custom-checkbox bgparrot">  
                    <input type="checkbox" class="custom-control-input expends" data-title="Transaction Charge" id="transaction_charge" checked>  
                    <label class="custom-control-label" for="transaction_charge">Transaction Charges</label>  
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-8 p-15">
            <div class="pad30">
                @if (
                    array_sum($charts['discounts'])+
                    array_sum($charts['shipping_cost'])+
                    array_sum($charts['cogs'])+
                    array_sum($charts['tax'])+
                    array_sum($charts['facebook_adspend'])+
                    array_sum($charts['google_adspend'])+
                    array_sum($charts['refunds'])+
                    array_sum($charts['transaction_charge'])
                 <= 0)
                    <h3>No data available for selected dates</h3>
                @else
                    <div id="canvas-holder">
                        <canvas id="chart-area"></canvas>
                    </div>                       
                @endif
            </div>
        </div>
    </div>
</div>
<!-- Expenditure Graph end here -->
<!-- Order Graph start here -->
<div class="container whitebox nopad">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-4 bdr-r-1">
            <div class="pad30">
                <h5 class="section-heading">Orders</h5>
                  <div class="custom-control form-control-lg custom-checkbox bgblue">  
                <input type="checkbox" class="custom-control-input orders" data-title="Visitors" id="visitors" checked>  
                <label class="custom-control-label " for="visitors">Visitors</label>  
            </div>
            <div class="custom-control form-control-lg custom-checkbox bgsky">  
                <input type="checkbox" class="custom-control-input orders" data-title="Total Orders" id="totalorders" checked>  
                <label class="custom-control-label" for="totalorders">Total Orders</label>  
            </div>
            <div class="custom-control form-control-lg custom-checkbox bgred">  
                <input type="checkbox" class="custom-control-input orders" data-title="Cancelled Orders" id="cancelledorders" checked>  
                <label class="custom-control-label" for="cancelledorders">Cancelled Orders</label>  
            </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-8 p-15">
            <div class="pad30">
                <div id="container">
                    <canvas id="canvasbar"></canvas>
                </div>                      
            </div>
        </div>
    </div>
</div>
<!-- Order Graph end here -->
<div class="container whitebox pad30">
     <div class="fullreport">
        <h5 class="section-heading">Full Report</h5>
        <div>
            <button class="btn btn-default" type="button" id="dwn-xls">Download</button>
        </div>
    </div>
    <table class="table table-bordered table-accordian table-responsive">
        <thead>
            <tr>
                <th>Days</th>
                <th>Total<br>Compaigns</th>
                <th>Total<br>Clicks</th>
                <th>CTR</th>
                <th>Average<br>CPC</th>
                <th>CPM</th>
                <th>Impressions</th>
                <th>Reach</th>
                <th>Adspend</th>
                <th>Revenue</th>
                <th>Conversion<br>Rate</th>
                <th>Gross<br>Profit</th>
                <th>Net<br>Profit</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($weeks as $key=>$week)
                <tr>
                    <td><a href="#" class="toggler" data-prod-cat="{{ $key+1 }}">Week {{ $loop->iteration }}</a></td>
                    <td>{{ $week['row']['campaigns'] }}</td>
                    <td>{{ $week['row']['clicks'] }}</td>
                    <td>{{ round($week['row']['ctr'], 2) }}</td>
                    <td>{{ round($week['row']['cpc'], 2) }}</td>
                    <td>{{ round($week['row']['cpm'], 2) }}</td>
                    <td>{{ $week['row']['impressions'] }}</td>
                    <td>{{ $week['row']['visits'] }}</td>
                    <td>{{ round($week['row']['adspend'], 2) }}</td>
                    <td>{{ round($week['row']['revenue'], 2) }}</td>
                    <td>{{ round($week['row']['conversion_rate'], 2) }}</td>
                    <td>{{ round($week['row']['gross_profit'], 2) }}</td>
                    <td>{{ round($week['row']['net_profit'], 2) }}</td>
                </tr>
                @foreach ($week['days'] as $day)
                    <tr class="subrow subrowitem{{ $key+1 }}" style="display:none">
                        <td>{{ date('d M, Y', strtotime($day['date'])) }}</td>
                        <td>{{ $day['campaigns'] }}</td>
                        <td>{{ $day['clicks'] }}</td>
                        <td>{{ round($day['ctr'], 2) }}</td>
                        <td>{{ round($day['cpc'], 2) }}</td>
                        <td>{{ round($day['cpm'], 2) }}</td>
                        <td>{{ $day['impressions'] }}</td>
                        <td>{{ $day['visits'] }}</td>
                        <td>{{ round($day['adspend'], 2) }}</td>
                        <td>{{ round($day['revenue'], 2) }}</td>
                        <td>{{ round($day['conversion_rate'], 2) }}</td>
                        <td>{{ round($day['gross_profit'], 2) }}</td>
                        <td>{{ round($day['net_profit'], 2) }}</td>
                    </tr> 
                @endforeach
            @endforeach
            <tr class="fixedrow">
                <td><b>Total</b></td>
                <td>{{ $total['campaigns'] }}</td>
                <td>{{ $total['clicks'] }}</td>
                <td>{{ number_format($total['ctr'], 2) }}</td>
                <td>{{ number_format($total['cpc'], 2) }}</td>
                <td>{{ number_format($total['cpm'], 2) }}</td>
                <td>{{ $total['impressions'] }}</td>
                <td>{{ $total['visits'] }}</td>
                <td>{{ round($total['adspend'], 2) }}</td>
                <td>{{ round($total['revenue'], 2) }}</td>
                <td>{{ round($total['conversion_rate'], 2) }}</td>
                <td>{{ round($total['gross_profit'], 2) }}</td>
                <td>{{ round($total['net_profit'], 2) }}</td>
            </tr>
             <tr class="fixedrow">
                <td><b>Average</b></td>
                <td>{{ $average['campaigns'] }}</td>
                <td>{{ round($average['clicks'],0) }}</td>
                <td>{{ round($average['ctr'], 2) }}</td>
                <td>{{ round($average['cpc'], 2) }}</td>
                <td>{{ round($average['cpm'], 2) }}</td>
                <td>{{ round($average['impressions'], 2) }}</td>
                <td>{{ round($average['visits'], 2) }}</td>
                <td>{{ round($average['adspend'], 2) }}</td>
                <td>{{ round($average['revenue'], 2) }}</td>
                <td>{{ round($average['conversion_rate'], 2) }}</td>
                <td>{{ round($average['gross_profit'], 2) }}</td>
                <td>{{ round($average['net_profit'], 2) }}</td>
            </tr>
        </tbody>
    </table>
</div>
@endsection
@push('page_js')
	<script src="{{ asset('js/chart.min.js') }}"></script>
    <script src="{{ asset('js/utils.js') }}"></script>
@endpush

@push('scripts')
	<script type="text/javascript">
        $(document).ready(function(){
            var start = moment('{{ $start }}');
            var end = moment('{{ $end }}');
            var chartTitle = "Showing data between "+start.format('MMM D, YYYY')+' To '+end.format('MMM D, YYYY');
            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                   'Today': [moment(), moment()],
                   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                   'This Month': [moment().startOf('month'), moment().endOf('month')],
                   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
                window.location.href="{{ route('home') }}?start="+picker.startDate.format('YYYY-MM-DD')+"&end="+picker.endDate.format('YYYY-MM-DD');              
            });

            cb(start, end);

            // script for make all height same start
            equalheight = function (element) {
                $(element).height("auto");
                var currentTallest = 0
                $(element).each(function () {
                    if ($(this).outerHeight() > currentTallest) {
                        currentTallest = $(this).outerHeight();
                    }
                });
                $(element).outerHeight(currentTallest);
            }
            $(window).on("load",function () {
                if ($(window).width() > 992) {
                    equalheight(".sameheight");
                }
                else {
                    $(".sameheight").height("auto");
                }
            });

            $(window).on("resize", function () {
                if ($(window).width() > 992) {
                    equalheight(".sameheight");
                }
                else {
                    $(".sameheight").height("auto");
                }
            });
            // script for make all height same start
            // script for toggle row in table start
            $('.subrowitem1').show();
            $(".toggler").click(function(e){
                e.preventDefault();
                $('.subrowitem'+$(this).attr('data-prod-cat')).toggle();
            });
            // script for toggle row in table end
            
            var sales_datasets = {
                total_sales: {
                    label: 'Total Sales',
                    backgroundColor: '#00b2d6',
                    borderColor: '#00b2d6',
                    data: @json($charts['total_sales']),
                    fill: false,
                },
                cogs:{
                    label: 'COGS',
                    backgroundColor: '#00c6a5',
                    borderColor: '#00c6a5',
                    data: @json($charts['cogs']),
                    fill: false,
                },
                gross_profit:{
                    label: 'Gross Profit',
                    backgroundColor: '#ff7356',
                    borderColor: '#ff7356',
                    data: @json($charts['gross_profit']),
                    fill: false
                },
                net_profit:{
                    label: 'Net Profit',
                    backgroundColor: '#d7e23f',
                    borderColor: '#d7e23f',
                    data: @json($charts['net_profit']),
                    fill: false
                },
                ad_spend:{
                    label: 'Ad Spend',
                    backgroundColor: '#fb71f6',
                    borderColor: '#fb71f6',
                    data: @json($charts['adspend']),
                    fill: false
                }
            }

            var salesConfig = {
                type: 'line',
                data: {
                    labels: @json($dates),
                    datasets: Object.values(sales_datasets)
                },
                options: {
                    responsive: true,
                    plugins: {
                        title: {
                            display: true,
                            text: 'Chart.js Line Chart'
                        },
                        tooltip: {
                            mode: 'index',
                            intersect: false,
                        }
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },                    
                    scales: {
                         xAxes: [{
                                display: false //this will remove all the x-axis grid lines
                            }],
                        x: {
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Month'
                            }
                        },
                        y: {
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Value'
                            }
                        }
                    },
                    legend: false,
                    title: {
                        display: true,
                        text: chartTitle+' (Currency {{ $shop['shop_currency'] }})',
                        position :'top'                         
                    }
                }
            };            

            var ctx = document.getElementById('canvas').getContext('2d');
            var saleProfitChart = new Chart(ctx, salesConfig);

            $(document).on("change", ".sales_profit", function(){
                var setName = $(this).data('title');
                if(this.checked){
                    var newDataset = sales_datasets[(setName.toLowerCase()).replace(/\ /g, "_")];
                    salesConfig.data.datasets.push(newDataset);
                    saleProfitChart.update();
                } else{
                    salesConfig.data.datasets = salesConfig.data.datasets.filter(function(obj) {
                        return (obj.label != setName); 
                    });
                    saleProfitChart.update();
                }
            });


            var expendData = {
                total_discounts:@json(round(array_sum($charts['discounts']),2)),
                shipping_cost:@json(round(array_sum($charts['shipping_cost']),2)),
                cogs:@json(round(array_sum($charts['cogs']),2)),
                taxes:@json(round(array_sum($charts['tax']),2)),
                facebook_adspend:@json(round(array_sum($charts['facebook_adspend']),2)),
                google_adspend:@json(round(array_sum($charts['google_adspend']),2)),
                refunds:@json(round(array_sum($charts['refunds']),2)),
                transaction_charge:@json(round(array_sum($charts['transaction_charge']),2)),
            }

            var colors = {total_discounts:'#00b2d6', shipping_cost:'#00c6a5', cogs:'#fb71f6', taxes:'#d7e23f', facebook_adspend:'#3b5998', google_adspend:'#dd4b39', refunds:'#bfa463', transaction_charge:'#96bf48'};

            var expConfig = {
                type: 'doughnut',
                data: {
                    labels: ['Total Discount', 'Shipping Cost', 'COGS', 'Taxes', 'Facebook Adspend', 'Google Adspend', 'Refunds', 'Transaction Charge'],
                    datasets: [
                        {
                            data: Object.values(expendData),
                            backgroundColor: Object.values(colors),
                            label: 'Expenditures'
                        }
                    ],                
                },
                options: {
                    responsive: true,
                    plugins: {
                        legend: {
                            position: 'top',
                        },
                        title: {
                            display: true,
                            text: 'Chart.js Doughnut Chart'
                        },
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    },
                    legend: false,
                    title: {
                        display: true,
                        text: chartTitle+' (Currency {{ $shop['shop_currency'] }})',
                        position :'top'
                    }
                }
            };

            var ctx2 = document.getElementById('chart-area').getContext('2d');
            var expendChart = new Chart(ctx2, expConfig);

            $(document).on("change", ".expends", function(){
                var data = [];
                var clrs = [];
                var lbls = [];
                $('.expends').each(function () {
                    if(this.checked){                        
                        var setNm = $(this).data('title');
                        lbls.push(setNm);
                        var tx = (setNm.toLowerCase()).replace(/\ /g, "_")
                        data.push(expendData[tx]);
                        clrs.push(colors[tx])
                    }
                });
                expConfig.data.datasets = [{data:data, backgroundColor:clrs, label: 'Expenditures'}];
                expConfig.data.labels = lbls;
                expendChart.update();

            });

            var orderDataset = {
                visitors: {
                    label: 'Visitors',
                    backgroundColor: '#00b2d6',
                    borderColor: '#00b2d6',
                    data: @json($charts['visits']),
                    fill: false,
                },
                total_orders: {
                    label: 'Total Orders',
                    backgroundColor: '#00c6a5',
                    borderColor: '#00c6a5',
                    data: @json($charts['total_orders']),
                    fill: false,
                },
                cancelled_orders: {
                    label: 'Cancelled Orders',
                    backgroundColor: '#ff7356',
                    borderColor: '#ff7356',
                    data: @json($charts['cancelled_orders']),
                    fill: false
                }
            }

            salesConfig.options.title.text=chartTitle
            var orderConfig = {
                type: 'line',
                data: {
                    labels: @json($dates),
                    datasets: Object.values(orderDataset)
                },
                options:salesConfig.options
            };

            var ctx1 = document.getElementById('canvasbar').getContext('2d');
            orderChart = new Chart(ctx1, orderConfig);

            $(document).on("change", ".orders", function(){
                var setName = $(this).data('title');
                if(this.checked){
                    var newDataset = orderDataset[(setName.toLowerCase()).replace(/\ /g, "_")];
                    orderConfig.data.datasets.push(newDataset);
                    orderChart.update();
                } else{
                    orderConfig.data.datasets = orderConfig.data.datasets.filter(function(obj) {
                        return (obj.label != setName); 
                    });
                    orderChart.update();
                }
            });

            $(document).on("click", "#dwn-xls", function(){
                window.location.href="{{ route('exportxls') }}?start="+start.format('YYYY-MM-DD')+"&end="+end.format('YYYY-MM-DD');
            })
        });
    </script>
@endpush