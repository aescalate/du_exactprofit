@if ($routeName == 'complete-profile-form')
    <!-- Info bar start here -->
    <div class="info-bar">
        <div class="container">
           <h3 class="profileheading">Thanks for installing the app. Please setup profile for your store “{{ $shop['shop_name'] }}”</h3>
        </div>
    </div>
    <!-- Info bar end here -->
@else
  <!-- Info bar start here -->
  <div class="info-bar">
      <div class="container">
          <div class="shop-dtl">
              <img srcset="{{ asset('images/shop.svg') }}" alt="">
              <div class="shop-dtl-info">
                  <h4>{{ $shop['shop_name'] }}</h4>
                  <b>{{ $shop['shop_currency'] }}</b> <a target="_blank" href="https://{!! $shop['shop_domain'] !!}">{!! $shop['shop_domain'] !!}</a>
              </div>
          </div>
          <div class="access-dtl">
              <a href="{{ !$accessGranted['facebook'] ? config('vars.grant_url').'/'.encryptString(Auth::id()) : '#' }}" class="btn btn-access {{ $accessGranted['facebook'] ? 'selected' : ''}} m-r-20">
                <span class="icn facebook"></span>{{ $accessGranted['facebook'] ? 'Granted' : 'Grant Access'}}<span class="grant"></span>
              </a>
              <a href="{{ !$accessGranted['google'] ? config('vars.grant_url').'/'.encryptString(Auth::id()) : '#' }}" class="btn btn-access {{ $accessGranted['google'] ? 'selected' : ''}}">
                <span class="icn google"></span>{{ $accessGranted['google'] ? 'Granted' : 'Grant Access'}}<span class="grant"></span>
              </a>
          </div>
      </div>
  </div>
  <!-- Info bar end here -->
  <!-- Menu bar start here -->
  <div class="menu-bar">
      <!-- desktop menu start -->
      <div class="container Desktop-menu">
          <nav class="navbar navbar-expand-lg navbar-light">
             <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item {{ $routeName == 'home' ? 'active' : '' }}">
                  <a class="nav-link" href="{{ route('home') }}">Dashboard <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item {{ $routeName == 'analytics.index' ? 'active' : '' }}">
                  <a class="nav-link" href="{{ route('analytics.index') }}">Analytics</a>
                </li>
                <li class="nav-item {{ $routeName == 'products.index' ? 'active' : '' }}">
                  <a class="nav-link" href="{{ route('products.index') }}">Products</a>
                </li>
                <li class="nav-item {{ $routeName == 'adaccounts.index' ? 'active' : '' }}">
                  <a class="nav-link" href="{{ route('adaccounts.index') }}">Ad Accounts</a>
                </li>
                <li class="nav-item {{ $routeName == 'settings.index' ? 'active' : '' }}">
                  <a class="nav-link" href="{{ route('settings.index') }}">Settings</a>
                </li>
                <li class="nav-item {{ $routeName == 'expenses' ? 'active' : '' }}">
                  <a class="nav-link" href="{{ route('expenses') }}">Shopify Expenses</a>
                </li>
              </ul>
            </div>
          </nav>
      </div>
      <!-- desktop menu end -->
      <!-- mobile menu start -->
      <div class="container mobile-menu">
          <b>Dashobard</b>
          <div class="dropdown show">
            <a class="btn btn-menu dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
            </a>

            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
              <a class="dropdown-item" href="{{ route('home') }}">Dashboard</a>
              <a class="dropdown-item" href="{{ route('analytics.index') }}">Analytics</a>
              <a class="dropdown-item" href="{{ route('products.index') }}">Products</a>
              <a class="dropdown-item" href="{{ route('adaccounts.index') }}">Ad Accounts</a>
              <a class="dropdown-item" href="{{ route('settings.index') }}">Setings</a>
              <a class="dropdown-item" href="{{ route('expenses') }}">Shopify Expenses</a>
            </div>
          </div>
      </div>
      <!-- desktop menu end -->
  </div>
@endif