<header>
    <div class="container">
        <a href="{{ route('home') }}" class="logo"><img src="{{ asset('images/logo.png') }}" srcset="{{ asset('images/logo.png') }}" alt=""></a>
            <div class="profile-text">
                <span>Hi <span>{{ Auth::user()->name }}, </span>Welcome back</span> 
        </div>
    </div>   
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>     
</header>