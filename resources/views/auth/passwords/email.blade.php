@extends('layouts.auth')

@section('content')
<div class="auth-box login-box">
        <!-- Start row -->
        <div class="row no-gutters align-items-center justify-content-center">
            <!-- Start col -->
            <div class="col-md-6 col-lg-5">
                <!-- Start Auth Box -->
                <div class="auth-box-right">
                    <div class="card">
                         <div class="card-header" style=" background: #3d59d9;">
                            <a href="{{ route('home') }}" class="logo"><img src="{{ asset('images/logo_white.png') }}" class="img-fluid" alt="logo"></a>
                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{ route('password.email') }}" id="frmForgot">
                                @csrf                                       
                                <h4 class="text-primary my-4 mt-0">Forgot Password</h4>
                                <div class="form-group">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email" autofocus placeholder="Email Address">
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <button type="submit" class="btn btn-success btn-lg btn-block font-18">{{ __('Send Password Reset Link') }}</button>
                            </form>
                            <div class="login-or">
                                <h6 class="text-muted">OR</h6>
                            </div>
                            <div class="social-login text-center">
                                <a href="{{ route('login') }}" class="btn btn-success btn-lg btn-block font-18 shopifybtn" >Back to Login</a>
                            </div>
                        </div>
                    </div>
                    <div class="text-center mt-25"> <a href="https://digitaluprisers.com/apps/exactprofit/privacy-policy">Terms &amp; Privacy Policy</a> </div>
                </div>
                <!-- End Auth Box -->
            </div>
            <!-- End col -->
        </div>
        <!-- End row -->
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(function(){
            $("#frmForgot").validate({
                ignore:[],
                rules: {
                    email:{
                        required:true,
                        email:true,
                        maxlength:255
                    }
                },
                messages: {
                    email:{
                        required:"@lang('validation.required',['attribute'=>'email'])",
                        maxlength:"@lang('validation.max.string',['attribute'=>'email','max'=>255])"
                    }
                }
            });
        });
    </script>
@endpush
