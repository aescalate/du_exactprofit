@extends('layouts.auth')

@section('content')
<div class="auth-box login-box">
    <!-- Start row -->
    <div class="row no-gutters align-items-center justify-content-center">
        <!-- Start col -->
        <div class="col-md-6 col-lg-5">
            <!-- Start Auth Box -->
            <div class="auth-box-right">
                <div class="card">
                     <div class="card-header" style=" background: #3d59d9;">
                        <a href="{{ route('home') }}" class="logo"><img src="{{ asset('images/logo_white.png') }}" class="img-fluid" alt="logo"></a>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('password.update') }}" id="frmReset">
                            @csrf            
                             <input type="hidden" name="token" value="{{ $token }}">                           
                            <h4 class="text-primary my-4 mt-0">Reset Password</h4>
                            <div class="form-group">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" autocomplete="email" autofocus placeholder="Email Address">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="new-password" placeholder="Password">
                                @error('password')
                                    <span class="help-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group @error('password') has-error @enderror">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" autocomplete="new-password" placeholder="Confirm Password">
                            </div>
                            <button type="submit" class="btn btn-success btn-lg btn-block font-18">{{ __('Reset Password') }}</button>
                        </form>
                    </div>
                </div>
                <div class="text-center mt-25"> <a href="https://digitaluprisers.com/apps/exactprofit/privacy-policy">Terms &amp; Privacy Policy</a> </div>
            </div>
            <!-- End Auth Box -->
        </div>
        <!-- End col -->
    </div>
    <!-- End row -->
</div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(function(){
            $("#frmReset").validate({
                ignore:[],
                rules: {
                    email:{
                        required:true,
                        email:true,
                        maxlength:255
                    },
                    password:{
                        required:true,
                        minlength:6,
                        maxlength:255
                    },
                    password_confirmation:{
                        equalTo:"#password"
                    }
                },
                messages: {
                    email:{
                        required:"@lang('validation.required',['attribute'=>'email'])",
                        maxlength:"@lang('validation.max.string',['attribute'=>'email','max'=>255])"
                    },
                    password:{
                        required:"@lang('validation.required',['attribute'=>'password'])",
                        maxlength:"@lang('validation.max.string',['attribute'=>'password','max'=>255])",
                        minlength:"@lang('validation.min.string',['attribute'=>'password','max'=>6])"
                    },
                    password_confirmation:{
                        equalTo:"@lang('validation.same',['attribute'=>'password','other'=>'confirm password'])"
                    }
                }
            });
        });
    </script>
@endpush
