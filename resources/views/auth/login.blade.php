@extends('layouts.auth')

@section('content')
    <div class="auth-box login-box">
        <!-- Start row -->
        <div class="row no-gutters align-items-center justify-content-center">
            <!-- Start col -->
            <div class="col-md-6 col-lg-5">
                <!-- Start Auth Box -->
                <div class="auth-box-right">
                    <div class="card">
                         <div class="card-header" style=" background: #3d59d9;">
                            <a href="{{ route('home') }}" class="logo"><img src="{{ asset('images/logo_white.png') }}" class="img-fluid" alt="logo"></a>
                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{ route('login') }}" id="frmLogin">
                                @csrf                                       
                                <h4 class="text-primary my-4 mt-0">Log in !</h4>
                                <div class="form-group">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email" autofocus placeholder="Email Address">
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="current-password" placeholder="Password">
                                </div>
                                <div class="form-row mb-3">
                                    <div class="col-sm-6">
                                        <div class="custom-control custom-checkbox text-left">
                                            <input type="checkbox" class="custom-control-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                            <label class="custom-control-label font-14" for="remember">Remember Me</label>
                                        </div>                                
                                    </div>
                                    <div class="col-sm-6">
                                      <div class="forgot-psw"> 
                                        <a id="forgot-psw" href="{{ route('password.request') }}" class="font-14">Forgot Password?</a>
                                      </div>
                                    </div>
                                </div>                          
                                <button type="submit" class="btn btn-success btn-lg btn-block font-18">Log in</button>
                            </form>
                            <div class="login-or">
                                <h6 class="text-muted">OR</h6>
                            </div>
                            <div class="social-login text-center">
                                <!-- Shophify popup code start  -->
                                <!-- Button coce -->
                                <button type="button" class="btn btn-success btn-lg btn-block font-18 shopifybtn"  data-toggle="modal" data-target=".bd-example-modal-lg">Continue with <img srcset="images/shopify-logo.png"></button>
                                <!-- Button coce -->
                                <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <form name="frmShopify" id="frmShopify" action="{{ route('shopify.integrate') }}" method="get">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleLargeModalLabel">Login</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <p class="text-left">Enter your Shopify store name:</p>
                                                    <div class="input-group mb-3">
                                                        <input type="text" class="form-control" name="shop_url" id="shop_url" placeholder="my-shop-name" aria-label="Shop Name" aria-describedby="basic-addon2" data-error-container="#shop-err">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text" id="basic-addon2">.myshopify.com</span>
                                                        </div>
                                                    </div>
                                                    <span id="shop-err"></span>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" class="btn btn-primary">Connect</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- Shophify popup code end -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center mt-25"> <a href="https://digitaluprisers.com/apps/exactprofit/privacy-policy">Terms &amp; Privacy Policy</a> </div>
                </div>
                <!-- End Auth Box -->
            </div>
            <!-- End col -->
        </div>
        <!-- End row -->
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(function(){
            $("#frmLogin").validate({
                ignore:[],
                rules: {
                    email:{
                        required:true,
                        email:true,
                        maxlength:255
                    },
                    password:{
                        required:true,
                        minlength:6,
                        maxlength:255
                    }
                },
                messages: {
                    email:{
                        required:"@lang('validation.required',['attribute'=>'email'])",
                        maxlength:"@lang('validation.max.string',['attribute'=>'email','max'=>255])"
                    },
                    password:{
                        required:"@lang('validation.required',['attribute'=>'password'])",
                        maxlength:"@lang('validation.max.string',['attribute'=>'password','max'=>255])",
                        minlength:"@lang('validation.min.string',['attribute'=>'password','max'=>6])"
                    }
                }
            });

            $("#frmShopify").validate({
                ignore:[],
                rules: {
                    shop_url:{
                        required:true,
                        maxlength:255
                    }
                },
                messages: {
                    shop_url:{
                        required:"@lang('validation.required',['attribute'=>'shop name'])",
                        maxlength:"@lang('validation.max.string',['attribute'=>'shop name','max'=>255])"
                    }
                }
            });
        });
    </script>
@endpush

{{-- @extends('layouts.auth')

@section('content')
<div class="container loginbox wow fadeInUp"  data-wow-delay="0.1s">
    <div class="col-xs-12 col-sm-6 col-md-6 leftpart">
        <img src="{{ asset('images/login_img.jpg') }}" alt="">
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 rightpart">
        <h1>Log in and get started.</h1>
        <form method="POST" action="{{ route('login') }}" class="loginform" id="frmLogin">
            @csrf
            <div class="form-group">
                <label for="email">Enter your Registered Email</label>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email" autofocus placeholder="Email Address">
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="password">Enter Password</label>
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="current-password" placeholder="Password">
            </div>
            <div class="form-group form-check">
               <input type="checkbox" class="form-check-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
               <label class="form-check-label" for="remember">Remember me</label>
            </div>
            <button type="submit" class="btn btn-primary">Continue to Login</button>
            <div class="signuparea">
                <p class="pull-left">Forgot Password?</p>
                <a href="{{ route('password.request') }}" class="frgtpswd">Reset Password</a>
            </div>
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <a href="{{ route('social.login', 'google') }}" class="btn btn-blue-icon google_btn"><span><img src="{{ asset('images/icn_google.png') }}" alt=""></span>Sign in with Google</a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <a href="{{ route('social.login', 'facebook') }}" class="btn btn-blue-icon facebook_btn"><span><img src="{{ asset('images/icn_facebook.png') }}" alt=""></span>Sign in with Facebook</a>
                </div>
            </div>
            <div class="signuparea">
                <p class="pull-left">New to Digital uprisers?</p>
                <a href="{{ route('signup') }}" class="btn btn_default pull-right">Sign Up</a>
            </div>
        </form>
    </div>
</div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(function(){
            $("#frmLogin").validate({
                ignore:[],
                rules: {
                    email:{
                        required:true,
                        email:true,
                        maxlength:255
                    },
                    password:{
                        required:true,
                        minlength:6,
                        maxlength:255
                    }
                },
                messages: {
                    email:{
                        required:"@lang('validation.required',['attribute'=>'email'])",
                        maxlength:"@lang('validation.max.string',['attribute'=>'email','max'=>255])"
                    },
                    password:{
                        required:"@lang('validation.required',['attribute'=>'password'])",
                        maxlength:"@lang('validation.max.string',['attribute'=>'password','max'=>255])",
                        minlength:"@lang('validation.min.string',['attribute'=>'password','max'=>6])"
                    }
                }
            });
        });
    </script>
@endpush --}}