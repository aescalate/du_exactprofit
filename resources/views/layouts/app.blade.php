<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Exact Profit - Digital Uprisers</title>
        <!-- Fevicon -->
        <link rel="icon" type="image/png" href="{{ asset('images/favicon-32x32.png') }}">
        <!-- Start css -->

        <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}"/>
        <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700;900&display=swap" rel="stylesheet">
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">

        <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
        <link href="{{ asset('plugins/bootstrap-toastr/toastr.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
        @stack('page_css')
        
        @stack('styles')

        <!-- Facebook Pixel Code -->
        <script>
          !function(f,b,e,v,n,t,s)
          {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
          n.callMethod.apply(n,arguments):n.queue.push(arguments)};
          if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
          n.queue=[];t=b.createElement(e);t.async=!0;
          t.src=v;s=b.getElementsByTagName(e)[0];
          s.parentNode.insertBefore(t,s)}(window, document,'script',
          'https://connect.facebook.net/en_US/fbevents.js');
          fbq('init', '434080454254186');
          fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
          src="https://www.facebook.com/tr?id=434080454254186&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-LE2W1RTQWN"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'G-LE2W1RTQWN');
        </script>
        <script type='text/javascript'>
            window.__lo_site_id = 257889;

            (function() {
                var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
                wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
              })();
        </script>
 
        <!-- Smartsupp Live Chat script -->
        <script type="text/javascript">
            var _smartsupp = _smartsupp || {};
            _smartsupp.key = '4590ae3ba6de6d61fabce4bb2bdaabd6156abe88';
            window.smartsupp||(function(d) {
              var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
              s=d.getElementsByTagName('script')[0];c=d.createElement('script');
              c.type='text/javascript';c.charset='utf-8';c.async=true;
              c.src='https://www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
            })(document);
        </script>
    </head>

    <body>
        <div class="Wrapper">
            @include('shared.header')
            <div class="content {{ $routeName == 'complete-profile-form' ? ' profilepage' : ($routeName == 'products.details' ? 'bg-white' : '') }}">
                @if ($routeName == 'products.details')
                    @include('pages.products.info', ['product'=>$product])
                @else
                    @include('shared.infobar')
                @endif
                <div class="datadiv container">
                    @yield('content')
                </div>
            </div>
            <footer>
                <a href="{{ config('app.marketing_url') }}/apps/exactprofit/privacy-policy" target="_blank" title="Terms & Privacy Policy">Terms & Privacy Policy</a>
            </footer>
        </div>
        @include('flash::message')

        <script src="{{ asset('js/jquery-3.5.1.min.js') }}"></script>
        <script src="{{ asset('js/popper.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>

        <script src="{{ asset('plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>

        <script src="{{ asset('plugins/bootstrap-toastr/toastr.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/ui-toastr.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/moment.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/daterangepicker.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootbox/bootbox.min.js') }}"></script>

        <script src="{{ asset('js/custom.js') }}" type="text/javascript"></script>
        @stack('page_js')

        <!-- End js -->
        <!-- chat box js  start -->
        <script type="text/javascript">
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "1000",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            $('div.alert').not('.alert-important').delay(3000).fadeOut(350);

            $(function(){
                $.validator.setDefaults({
                    errorClass: 'help-block',
                    errorElement: 'span',
                    highlight: function (element) {
                       $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element) {
                       $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorPlacement: function (error, element) {
                        if (element.attr("data-error-container")) {
                            error.appendTo(element.attr("data-error-container"));
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
            });
        </script>
        
        @stack('scripts')
    </body>
</html>
