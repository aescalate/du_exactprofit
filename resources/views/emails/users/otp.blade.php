@component('mail::message')
Hello,

Thank you registring to {{ config('app.name') }}, Your One Time Password (OTP) to complete the registration is <b>{{ $user->otp }} </b>.
Click below button to enter the OTP.

@component('mail::button', ['url' => route('otpForm', $user->otp_token)])
Enter OTP
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
