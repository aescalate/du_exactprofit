<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PricesTableSeeder::class);
        $this->call(LanguagesSeeder::class);
        /*$this->call(UsersSeeder::class);*/
    }
}
