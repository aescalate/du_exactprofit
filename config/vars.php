<?php

return [
	'shopify_key' => env('SHOPIFY_API_KEY', ''),
	'shopify_secret' => env('SHOPIFY_API_SECRET', ''),
	'shopify_mode' => env('SHOPIFY_MODE', 'test'),
	'admin_url' => env('ADMIN_URL', ''),
	'grant_url' => env('GRANT_URL', ''),
	'google_dev_token' => env('GOOGLE_DEV_TOKEN'),
	'google_client_id'=> env('GOOGLE_CLIENT_ID'),
	'google_client_secret'=> env('GOOGLE_CLIENT_SECRET'),
	'adwords_scope'=> env('GOOGLE_ADWORDS_SCOPE'),
	'analytics_scope'=> env('GOOGLE_ANALYTICS_SCOPE'),
	'fixer_key' => env('FIXER_API_KEY', '')
];