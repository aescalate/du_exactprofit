/*
---------------------------------
    : Custom - C3 Charts js :
---------------------------------
*/
"use strict";
$(document).ready(function() {
    
    /* -- C3 - Combination Chart -- */
    var combinationChart = c3.generate({
        bindto: '#c3-combination',
        color: { pattern: ["#f2f5fa", "#303030"] },
        data: {
            columns: [
                ['Total Sales', 11000, 12000, 12150, 12340, 12360, 12350],
                ['Views', 200, 130, 90, 240, 130, 220],
            ],
            type: 'bar',
            types: {
                TotalSales: 'spline',
                Views: 'line',
            },
            groups: [
                ['TotalSales','Views']
            ]
        }
    });      
});