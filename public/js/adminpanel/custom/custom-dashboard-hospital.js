/*
-------------------------------------------
    : Custom - Dashboard Hospital js :
-------------------------------------------
*/
"use strict";
$(document).ready(function() {    
    /* -- Apex Basic Column Chart -- */
    var options = {
        chart: {
            height: 295,
            type: 'bar',
            toolbar: {
                show: false
            },
            zoom: {
                enabled: false
            }
        },
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: '45%',
                endingShape: 'rounded'  
            },
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            show: true,
            width: 2,
            colors: ['transparent']
        },
        colors: ['#0080ff','#18d26b','#d4d8de'],
        series: [{
            name: 'New',
            data: [76, 85, 101, 98, 87, 105, 91, 114, 94]
        }, {
            name: 'Old',            
            data: [44, 55, 57, 56, 61, 58, 63, 60, 66]
        }, {
            name: 'Booked',
            data: [35, 41, 36, 26, 45, 48, 52, 53, 41]
        }],
        legend: {
            show: false,
        },
        xaxis: {
            categories: ['Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct'],
            axisBorder: {
                show: true, 
                color: 'rgba(0,0,0,0.05)'
            },
            axisTicks: {
                show: true, 
                color: 'rgba(0,0,0,0.05)'
            }
        },
        yaxis: {
            title: {
                text: ''
            }
        },
        grid: {
            row: {
                colors: ['transparent', 'transparent'], opacity: .2
            },
            borderColor: 'rgba(0,0,0,0.05)'
        },
        fill: {
            opacity: 1

        },
        tooltip: {
            y: {
                formatter: function (val) {
                    return "$ " + val + " thousands"
                }
            }
        }
    }
    var chart = new ApexCharts(
        document.querySelector("#apex-basic-column-chart"),
        options
    );
    chart.render();

    /* ----- Total Order Chart ----- */
    var options = {
        chart: {
            type:"area",
            height: 80,
            sparkline: {
                enabled: true
            }
        },
        stroke: {
            curve: "smooth", 
            width: 2
        },
        fill: {
            type: 'gradient',
            gradient: {
                shadeIntensity: 1,
                inverseColors: false,
                opacityFrom: 0.45,
                opacityTo: 0.05,
                stops: [20, 100, 100, 100]
            },
        },
        series:[ {
            data: [71, 40, 28, 51, 42, 109, 100]
        }],
        yaxis: {
            min: 0
        },
        colors:["#0080ff"],
        grid: {
            row: {
                colors: ['transparent', 'transparent'], opacity: .2
            },
            borderColor: 'transparent'
        },
        tooltip: {
            x: {
                format: 'dd/MM/yy HH:mm'
            },
        }
    }
    var chart = new ApexCharts(
        document.querySelector("#total-orders"),
        options
    );
    chart.render();

    /* ----- Total Sales Chart ----- */
    var options = {
        chart: {
            type:"area",
            height: 80,
            sparkline: {
                enabled: true
            }
        },
        stroke: {
            curve: "smooth", 
            width: 2
        },
        fill: {
            type: 'gradient',
            gradient: {
                shadeIntensity: 1,
                inverseColors: false,
                opacityFrom: 0.45,
                opacityTo: 0.05,
                stops: [20, 100, 100, 100]
            },
        },
        series:[ {
            data: [35, 105, 95, 45, 25, 95, 58]
        }],
        yaxis: {
            min: 0
        },
        colors:["#0080ff"],
        grid: {
            row: {
                colors: ['transparent', 'transparent'], opacity: .2
            },
            borderColor: 'transparent'
        },
        tooltip: {
            x: {
                format: 'dd/MM/yy HH:mm'
            },
        }
    }
    var chart = new ApexCharts(
        document.querySelector("#total-sales"),
        options
    );
    chart.render();
    
    /* ----- Total Discounts Chart ----- */
    var options = {
        chart: {
            type:"area",
            height: 80,
            sparkline: {
                enabled: true
            }
        },
        stroke: {
            curve: "smooth", 
            width: 2
        },
        fill: {
            type: 'gradient',
            gradient: {
                shadeIntensity: 1,
                inverseColors: false,
                opacityFrom: 0.45,
                opacityTo: 0.05,
                stops: [20, 100, 100, 100]
            },
        },
        series:[ {
            data: [35, 105, 95, 45, 25, 95, 58]
        }],
        yaxis: {
            min: 0
        },
        colors:["#0080ff"],
        grid: {
            row: {
                colors: ['transparent', 'transparent'], opacity: .2
            },
            borderColor: 'transparent'
        },
        tooltip: {
            x: {
                format: 'dd/MM/yy HH:mm'
            },
        }        
    }
    var chart = new ApexCharts(
        document.querySelector("#total-discounts"),
        options
    );        
    chart.render();
    /* ----- Revenue Chart ----- */
    var options = {
        chart: {
            type:"area",
            height: 80,
            sparkline: {
                enabled: true
            }
        },
        stroke: {
            curve: "smooth", 
            width: 2
        },
        fill: {
            type: 'gradient',
            gradient: {
                shadeIntensity: 1,
                inverseColors: false,
                opacityFrom: 0.45,
                opacityTo: 0.05,
                stops: [20, 100, 100, 100]
            },
        },
        series:[ {
            data: [35, 105, 95, 45, 25, 95, 58]
        }],
        yaxis: {
            min: 0
        },
        colors:["#0080ff"],
        grid: {
            row: {
                colors: ['transparent', 'transparent'], opacity: .2
            },
            borderColor: 'transparent'
        },
        tooltip: {
            x: {
                format: 'dd/MM/yy HH:mm'
            },
        }       
    }
    var chart = new ApexCharts(
        document.querySelector("#revenue"),
        options
    );        
    chart.render();
    /* ----- Shipping Cost Chart ----- */
    var options = {
        chart: {
            type:"area",
            height: 80,
            sparkline: {
                enabled: true
            }
        },
        stroke: {
            curve: "smooth", 
            width: 2
        },
        fill: {
            type: 'gradient',
            gradient: {
                shadeIntensity: 1,
                inverseColors: false,
                opacityFrom: 0.45,
                opacityTo: 0.05,
                stops: [20, 100, 100, 100]
            },
        },
        series:[ {
            data: [35, 105, 95, 45, 25, 95, 58]
        }],
        yaxis: {
            min: 0
        },
        colors:["#0080ff"],
        grid: {
            row: {
                colors: ['transparent', 'transparent'], opacity: .2
            },
            borderColor: 'transparent'
        },
        tooltip: {
            x: {
                format: 'dd/MM/yy HH:mm'
            },
        }       
    }
    var chart = new ApexCharts(
        document.querySelector("#shippingcost"),
        options
    );        
    chart.render();
    /* ----- Gross Profit Chart ----- */
    var options = {
        chart: {
            type:"area",
            height: 80,
            sparkline: {
                enabled: true
            }
        },
        stroke: {
            curve: "smooth", 
            width: 2
        },
        fill: {
            type: 'gradient',
            gradient: {
                shadeIntensity: 1,
                inverseColors: false,
                opacityFrom: 0.45,
                opacityTo: 0.05,
                stops: [20, 100, 100, 100]
            },
        },
        series:[ {
            data: [35, 105, 95, 45, 25, 95, 58]
        }],
        yaxis: {
            min: 0
        },
        colors:["#0080ff"],
        grid: {
            row: {
                colors: ['transparent', 'transparent'], opacity: .2
            },
            borderColor: 'transparent'
        },
        tooltip: {
            x: {
                format: 'dd/MM/yy HH:mm'
            },
        }       
    }
    var chart = new ApexCharts(
        document.querySelector("#grossprofit"),
        options
    );        
    chart.render();
    /* ----- Taxes Chart ----- */
    var options = {
        chart: {
            type:"area",
            height: 80,
            sparkline: {
                enabled: true
            }
        },
        stroke: {
            curve: "smooth", 
            width: 2
        },
        fill: {
            type: 'gradient',
            gradient: {
                shadeIntensity: 1,
                inverseColors: false,
                opacityFrom: 0.45,
                opacityTo: 0.05,
                stops: [20, 100, 100, 100]
            },
        },
        series:[ {
            data: [35, 105, 95, 45, 25, 95, 58]
        }],
        yaxis: {
            min: 0
        },
        colors:["#0080ff"],
        grid: {
            row: {
                colors: ['transparent', 'transparent'], opacity: .2
            },
            borderColor: 'transparent'
        },
        tooltip: {
            x: {
                format: 'dd/MM/yy HH:mm'
            },
        }       
    }
    var chart = new ApexCharts(
        document.querySelector("#taxes"),
        options
    );        
    chart.render();
    /* ----- Total Refund Chart ----- */
    var options = {
        chart: {
            type:"area",
            height: 80,
            sparkline: {
                enabled: true
            }
        },
        stroke: {
            curve: "smooth", 
            width: 2
        },
        fill: {
            type: 'gradient',
            gradient: {
                shadeIntensity: 1,
                inverseColors: false,
                opacityFrom: 0.45,
                opacityTo: 0.05,
                stops: [20, 100, 100, 100]
            },
        },
        series:[ {
            data: [35, 105, 95, 45, 25, 95, 58]
        }],
        yaxis: {
            min: 0
        },
        colors:["#0080ff"],
        grid: {
            row: {
                colors: ['transparent', 'transparent'], opacity: .2
            },
            borderColor: 'transparent'
        },
        tooltip: {
            x: {
                format: 'dd/MM/yy HH:mm'
            },
        }       
    }
    var chart = new ApexCharts(
        document.querySelector("#totalrefund"),
        options
    );        
    chart.render();
    /* ----- Average Value Chart ----- */
    var options = {
        chart: {
            type:"area",
            height: 80,
            sparkline: {
                enabled: true
            }
        },
        stroke: {
            curve: "smooth", 
            width: 2
        },
        fill: {
            type: 'gradient',
            gradient: {
                shadeIntensity: 1,
                inverseColors: false,
                opacityFrom: 0.45,
                opacityTo: 0.05,
                stops: [20, 100, 100, 100]
            },
        },
        series:[ {
            data: [35, 105, 95, 45, 25, 95, 58]
        }],
        yaxis: {
            min: 0
        },
        colors:["#0080ff"],
        grid: {
            row: {
                colors: ['transparent', 'transparent'], opacity: .2
            },
            borderColor: 'transparent'
        },
        tooltip: {
            x: {
                format: 'dd/MM/yy HH:mm'
            },
        }       
    }
    var chart = new ApexCharts(
        document.querySelector("#avgvalue"),
        options
    );        
    chart.render();
    /* ----- Avg Order Profit Chart ----- */
    var options = {
        chart: {
            type:"area",
            height: 80,
            sparkline: {
                enabled: true
            }
        },
        stroke: {
            curve: "smooth", 
            width: 2
        },
        fill: {
            type: 'gradient',
            gradient: {
                shadeIntensity: 1,
                inverseColors: false,
                opacityFrom: 0.45,
                opacityTo: 0.05,
                stops: [20, 100, 100, 100]
            },
        },
        series:[ {
            data: [35, 105, 95, 45, 25, 95, 58]
        }],
        yaxis: {
            min: 0
        },
        colors:["#0080ff"],
        grid: {
            row: {
                colors: ['transparent', 'transparent'], opacity: .2
            },
            borderColor: 'transparent'
        },
        tooltip: {
            x: {
                format: 'dd/MM/yy HH:mm'
            },
        }       
    }
    var chart = new ApexCharts(
        document.querySelector("#avgprofit"),
        options
    );        
    chart.render();
    /* ----- Gross Margin Chart ----- */
    var options = {
        chart: {
            type:"area",
            height: 80,
            sparkline: {
                enabled: true
            }
        },
        stroke: {
            curve: "smooth", 
            width: 2
        },
        fill: {
            type: 'gradient',
            gradient: {
                shadeIntensity: 1,
                inverseColors: false,
                opacityFrom: 0.45,
                opacityTo: 0.05,
                stops: [20, 100, 100, 100]
            },
        },
        series:[ {
            data: [35, 105, 95, 45, 25, 95, 58]
        }],
        yaxis: {
            min: 0
        },
        colors:["#0080ff"],
        grid: {
            row: {
                colors: ['transparent', 'transparent'], opacity: .2
            },
            borderColor: 'transparent'
        },
        tooltip: {
            x: {
                format: 'dd/MM/yy HH:mm'
            },
        }       
    }
    var chart = new ApexCharts(
        document.querySelector("#grossmargin"),
        options
    );        
    chart.render();
    /* ----- Net Margin Chart ----- */
    var options = {
        chart: {
            type:"area",
            height: 80,
            sparkline: {
                enabled: true
            }
        },
        stroke: {
            curve: "smooth", 
            width: 2
        },
        fill: {
            type: 'gradient',
            gradient: {
                shadeIntensity: 1,
                inverseColors: false,
                opacityFrom: 0.45,
                opacityTo: 0.05,
                stops: [20, 100, 100, 100]
            },
        },
        series:[ {
            data: [35, 105, 95, 45, 25, 95, 58]
        }],
        yaxis: {
            min: 0
        },
        colors:["#0080ff"],
        grid: {
            row: {
                colors: ['transparent', 'transparent'], opacity: .2
            },
            borderColor: 'transparent'
        },
        tooltip: {
            x: {
                format: 'dd/MM/yy HH:mm'
            },
        }       
    }
    var chart = new ApexCharts(
        document.querySelector("#netmargin"),
        options
    );        
    chart.render();
    /* ----- Net Profit Chart ----- */
    var options = {
        chart: {
            type:"area",
            height: 80,
            sparkline: {
                enabled: true
            }
        },
        stroke: {
            curve: "smooth", 
            width: 2
        },
        fill: {
            type: 'gradient',
            gradient: {
                shadeIntensity: 1,
                inverseColors: false,
                opacityFrom: 0.45,
                opacityTo: 0.05,
                stops: [20, 100, 100, 100]
            },
        },
        series:[ {
            data: [35, 105, 95, 45, 25, 95, 58]
        }],
        yaxis: {
            min: 0
        },
        colors:["#0080ff"],
        grid: {
            row: {
                colors: ['transparent', 'transparent'], opacity: .2
            },
            borderColor: 'transparent'
        },
        tooltip: {
            x: {
                format: 'dd/MM/yy HH:mm'
            },
        }       
    }
    var chart = new ApexCharts(
        document.querySelector("#netprofit"),
        options
    );        
    chart.render();
    /* ----- Ad Spend Chart ----- */
    var options = {
        chart: {
            type:"area",
            height: 80,
            sparkline: {
                enabled: true
            }
        },
        stroke: {
            curve: "smooth", 
            width: 2
        },
        fill: {
            type: 'gradient',
            gradient: {
                shadeIntensity: 1,
                inverseColors: false,
                opacityFrom: 0.45,
                opacityTo: 0.05,
                stops: [20, 100, 100, 100]
            },
        },
        series:[ {
            data: [35, 105, 95, 45, 25, 95, 58]
        }],
        yaxis: {
            min: 0
        },
        colors:["#0080ff"],
        grid: {
            row: {
                colors: ['transparent', 'transparent'], opacity: .2
            },
            borderColor: 'transparent'
        },
        tooltip: {
            x: {
                format: 'dd/MM/yy HH:mm'
            },
        }       
    }
    var chart = new ApexCharts(
        document.querySelector("#adspend"),
        options
    );        
    chart.render();
    /* ----- CAC Chart ----- */
    var options = {
        chart: {
            type:"area",
            height: 80,
            sparkline: {
                enabled: true
            }
        },
        stroke: {
            curve: "smooth", 
            width: 2
        },
        fill: {
            type: 'gradient',
            gradient: {
                shadeIntensity: 1,
                inverseColors: false,
                opacityFrom: 0.45,
                opacityTo: 0.05,
                stops: [20, 100, 100, 100]
            },
        },
        series:[ {
            data: [35, 105, 95, 45, 25, 95, 58]
        }],
        yaxis: {
            min: 0
        },
        colors:["#0080ff"],
        grid: {
            row: {
                colors: ['transparent', 'transparent'], opacity: .2
            },
            borderColor: 'transparent'
        },
        tooltip: {
            x: {
                format: 'dd/MM/yy HH:mm'
            },
        }       
    }
    var chart = new ApexCharts(
        document.querySelector("#cac"),
        options
    );        
    chart.render();
});