$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
});

function getStatusText(code)
{
    sText = "";
    if(code !== undefined)
    {
        switch(code)
        {
            case 200:{ sText = 'Success'; break;}
            case 404:{ sText = 'Error';break;}
            case 403:{ sText = 'Error';break;}
            case 500:{ sText = 'Error';break;}
            case "success": { sText = "Success"; break;}
            case "danger":{ sText = 'Error';break;}
            case "warning":{ sText = 'Error';break;}
            default:{sText = 'Error';}
            
        }
    }
    return sText;
}

function showMessage(sType,sText){
    sType = getStatusText(sType);
    toastr[sType.toLowerCase()](sText);
}
function addOverlay(){$('<div id="overlayDocument"><img src="/img/loading.gif" /></div>').appendTo(document.body);}
function removeOverlay(){$('#overlayDocument').remove();}