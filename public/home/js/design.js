 $( window ).resize(function() {
	     	if ($(window).width() < 992) {
				   $("#headermenu").css("display", "none");
				}
				else {
				   $("#headermenu").css("display", "block");
				}  		
		});
		$(document).ready(function(){
			// home page video script start here 
			$("#videobtn").on("click",function(){
	   			$('#videohome').attr("src", "https://www.youtube.com/embed/1kCDd6rEmxs"); // need to enter url forvideo
	   		});
	   		$(document).click(function(event) {
			 $('#homeVideo').on('hidden.bs.modal', function(){
				$('#homeVideo').hide();
				$('#homeVideo iframe').attr("src", "");
				});
			});
	   		// home page video script end  here

		    $(".mobile-toggle").on("click",function(){
		    	$("#headermenu").css("width", "100%");
		    	$("#headermenu").css("display", "block");
		    });
		    $("#closebtn").on("click", function(){
				$("#headermenu").css("width", "0");				
		    });
			// sticky header script start here
		   	// When the user scrolls the page, execute myFunction
			window.onscroll = function() {myFunction()};
			// Get the header
			var header = document.getElementById("myHeader");
			// Get the offset position of the navbar
			var sticky = header.offsetTop;
			// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
			function myFunction() {
			  if (window.pageYOffset > sticky) {
			    header.classList.add("sticky");
			  } else {
			    header.classList.remove("sticky");
			  }
			}
			// sticky header script end here
	        $('.screen_slider').slick({
		          infinite: true,
		          slidesToShow: 1,
		          slidesToScroll: 1,
		          dots: true,
		          speed: 300,
		          arrows: false,
		          autoplay: true,
				  autoplaySpeed: 4000,
	            });

	        $('.trendingblogslider').slick({
				  slidesToShow: 4,
				  slidesToScroll: 1,
				  autoplay: true,
				  autoplaySpeed: 2000,
				  responsive: [
		                {
		                  breakpoint: 1199,
		                  settings: {
		                    slidesToShow: 3
		                  }
		                },
		                {
		                  breakpoint: 992,
		                  settings: {
		                    slidesToShow: 2
		                  }
		                },
		                {
		                  breakpoint: 420,
		                  settings: {
		                    slidesToShow: 1
		                  }
		                }
		            ]	
      		});
      		$(".navbar-nav li a").on('click', function(event) {
			    // Make sure this.hash has a value before overriding default behavior
			    if (this.hash !== "") {
			      // Prevent default anchor click behavior
			      event.preventDefault();

			      // Store hash
			      var hash = this.hash;

			      // Using jQuery's animate() method to add smooth page scroll
			      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
			      $('html, body').animate({
			        scrollTop: $(hash).offset().top
			      }, 800, function(){
			   
			        // Add hash (#) to URL when done scrolling (default click behavior)
			        window.location.hash = hash;
			      });
			    } // End if
			  });
	    });